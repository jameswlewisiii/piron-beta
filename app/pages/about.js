import Layout from '../components/layout/main'
import Link from 'next/link'
import IronApi from '../modules/IronApi';

export default class About extends React.Component {
    async handleSubmit(event) {
        event.preventDefault()

        let iron = new IronApi
        let response = await iron.login(
            document.getElementById('email').value,
            document.getElementById('password').value
        )
        if(response.data.token) {
            window.location.href = '/dashboard'
        } else {
            document.getElementById('error-message').innerText = response.data.message
        }
    }
    render() {
        return (
            <Layout>
                <div className="container mt-5">
                    <div className="row">
                        <div className="col-md-6 offset-md-3">
                            <div className="card">
                                <div className="card-header">
                                    Log In
                                </div>
                                <div className="card-body">
                                    <h5 className="card-title">Log into your account.</h5>
                                    <div id="error-message" className="text-danger"></div>
                                    <form onSubmit={this.handleSubmit}>
                                        <div className="form-group">
                                            <label >Email address</label>
                                            <input type="email" className="form-control" id="email" aria-describedby="emailHelp" placeholder="Enter email" required/>
                                            <small id="emailHelp" className="form-text text-muted">We'll never share your email with anyone else.</small>
                                        </div>
                                        <div className="form-group">
                                            <label>Password</label>
                                            <input type="password" className="form-control" id="password" placeholder="Password" required/>
                                            <Link href="/password-reset">
                                                <small><a href="#">Forgot Password?</a></small>
                                            </Link>
                                        </div>
                                        <button type="submit" className="btn btn-primary w-100">Submit</button>
                                    </form>
                                </div>
                            </div>
                            <div className="card mt-3">
                                <div className="card-body">
                                    Don't have an account? Sign up <Link href="/register"><a> here</a></Link>.
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </Layout>
        )
    }   
}
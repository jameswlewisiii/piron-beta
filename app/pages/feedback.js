import Layout from "../components/layout/main";
import IronApi from "../modules/IronApi";
import SupportForm from "../components/supportForm";

export default class Feedback extends React.Component {
    state = {
        errorMessage: null,
        successMessage: false
    };
    afterSubmit() {
        window.scrollTo(0, 0);
        alert("You have succesfully sent the Piron.io team feedback.");
    }
    render(props) {
        return (
            <Layout>
                <div className="jumbotron">
                    <div className="container">
                        <div className="row">
                            <div className="col-12">
                                {this.state.successMessage && (
                                    <div
                                        className="alert alert-success"
                                        role="alert"
                                    >
                                        You have succesfully sent the Piron.io
                                        team feedback.
                                    </div>
                                )}
                                <h1 className="display-1">Feedback</h1>
                                <h2>Piron.io is currently in beta</h2>
                            </div>
                            <div className="col-12">
                                {/* <h2> <span className="bg-warning">About</span></h2> */}
                                <p className="lead">
                                    Built for tattoo artist and shop managers.
                                    The tool is completely{" "}
                                    <span className="text-primary">free</span>{" "}
                                    to use. It is currently in{" "}
                                    <span className="text-info">beta</span> at
                                    the moment while we iron out some of the
                                    wrinkles and add more features.
                                </p>
                                <p className="lead">
                                    If you enconter any type of error or have
                                    any suggestions on how to make the tool
                                    better don't hesitate to contact our team at{" "}
                                    <a
                                        className="text-warning"
                                        href="mailto:jameswlewisiii@gmail.com"
                                    >
                                        whatsup@piron.io
                                    </a>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="container">
                    <div className="row">
                        <div className="col-12">
                            <h1>Let us know</h1>
                            <SupportForm afterSubmit={this.afterSubmit} />
                        </div>
                    </div>
                </div>
            </Layout>
        );
    }
}

import Layout from "../components/layout/main";
import Link from "next/link";

export default class Quickstart extends React.Component {
    render() {
        return (
            <Layout>
                <div className="jumbotron">
                    <div className="container">
                        <div className="row">
                            <div className="col-12">
                                <h1>Quickstart Guide</h1>
                                <h3>
                                    An easy to follow guide that will help new
                                    users get acquainted with the Piron.io
                                    application.
                                </h3>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="container">
                    <div className="row">
                        <div className="col-md-3 col-sm-12">
                            <ul className="nav flex-column">
                                <li className="nav-item">
                                    <a className="nav-link" href="#intro">
                                        Intro
                                    </a>
                                </li>
                                <li className="nav-item">
                                    <a className="nav-link" href="#dashboard">
                                        Dashboard
                                    </a>
                                </li>
                                <li className="nav-item">
                                    <a className="nav-link" href="#clients">
                                        Clients
                                    </a>
                                </li>
                                <li className="nav-item">
                                    <a
                                        className="nav-link"
                                        href="#transactions"
                                    >
                                        Transactions
                                    </a>
                                </li>
                                <li className="nav-item">
                                    <a
                                        className="nav-link"
                                        href="#appointments"
                                    >
                                        Appoinments
                                    </a>
                                </li>
                                <li className="nav-item">
                                    <a className="nav-link" href="#multi">
                                        Multi-User Accounts
                                    </a>
                                </li>
                                <li className="nav-item">
                                    <a className="nav-link" href="#settings">
                                        Settings
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div className="col-md-9 col-sm-12">
                            <h3 id="intro">Into</h3>
                            <h5>Guides Purpose</h5>
                            <p>
                                This guide is intended to give new users a breif
                                overview of our application.
                            </p>

                            <h5>Registration</h5>
                            <p>
                                The first step is to register an account by
                                visiting the{" "}
                                <Link href="/register">
                                    <a href="#">registration page</a>
                                </Link>
                                . It only takes two mintues to register. Enter a
                                valid email address, set up a password, and
                                submit the registration form to create an
                                account.
                            </p>
                            <img
                                src="/static/images/piron_dashboard.png"
                                alt="Piron Dashboard"
                                width="400px"
                                className="img-thumbnail img-fluid float-right ml-2 mr-2"
                            />
                            <h3 id="dashboard">Dashboard</h3>
                            <p>
                                The dashboard is Piron's centeral hub. It will
                                be the page you are directed to after creating
                                an account and each time you log in. It provides
                                a top down view of all clients, transactions,
                                and appointments.
                            </p>
                            <p>
                                The top row of colorfully boxes called dash
                                cards display the current months clients,
                                transactions, appointments, and income total.
                                Located on the right under "Transactions this
                                month" is a line chart of transaction made this
                                month and to the right on the chart is a list of
                                recent transaction that link to the individual
                                transaction views.
                            </p>
                            <p>
                                On the last row of the dashboard is a list of
                                today's appointments and new clients created
                                this month. Both list are clickable and will
                                direct you to the corresponding individual view.
                            </p>
                            <h3 id="clients">Clients</h3>
                            <p>
                                You can access all clients by clicking the
                                "CLIENTS" link in the navbar. The client view
                                allows you to view, sort, and filter all
                                clients. To the far right inside the client
                                table are the client options. The client option
                                buttons allow you to view (blue), edit (yellow),
                                and delete (red). By viewing a client, you can
                                see the client's details, any appointments the
                                client might have, as well as past transactions.
                            </p>
                            <img
                                src="/static/images/piron_buttons.png"
                                alt="Piron Buttons"
                                className="img img-fluid float-right ml-2 mr-2"
                            />
                            <p>
                                To The top right of the clients page will a list
                                of buttons. To create a new client click the
                                green button located at the top right. If you
                                are apart of a multi-user account, the black and
                                gray button will appear and allow you to change
                                the client table list view.
                            </p>

                            <h3 id="transactions">Transactions</h3>
                            <p>
                                Transactions repersent any sale or tattoo done
                                for a client. A transaction is assoicated to a
                                client and make up a clients history. You can
                                view all transaction by clicking the
                                "TRANSACTIONS" link in the navbar. Just like the
                                client's table, the transaction table can be
                                sorted, filtered, and contain the same view,
                                update, and delete buttons.
                            </p>
                            <p>
                                To create a new transaction click the green
                                button located at the top right.
                            </p>
                            <h3 id="appointments">Appointments</h3>
                            <p>
                                Appointments are used to keep the shop orginized
                                and running like a fine tuned machine. You can
                                view the appointments page my clicking the
                                "APPOINTMENTS" link in the top navbar. Located
                                on the appointment page are the appointment
                                filter tabs. By default daily is selected,
                                however; you filter the appointment list by week
                                or month.
                            </p>
                            <p>
                                To create a new appointment click the green
                                button located at the top right.
                            </p>
                            <h3 id="multi">Multi-User Accounts</h3>
                            <p>
                                If you have a{" "}
                                <a href="/register/">pro account</a> you have
                                the ability to add mulitple users to an account.
                                This give users the ablity to work together and
                                share important client, transation, and
                                appointment information with other Piron users.
                                A perfect set up for tattoo shops with multiple
                                tattoo artist.
                            </p>
                            <p>
                                Click the "Users" link in the navigation bar to
                                view all account users. You can add addtional
                                users by clicking on the green button located
                                int he top right. Once a user has been added a
                                tempary password is emailed to the user's email
                                address.
                            </p>
                            <img
                                src="/static/images/piron_invite.png"
                                alt="Piron Buttons"
                                className="img img-fluid float-right ml-2 mr-2"
                            />
                            <p>
                                A newly create users will default to the "user"
                                role. The user role doesn't have the permission
                                to edit or delete other user's items on the
                                account.
                            </p>

                            <p>
                                You can change a user's role by clicked the blue
                                button titled "CHANGE ROLES." This will toggle
                                the an account user from the roles "user" and
                                "admin". The "admin" role has permission to
                                view, edit, and delete all items on the account
                                this includes making changes to other user's
                                data.
                            </p>

                            <h3 id="settings">Settings</h3>
                            <p>
                                The settings page can be accessed by click the
                                account email, located in the top right corner
                                or by visiting{" "}
                                <Link href="/settings">/settings</Link>. On the
                                settings page you can manage your shop/studios
                                information including website, email, phone, and
                                address. The settings page is also where you can
                                update your login email and password.
                            </p>
                        </div>
                    </div>
                </div>
            </Layout>
        );
    }
}

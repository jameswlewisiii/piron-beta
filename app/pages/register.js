import Layout from "../components/layout/main";
import Link from "next/link";
import IronApi from "../modules/IronApi";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { library } from "@fortawesome/fontawesome-svg-core";
import { faCheck } from "@fortawesome/free-solid-svg-icons";

library.add([faCheck]);

export default class Register extends React.Component {
    async handleSubmit(event) {
        event.preventDefault();

        let iron = new IronApi();
        let res = await iron.register({
            email: document.getElementById("email").value,
            password: document.getElementById("password").value
        });

        if (res.data.token) {
            window.location.href = "/dashboard";
        } else {
            document.getElementById("error-message").innerText =
                res.data.message;
        }
    }

    render() {
        return (
            <Layout>
                <div className="container mt-5">
                    <h1 className="text-center m-4">
                        Select the right Plan for your workload
                    </h1>
                    <div className="row">
                        <div className="col-sm-12">
                            <div className="card-deck">
                                {/* FREEMIUM PLAN */}
                                <div className="card">
                                    <div className="bg-info">
                                        <div className="card-body">
                                            <h4 className="text-center">
                                                Freemium
                                            </h4>
                                            <h1 className="display-3 text-center mt-3">
                                                $0.00
                                                <span className="h4">
                                                    / month
                                                </span>
                                            </h1>
                                        </div>
                                    </div>
                                    <div className="card-body text-center">
                                        <h5 className="card-title m-4">
                                            Single User Account{" "}
                                            <FontAwesomeIcon icon="check" />
                                        </h5>
                                        <h5 className="card-title m-4">
                                            Unlimated client database{" "}
                                            <FontAwesomeIcon icon="check" />
                                        </h5>
                                        <h5 className="card-title m-4">
                                            Appointment manager{" "}
                                            <FontAwesomeIcon icon="check" />
                                        </h5>
                                        <h5 className="card-title m-4">
                                            Comprehensive Dashboard{" "}
                                            <FontAwesomeIcon icon="check" />
                                        </h5>
                                        <Link href="/register/freemium">
                                            <button className="btn btn-primary mt-4">
                                                Join for free
                                            </button>
                                        </Link>
                                    </div>
                                    <div className="card-footer">
                                        <small className="text-muted">
                                            No credit card is required for sign
                                            up.
                                        </small>
                                    </div>
                                </div>
                                {/* PRO PLAN */}
                                <div className="card">
                                    <div className="bg-warning">
                                        <div className="card-body">
                                            <h4 className="text-center">Pro</h4>
                                            <h1 className="display-3 text-center mt-3">
                                                $2.99
                                                <span className="h4">
                                                    / month
                                                </span>
                                            </h1>
                                        </div>
                                    </div>
                                    <div className="card-body text-center">
                                        <h5 className="card-title m-4">
                                            30 day free trial{" "}
                                            <FontAwesomeIcon icon="check" />
                                        </h5>
                                        <h5 className="card-title m-4">
                                            All freemium features{" "}
                                            <FontAwesomeIcon icon="check" />
                                        </h5>
                                        <h5 className="card-title m-4">
                                            Unlimited User Accounts{" "}
                                            <FontAwesomeIcon icon="check" />
                                        </h5>
                                        <h5 className="card-title m-4">
                                            Access to all features{" "}
                                            <FontAwesomeIcon icon="check" />
                                        </h5>
                                        <h5 className="card-title m-4">
                                            Account support and help{" "}
                                            <FontAwesomeIcon icon="check" />
                                        </h5>
                                        <Link href="/register/pro">
                                            <button className="btn btn-primary mt-4">
                                                Choose Pro
                                            </button>
                                        </Link>
                                    </div>
                                    <div className="card-footer">
                                        <small className="text-muted">
                                            Credit card required. Payment will
                                            be billed monthly.
                                        </small>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </Layout>
        );
    }
}

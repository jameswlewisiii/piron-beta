import Link from "next/link";
import IronApi from "../../modules/IronApi";
import AppLayout from "../../components/layout/appLayout";
import TopInfo from "../../components/topInfo";
import AccountForm from "../../components/settings/accountForm";
import EmailForm from "../../components/settings/emailForm";
import PasswordForm from "../../components/settings/passwordForm";
import BillingForm from "../../components/settings/billingForm";

export default class SettingsIndex extends React.Component {
    breadcrumbs = [
        {
            href: "/dashboard",
            name: "Dashboard"
        },
        {
            href: "/settings",
            name: "Settings",
            active: true
        }
    ];
    state = {
        menu: {
            general: 1,
            email: 0,
            password: 0,
            billing: 0
        }
    };
    componentDidMount() {}
    displayGeneral = event => {
        this.setState({
            menu: {
                general: 1,
                email: 0,
                password: 0,
                billing: 0
            }
        });
    };
    displayEmail = event => {
        this.setState({
            menu: {
                general: 0,
                email: 1,
                password: 0,
                billing: 0
            }
        });
    };
    displayPassword = event => {
        this.setState({
            menu: {
                general: 0,
                email: 0,
                password: 1,
                billing: 0
            }
        });
    };
    displayBilling = event => {
        this.setState({
            menu: {
                general: 0,
                email: 0,
                password: 0,
                billing: 1
            }
        });
    };

    render() {
        let nonActiveClass = "nav-link text-warning";
        let activeClass = nonActiveClass + " active";

        return (
            <AppLayout>
                <TopInfo title="Settings" breadcrumbs={this.breadcrumbs} />
                <div className="row">
                    {/* <div className="col-3">
                        <div className="card" >
                            <div className="card-header">
                                <FontAwesomeIcon icon="cog"/> Settings
                            </div>
                            <ul className="list-group list-group-flush">
                                <a href="#" id='general' className={this.state.menu.general ? activeClass : nonActiveClass} onClick={this.displayGeneral}>
                                    General
                                </a>
                                <a href="#" id='email' className={this.state.menu.email ? activeClass : nonActiveClass} onClick={this.displayEmail}>
                                    Email
                                </a>
                                <a href="#" id='email' className={this.state.menu.password ? activeClass : nonActiveClass} onClick={this.displayPassword}>
                                    Password
                                </a>
                            </ul>
                        </div>
                    </div> */}
                    <div className="col-12">
                        <div className="card">
                            <ul className="nav nav-tabs">
                                <li className="nav-item">
                                    <a
                                        href="#"
                                        id="general"
                                        className={
                                            this.state.menu.general
                                                ? activeClass
                                                : nonActiveClass
                                        }
                                        onClick={this.displayGeneral}
                                    >
                                        General
                                    </a>
                                </li>
                                <li className="nav-item">
                                    <a
                                        href="#"
                                        id="email"
                                        className={
                                            this.state.menu.email
                                                ? activeClass
                                                : nonActiveClass
                                        }
                                        onClick={this.displayEmail}
                                    >
                                        Email
                                    </a>
                                </li>
                                <li className="nav-item">
                                    <a
                                        href="#"
                                        id="email"
                                        className={
                                            this.state.menu.password
                                                ? activeClass
                                                : nonActiveClass
                                        }
                                        onClick={this.displayPassword}
                                    >
                                        Password
                                    </a>
                                </li>
                                <li className="nav-item">
                                    <a
                                        href="#"
                                        id="billing"
                                        className={
                                            this.state.menu.billing
                                                ? activeClass
                                                : nonActiveClass
                                        }
                                        onClick={this.displayBilling}
                                    >
                                        Billing
                                    </a>
                                </li>
                            </ul>
                            <div className="card-body">
                                {this.state.menu.general == 1 && (
                                    <AccountForm />
                                )}
                                {this.state.menu.email == 1 && <EmailForm />}
                                {this.state.menu.password == 1 && (
                                    <PasswordForm />
                                )}
                                {this.state.menu.billing == 1 && (
                                    <BillingForm />
                                )}
                            </div>
                        </div>
                    </div>
                </div>
            </AppLayout>
        );
    }
}

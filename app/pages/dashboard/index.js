import AppLayout from '../../components/layout/appLayout'
import IronApi from '../../modules/IronApi'
import ClientList from '../../components/clients/clientList'
import TransactionList from '../../components/transactions/transactionList'
import TransactionGraph from '../../components/transactions/transactionGraph'
import Dashcard from '../../components/dashcard'
import AppointmentManager from '../../components/appointmentManager'

export default class Index extends React.Component {
    state = {
        monthlyTotals: {
            clients: 0,
            appointments: 0,
            transactions: 0,
            income: 0,
        },
    }
    async componentDidMount() {
        let iron = new IronApi()
        let res = await iron.getDashboardData()
        this.setState({monthlyTotals: res.data.analytics})
    }
    render() {

        return (
            <AppLayout>
                <div>
                    <div className="row">
                        <div className="col-sm-12 col-md-3 mt-3 mt-md-0">
                            <Dashcard link='/clients' number={this.state.monthlyTotals.clients} background='info' infoText='New Clients' />
                        </div>
                        <div className="col-sm-12 col-md-3 mt-3 mt-md-0">
                            <Dashcard link='/appointments' number={this.state.monthlyTotals.appointments} background='warning' infoText='Appointments' />
                        </div>
                        <div className="col-sm-12 col-md-3 mt-3 mt-md-0">
                            <Dashcard link='/transactions' number={this.state.monthlyTotals.transactions} background='danger' infoText='Transactions' />
                        </div>

                        <div className="col-sm-12 col-md-3 mt-3 mt-md-0">
                            <Dashcard link='/transactions' number={'$' + this.state.monthlyTotals.income} background='success' infoText='Income Made' />
                        </div>
                    </div>

                    <div className="row mt-5">
                        <div className="col-sm-12 col-md-9">
                            <div className="card">
                                <div className="card-body">
                                    <h5 className="card-title text-primary">Transactions this month</h5>
                                    <TransactionGraph/>
                                </div>
                            </div>
                        </div>
                        <div className="col-sm-12 col-md-3">
                            <div className="card">
                                <div className="card-body">
                                    <h5 className="card-title text-primary">Most Recent Transactions</h5>
                                </div>
                                <TransactionList transactions='last30days'/>
                            </div>
                        </div>
                    </div>

                    <div className="row mt-5">
                        <div className="col-sm-12 col-md-6">
                            <div className="card">
                                <div className="card-body">
                                    <h5 className="card-title text-primary">Today's Appointemts</h5>
                                </div>
                                <AppointmentManager timespan='day' datespanOption={false}/>
                            </div>
                        </div>
                        <div className="col-sm-12 col-md-6">
                            <div className="card">
                                <div className="card-body">
                                    <h5 className="card-title text-primary">Most Recent Clients</h5>
                                </div>
                                <ClientList/>
                            </div>
                        </div>
                    </div>
                </div>
            </AppLayout> 
        )
    }
}
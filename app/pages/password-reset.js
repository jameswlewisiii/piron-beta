import Layout from '../components/layout/main'
import Router from 'next/router'
import IronApi from '../modules/IronApi';

export default class PasswordReset extends React.Component {
    async handleSubmit(event) {
        event.preventDefault()
        let iron = new IronApi;
        let email = document.getElementById('email').value
        let res = await iron.resetPassword(email)
        if(res.data.success) {
            Router.push('/login')
        }
    }
    render() {
        return (
            <Layout>
                <div className="container mt-5">
                    <div className="row">
                        <div className="col-md-6 offset-md-3">
                            <div className="card">
                            <div className="card-header">
                                Password Reset
                            </div>
                            <div className="card-body">
                                <h5 className="card-title">Reset your current password</h5>
                                <div id="error-message" className="text-danger"></div>
                                <form onSubmit={this.handleSubmit}>
                                    <div className="form-group">
                                        <label >Email address</label>
                                        <input type="email" className="form-control" id="email" aria-describedby="emailHelp" placeholder="Enter email" required/>
                                        <small id="emailHelp" className="form-text text-muted">You will receive a temporary password to the email provided.</small>
                                    </div>
                                    <button type="submit" className="btn btn-primary w-100">Reset My Password</button>
                                </form>
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
            </Layout>
        )
    }   
}
import Link from "next/link";
import Layout from "../components/layout/main";
import { library } from "@fortawesome/fontawesome-svg-core";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
    faChartLine,
    faCalendarAlt,
    faUsers,
    faFileContract
} from "@fortawesome/free-solid-svg-icons";
library.add([faUsers, faChartLine, faCalendarAlt, faFileContract]);

const Index = () => (
    <Layout title="Iron">
        <div className="jumbotron">
            <div className="container">
                <div className="row">
                    <div className="col-md-6 offset-md-3 text-center">
                        <h1 className="display-1 d-none d-md-inline">
                            Piron.io
                        </h1>
                        <h2>A Powerful Tattoo Studio Management Tool </h2>
                    </div>
                    <div className="col-6 offset-3 mt-3">
                        {/* <h2> <span className="bg-warning">About</span></h2> */}
                        {/* <p className="lead">
                            Built for tattoo artist and shop managers.
                            The tool is completely <span className="text-primary">free</span> to use.
                            It is currently in <span className="text-info">beta</span> at the moment while we iron out some of the wrinkles and add more features.
                        </p> */}
                        {/* <p className="lead">
                            If you enconter any type of error or have any suggestions 
                            on how to make the tool better don't hesitate to contact our
                             team at <a className="text-warning" href="mailto:jameswlewisiii@gmail.com">whatsup@piron.io</a>
                        </p> */}
                    </div>
                </div>
            </div>
        </div>
        {/* <div className="container">
            <div className="row mt-5 mb-5">
                <div className="col-md-4 col-sm-12 text-center">
                    <h1 className="text-info display-1">
                        <FontAwesomeIcon icon="chart-line"/>
                    </h1>
                    <p className="h3">
                        Expand Your Business 
                    </p>
                    <p className="h5 text-secondary">
                        Gain useful insight into your shop with a rich set of analytical charts, graphs and, data sets.
                    </p>
                </div>

                <div className="col-md-4 col-sm-12 text-center">
                    <h1 className="text-info display-1">
                        <FontAwesomeIcon icon="users"/>
                    </h1>
                    <p className="h3">
                        Keep Up With Your Clients
                    </p>
                    <p className="h5 text-secondary">
                        Know your client by staying up to date with important client details, appointments, and previous transactions with your personal cloud based client database.
                    </p>
                </div>

                <div className="col-md-4 col-sm-12 text-center">
                    <h1 className="text-info display-1">
                        <FontAwesomeIcon icon="file-contract"/>
                    </h1>
                    <p className="h3">
                        Manage Your Appointments
                    </p>
                    <p className="h5 text-secondary">
                        Stay orginaized with Piron's convenient appointment scheduler. View today's, next weeks, or the entire months appointments all in one place.
                    </p>
                </div>
            </div>
        </div> */}
        <div className="container">
            <div className="row p-3">
                <div className="col-md-4 col-sm-12">
                    <p className="h1">Expand your business</p>
                    <hr></hr>
                    <p className="h3 text-secondary">
                        Gain useful insight into your studio with a rich set of
                        analytical charts, graphs and, data sets.
                        {/* Visualize your data and make smart reactive business desitions */}
                    </p>
                </div>
                <div className="col-md-8 col-sm-12">
                    <img
                        src="/static/images/piron_graph.png"
                        alt="Piron Graph"
                        className="img img-fluid"
                    />
                </div>
            </div>
            <div className="row p-5"></div>
            <div className="row p-3">
                <div className="col-md-8 col-sm-12 d-none d-md-inline">
                    <img
                        src="/static/images/piron_clients.png"
                        alt="Piron Clients"
                        className="img img-fluid"
                    />
                </div>
                <div className="col-md-4 col-sm-12">
                    <p className="h1">Keep up with your clients</p>
                    <hr></hr>
                    <p className="h3 text-secondary">
                        Know your clients by staying up to date with important
                        details, appointments, and previous transactions. All
                        securely stored into a personal client database.
                    </p>
                </div>
                <div className="col-md-8 col-sm-12 d-md-none">
                    <img
                        src="/static/images/piron_clients.png"
                        alt="Piron Clients"
                        className="img img-fluid"
                    />
                </div>
            </div>
            <div className="row p-5"></div>
            <div className="row p-3">
                <div className="col-md-4 col-sm-12">
                    <p className="h1">Manage your appointments</p>
                    <hr></hr>
                    <p className="h3 text-secondary">
                        Stay orginaized with Piron's convenient appointment
                        scheduler. View today's, next weeks, or the entire
                        months appointments all in one place.
                    </p>
                </div>
                <div className="col-md-8 col-sm-12">
                    <img
                        src="/static/images/piron_appointment.png"
                        alt="Piron Appointment"
                        className="img img-fluid"
                    />
                </div>
            </div>
            <div className="row p-5"></div>
            <div className="row p-3">
                <div className="col-md-8 col-sm-12">
                    <img
                        src="/static/images/piron_dashboard.png"
                        alt="Piron Graph"
                        className="img img-fluid"
                    />
                </div>
                <div className="col-md-4 col-sm-12">
                    <p className="h1">Powerful dashboard</p>
                    <hr></hr>
                    <p className="h3 text-secondary">
                        All the important data on a single page. Piron's cutting
                        edge dashboard helps stuidos keep up with the performce
                        of their shop. With new clients, upcoming appointments,
                        and a monthly revenue chart, all on the dashboard.
                        {/* Visualize your data and make smart reactive business desitions */}
                    </p>
                </div>
            </div>
        </div>
        {/* <div className="jumbotron">
            <div className="row">
                <div className="col-6 offset-3 mt-3">
                        <h2> <span className="bg-warning">Upcoming Features</span></h2>
                    <p className="lead">
                        The following features on there way:
                    </p>
                    <p>
                        <ul>
                            <li>Analytic/Reporting tab - A page tab that provides more charts and insight into your shop.</li>
                            <li>Appointment Communication Channels - The ability to communication with a client about an appointment via Email or SMS.</li>
                            <li>Mulitple User Support - Support for having multiple users on the same account.</li>
                            <li>Mobile Application - An application for IOS and Android devices.</li>
                        </ul>
                    </p>
                    <p className="lead">
                        If you enconter any type of error or have any suggestions 
                        on how to make the tool better don't hesitate to contact our
                            team by visiting our <Link href="/feedback"><a className="text-warning" href="#">feedback</a></Link> page.
                    </p>
                </div>
            </div>
        </div> */}
        {/* <div className="jumbotron">
            <div className="container">
                <div className="row">
                    <div className="col-md-12 text-center">
                        <h2>
                            Simple plans hosted in the cloud.
                            <br />
                            Priced to scale.
                        </h2>
                    </div>
                    <div className="col-12 mt-3">
                        <div className="card-deck">
                            <div className="card">
                                <div className="bg-info">
                                    <div className="card-body">
                                        <h4 className="text-center">
                                            Freemium
                                        </h4>
                                        <h1 className="display-3 text-center mt-3">
                                            $0.00
                                            <span className="h4">/ month</span>
                                        </h1>
                                    </div>
                                </div>
                                <div className="card-body text-center">
                                    <h5 className="card-title m-4">
                                        Single User Account{" "}
                                        <FontAwesomeIcon icon="check" />
                                    </h5>
                                    <h5 className="card-title m-4">
                                        Unlimated client database{" "}
                                        <FontAwesomeIcon icon="check" />
                                    </h5>
                                    <h5 className="card-title m-4">
                                        Appointment manager{" "}
                                        <FontAwesomeIcon icon="check" />
                                    </h5>
                                    <h5 className="card-title m-4">
                                        Comprehensive Dashboard{" "}
                                        <FontAwesomeIcon icon="check" />
                                    </h5>
                                    <Link href="/register/freemium">
                                        <button className="btn btn-primary mt-4">
                                            Try it free
                                        </button>
                                    </Link>
                                </div>
                                <div className="card-footer">
                                    <small className="text-muted">
                                        No credit card is required for sign up.
                                    </small>
                                </div>
                            </div>
                            <div className="card">
                                <div className="bg-warning">
                                    <div className="card-body">
                                        <h4 className="text-center">Pro</h4>
                                        <h1 className="display-3 text-center mt-3">
                                            $2.99
                                            <span className="h4">/ month</span>
                                        </h1>
                                    </div>
                                </div>
                                <div className="card-body text-center">
                                    <h5 className="card-title m-4">
                                        30 day free trial{" "}
                                        <FontAwesomeIcon icon="check" />
                                    </h5>
                                    <h5 className="card-title m-4">
                                        All freemium features{" "}
                                        <FontAwesomeIcon icon="check" />
                                    </h5>
                                    <h5 className="card-title m-4">
                                        Unlimited User Accounts{" "}
                                        <FontAwesomeIcon icon="check" />
                                    </h5>
                                    <h5 className="card-title m-4">
                                        Access to all features{" "}
                                        <FontAwesomeIcon icon="check" />
                                    </h5>
                                    <h5 className="card-title m-4">
                                        Account support and help{" "}
                                        <FontAwesomeIcon icon="check" />
                                    </h5>
                                    <Link href="/register/pro">
                                        <button className="btn btn-primary mt-4">
                                            Try it free
                                        </button>
                                    </Link>
                                </div>
                                <div className="card-footer">
                                    <small className="text-muted">
                                        Credit card required. Payment will be
                                        billed monthly.
                                    </small>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div> */}

        <div className="container">
            <div className="row mt-5">
                <div className="col-md-6 offset-md-3 text-center">
                    <h2>It’s a better way to manage a tattoo stuido.</h2>
                    <Link href="/register/freemium">
                        <h1>
                            <a
                                className="btn btn-primary text-info mt-2"
                                href="#"
                            >
                                Register for a free account
                            </a>
                        </h1>
                    </Link>
                </div>
            </div>
        </div>
    </Layout>
);

export default Index;

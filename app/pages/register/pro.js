import Layout from "../../components/layout/main";
import Link from "next/link";
import { Elements, StripeProvider } from "react-stripe-elements";
import CheckoutForm from "../../components/stripe/checkoutForm";
import "../../css/stripe.css";
import IronApi from "../../modules/IronApi";

export default class ProRegister extends React.Component {
    constructor() {
        super();
        this.state = { stripe: null };
    }

    componentDidMount() {
        if (window.Stripe) {
            this.setState({
                stripe: window.Stripe(IronApi.getStripeKey())
            });
        }
    }

    render() {
        return (
            <Layout>
                <div className="container mt-5">
                    <StripeProvider stripe={this.state.stripe}>
                        <Elements>
                            <div className="container mt-5">
                                <div className="row">
                                    <div className="col-sm-12 offset-sm-0 col-md-6">
                                        <h1>Pro Plan</h1>
                                        <hr></hr>
                                        <p>
                                            The Pro Plan offers unlimited users
                                            which makes it great for tattoo
                                            shops/studios.
                                        </p>

                                        <p>
                                            Invoice will be automatically billed
                                            at the end of the 30 day free trial.
                                        </p>

                                        <div className="card">
                                            <div className="card-body">
                                                <div className="card-title">
                                                    <h5>Summary</h5>
                                                </div>
                                                <p>
                                                    1 X Pro Plan - Unlimited
                                                    Users
                                                </p>
                                                <span>Total</span>
                                                <span className="float-right text-primary">
                                                    $2.99 / month
                                                </span>
                                            </div>
                                        </div>
                                        <img
                                            src="/static/images/secure-stripe-payment-logo.png"
                                            alt="Piron Graph"
                                            className="img img-fluid"
                                        />
                                    </div>
                                    <div className="col-sm-12 offset-sm-0 col-md-6">
                                        <div className="card">
                                            <div className="card-body">
                                                <CheckoutForm />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </Elements>
                    </StripeProvider>
                </div>
            </Layout>
        );
    }
}

import Layout from "../../components/layout/main";
import Link from "next/link";
import IronApi from "../../modules/IronApi";

export default class FreemiumRegister extends React.Component {
    async handleSubmit(event) {
        event.preventDefault();

        let iron = new IronApi();
        let res = await iron.registerFreemium({
            email: document.getElementById("email").value,
            password: document.getElementById("password").value,
            first_name: document.getElementById("first_name").value,
            last_name: document.getElementById("last_name").value
        });

        if (res.data.token) {
            window.location.href = "/dashboard";
        } else {
            document.getElementById("error-message").innerText =
                res.data.message;
        }
    }

    render() {
        return (
            <Layout>
                <div className="container mt-5">
                    <div className="row">
                        <div className="col-sm-12 offset-sm-0 col-md-6 offset-md-3">
                            <div className="card">
                                <div className="card-header">Register</div>
                                <div className="card-body">
                                    <h5 className="card-title">
                                        Create an account.
                                    </h5>
                                    <p className="card-text">
                                        It takes less than five minutes to
                                        register. No credit card, no email spam,
                                        no hassle. Enter a name, an email, a
                                        password, and click register. You will
                                        have access to the Piron tattoo
                                        management tool.
                                    </p>
                                    <p className="card-text">
                                        Already have an account? Login{" "}
                                        <Link href="/login">
                                            <a> here</a>
                                        </Link>
                                        .
                                    </p>
                                    <div
                                        id="error-message"
                                        className="text-danger"
                                    ></div>
                                    <form onSubmit={this.handleSubmit}>
                                        User Information
                                        <hr></hr>
                                        <div className="form-group">
                                            <input
                                                type="text"
                                                name="first_name"
                                                className="form-control"
                                                id="first_name"
                                                placeholder="First Name *"
                                                required
                                            />
                                        </div>
                                        <div className="form-group">
                                            <input
                                                type="text"
                                                name="last_name"
                                                className="form-control"
                                                id="last_name"
                                                placeholder="Last Name *"
                                                required
                                            />
                                        </div>
                                        <div className="form-group">
                                            <input
                                                type="email"
                                                name="email"
                                                className="form-control"
                                                id="email"
                                                placeholder="Email *"
                                                required
                                            />
                                        </div>
                                        <div className="form-group">
                                            <input
                                                type="password"
                                                name="password"
                                                className="form-control"
                                                id="password"
                                                placeholder="Password *"
                                                required
                                            />
                                        </div>
                                        {/* Account Information
                                    <hr></hr>
                                    <div className="form-group">
                                        <input type="text" name="name" className="form-control" id="name" placeholder="Shop/Studio Name"/>
                                    </div>
                                    <div className="form-group">
                                        <input type="text" name="address1" className="form-control" id="address1" placeholder="Address 1"/>
                                    </div>
                                    <div className="form-group">
                                        <input type="text" name="address2" className="form-control" id="address2" placeholder="Address 2"/>
                                    </div>
                                    <div className="form-group">
                                        <input type="text" name="city" className="form-control" id="address2" placeholder="City"/>
                                    </div>
                                    <div className="form-group">
                                        <input type="text" name="state" className="form-control" id="state" placeholder="State"/>
                                    </div>
                                    <div className="form-group">
                                        <input type="text" name="zip" className="form-control" id="zip" placeholder="Zip Code"/>
                                    </div>
                                    <div className="form-group">
                                        <input type="text" name="phone" className="form-control" id="phone" placeholder="Phone"/>
                                    </div>
                                    <div className="form-group">
                                        <input type="website" name="shop_name" className="form-control" id="website" placeholder="Website"/>
                                    </div>
                                    <div className="form-group">
                                        <input type="text" name="facebook" className="form-control" id="facebook" placeholder="Facebook Page"/>
                                    </div> */}
                                        <button
                                            type="submit"
                                            className="btn btn-primary w-100"
                                        >
                                            Register
                                        </button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </Layout>
        );
    }
}

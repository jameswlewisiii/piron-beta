import IronApi from '../modules/IronApi'
import Router from 'next/router'
import Layout from '../components/layout/main'

export default class Logout extends React.Component {
    componentDidMount() {
        if(IronApi.authenticated()) {
            let iron = new IronApi
            iron.logout()
        }
        Router.push('/')
    }
    render() {
        return (
            <Layout>
                <div className="container mt-5">
                    <div className="row">
                        <div className="col-12 text-center">
                            <p className="display-4">Thank you for using Piron.io</p>
                            <p className="lead">
                                Loging you out...
                            </p>
                        </div>
                    </div>
                </div>
            </Layout>
        )
    }
  }
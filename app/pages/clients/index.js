import AppLayout from "../../components/layout/appLayout";
import ReactTable from "react-table";
import Link from "next/link";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import IronApi from "../../modules/IronApi";
import TopInfo from "../../components/topInfo";

export default class Index extends React.Component {
    breadcrumbs = [
        {
            href: "/dashboard",
            name: "Dashboard"
        },
        {
            href: "/clients",
            name: "Clients",
            active: true
        }
    ];
    columns = [
        {
            id: "fullName",
            Header: "Full Name",
            accessor: c => {
                return (
                    <Link href={`/clients/view?id=${c.id}`}>
                        <a>
                            {c.first_name} {c.last_name}
                        </a>
                    </Link>
                );
            },
            filterMethod: (filter, row) => {
                let name =
                    row._original.first_name + " " + row._original.last_name;
                return name.search(filter.value) !== -1;
            },
            filterable: true
        },
        {
            id: "email",
            Header: "Email",
            accessor: "email",
            filterable: true
        },
        {
            Header: "Phone",
            accessor: "phone",
            filterable: true
        },
        {
            id: "address",
            Header: "Address",
            accessor: c => c.address1,
            filterable: true
        },
        {
            Header: "City",
            accessor: "city",
            filterable: true
        },
        {
            Header: "State",
            accessor: "state",
            filterable: true
        },
        {
            id: "options",
            Header: "Options",
            accessor: c => {
                if (IronApi.isCurrentUserAdmin() || this.state.userView) {
                    return (
                        <div>
                            <Link href={`/clients/view?id=${c.id}`}>
                                <button className="btn btn-sm btn-info ">
                                    <FontAwesomeIcon icon="eye" />
                                </button>
                            </Link>
                            <Link href={`/clients/edit?id=${c.id}`}>
                                <button className="btn btn-sm btn-warning">
                                    <FontAwesomeIcon icon="edit" />
                                </button>
                            </Link>
                            <button
                                className="btn btn-sm btn-danger"
                                onClick={() => this.deleteClient(c.id)}
                            >
                                <FontAwesomeIcon icon="trash" />
                            </button>
                        </div>
                    );
                } else {
                    return (
                        <div>
                            <Link href={`/clients/view?id=${c.id}`}>
                                <button className="btn btn-sm btn-info ">
                                    <FontAwesomeIcon icon="eye" />
                                </button>
                            </Link>
                        </div>
                    );
                }
            }
        }
    ];
    options = {};
    state = {
        userView: false,
        accountView: true,
        clients: []
    };
    componentDidMount() {
        this.iron = new IronApi();
        this.getClients();
    }
    async getClients() {
        let response = await this.iron.getClients();
        this.setState({
            clients: response.data.clients,
            userView: false,
            accountView: true
        });
    }
    async deleteClient(clientId) {
        let res = await this.iron.deleteClient(clientId);
        this.getClients();
    }
    async handleUserView() {
        let response = await this.iron.getUserClients();
        this.setState({
            clients: response.data.clients,
            userView: true,
            accountView: false
        });
    }
    render() {
        const { clients } = this.state;

        return (
            <AppLayout>
                <TopInfo
                    title="Clients"
                    userViewToggle={true}
                    breadcrumbs={this.breadcrumbs}
                    userViewFunc={this.handleUserView.bind(this)}
                    accountViewFunc={this.getClients.bind(this)}
                    createHref="/clients/new"
                />
                <div className="row">
                    <div className="col-12">
                        <div className="card">
                            <div className="body">
                                <ReactTable
                                    data={clients}
                                    columns={this.columns}
                                />
                            </div>
                        </div>
                    </div>
                </div>
            </AppLayout>
        );
    }
}

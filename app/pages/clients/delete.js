import AppLayout from '../../components/layout/appLayout'
import IronApi from '../../modules/IronApi'
import Router from 'next/router'

export default class DeleteClient extends React.Component {

    static getInitialProps({query}) {
        return {query}
    }
    async componentDidMount() {
        let iron = new IronApi
        await iron.deleteClient(this.props.query.id)
        Router.push('/clients/')
    }
    render() {
        return (
            <AppLayout>                
                Deleteing Client....
            </AppLayout> 
        )
    }
}
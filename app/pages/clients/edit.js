import AppLayout from '../../components/layout/appLayout'
import TopInfo from '../../components/topInfo'
import ClientForm from '../../components/clients/clientForm'

export default class EditClient extends React.Component {
    breadcrumbs = [
        {
            href: '/dashboard/',
            name: 'Dashboard',
        },
        {
            href: '/clients',
            name: 'Clients',
        },
        {
            href: '/clients/edit',
            name: 'Edit',
            active: true
        }
    ]
    static getInitialProps({query}) {
        return {query}
    }
    render() {
        return (
            <AppLayout>     
                <TopInfo 
                    title="Edit Client" 
                    breadcrumbs={this.breadcrumbs}
                />
                <div className="row">
                    <div className="col-12">
                        <div className="card">
                            <div className="card-body">
                                {this.props.query && <ClientForm clientId={this.props.query.id} />}
                            </div>
                        </div>
                    </div>
                </div>
            </AppLayout> 
        )
    }
}

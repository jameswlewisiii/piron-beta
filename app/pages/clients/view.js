import AppLayout from "../../components/layout/appLayout";
import IronApi from "../../modules/IronApi";
import Link from "next/link";
import TopInfo from "../../components/topInfo";
import ClientCard from "../../components/clients/clientCard";
import AppointmentList from "../../components/appointments/appointmentList";
import TransactionList from "../../components/transactions/transactionList";

export default class ViewClient extends React.Component {
    breadcrumbs = [
        {
            href: "/dashboard/",
            name: "Dashboard"
        },
        {
            href: "/clients",
            name: "Clients"
        },
        {
            href: "/clients/view",
            name: "View",
            active: true
        }
    ];
    state = {
        error: false,
        errorMessage: "A wild error has appear.",
        client: null
    };
    static getInitialProps({ query }) {
        return { query };
    }
    async componentDidMount() {
        this.loadClient();
    }
    async loadClient() {
        let id = this.props.query.id;
        let iron = new IronApi();
        let res = await iron.getClient(id);
        let client = res.data.client;
        this.setState({ client: client });
    }
    render() {
        let client = this.state.client;
        let transactionList = "";

        return (
            <AppLayout>
                {client && (
                    <TopInfo
                        title={client.first_name + " " + client.last_name}
                        breadcrumbs={this.breadcrumbs}
                        editHref={`/clients/edit?id=${client.id}`}
                        deleteHref={`/clients/delete?id=${client.id}`}
                    />
                )}
                <div className="row">
                    <div className="col-md-12 col-lg-4">
                        <ClientCard
                            client={client}
                            title="Client"
                            textColor="text-primary"
                        />
                    </div>

                    <div className="col-md-12 col-lg-8">
                        <div className="card border-light">
                            <div className="card border-light">
                                <div className="card-header text-primary">
                                    Appointments History
                                </div>
                                {client && (
                                    <AppointmentList
                                        appointments={client.appointments}
                                    />
                                )}
                            </div>
                        </div>
                        {client && (
                            <div className="card border-light mt-3">
                                <div className="card-header text-primary">
                                    Transactions History
                                </div>
                                {client && (
                                    <TransactionList
                                        transactions={client.transactions}
                                    />
                                )}
                            </div>
                        )}
                    </div>
                </div>
            </AppLayout>
        );
    }
}

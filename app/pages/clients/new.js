import AppLayout from '../../components/layout/appLayout'
import ClientForm from '../../components/clients/clientForm'
import TopInfo from '../../components/topInfo'

export default class NewClient extends React.Component {
    breadcrumbs = [
        {
            href: '/dashboard/',
            name: 'Dashboard',
        },
        {
            href: '/clients',
            name: 'Clients',
        },
        {
            href: '/clients/new',
            name: 'New',
            active: true
        }
    ]
    render() {
        return (
            <AppLayout>                
                <TopInfo 
                    title="Add a new client" 
                    breadcrumbs={this.breadcrumbs}
                />
                <div className="row">
                    <div className="col-12">
                        <div className="card">
                            <div className="card-body">
                                <ClientForm/>
                            </div>
                        </div>
                    </div>
                </div>
            </AppLayout> 
        )
    }
}
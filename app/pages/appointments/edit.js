import AppLayout from '../../components/layout/appLayout'
import TopInfo from '../../components/topInfo'
import AppointmentForm from '../../components/appointments/appointmentForm'

export default class EditAppointment extends React.Component {
    breadcrumbs = [
        {
            href: '/dashboard/',
            name: 'Dashboard',
        },
        {
            href: '/appointments',
            name: 'Appointments',
        },
        {
            href: '/appointments/edit',
            name: 'Edit',
            active: true
        }
    ]
    static getInitialProps({query}) {
        return {query}
    }
    render() {
        return (
            <AppLayout>     
                <TopInfo 
                    title="Edit Appointment" 
                    breadcrumbs={this.breadcrumbs}
                />
                <div className="row">
                    <div className="col-12">
                        <div className="card">
                            <div className="card-body">
                                <AppointmentForm appointmentId={this.props.query.id}/>
                            </div>
                        </div>
                    </div>
                </div>
            </AppLayout> 
        )
    }
}

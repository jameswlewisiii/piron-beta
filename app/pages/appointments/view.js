import AppLayout from "../../components/layout/appLayout";
import Link from "next/link";
import IronApi from "../../modules/IronApi";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import TopInfo from "../../components/topInfo";
import ClientCard from "../../components/clients/clientCard";
import ViewHelper from "../../modules/ViewHelper";

export default class ViewAppointment extends React.Component {
    breadcrumbs = [
        {
            href: "/dashboard/",
            name: "Dashboard"
        },
        {
            href: "/appointments",
            name: "Appointments"
        },
        {
            href: "/appointments/view",
            name: "View",
            active: true
        }
    ];
    state = {
        error: false,
        errorMessage: "A wild error has appear.",
        appointment: null
    };
    static getInitialProps({ query }) {
        return { query };
    }
    async componentDidMount() {
        this.loadAppointment();
    }
    async loadAppointment() {
        let id = this.props.query.id;
        let iron = new IronApi();
        let res = await iron.getAppointment(id);
        let appointment = res.data.appointment;
        this.setState({ appointment: appointment });
    }
    getAppointmentDate() {
        let date = new Date(this.state.appointment.date);
        return (
            date.toLocaleString("en-us", { month: "long" }) +
            " " +
            date.getUTCDate() +
            ", " +
            date.getFullYear()
        );
    }
    render() {
        let appointment = null;
        let client = null;
        let clientFullNameLink = null;
        if (this.state.appointment) {
            appointment = this.state.appointment;
            client = appointment.client;
            clientFullNameLink = (
                <Link href={`/clients/view?id=${client.id}`}>
                    <h4 className="card-title">
                        <a href={`/clients/view?id=${client.id}`}>
                            {client.first_name + " " + client.last_name}
                        </a>
                    </h4>
                </Link>
            );
        }

        return (
            <AppLayout>
                {appointment && (
                    <TopInfo
                        title={
                            appointment.notes +
                            " - " +
                            client.first_name +
                            " " +
                            client.last_name
                        }
                        breadcrumbs={this.breadcrumbs}
                        editHref={`/appointments/edit?id=${appointment.id}`}
                        deleteHref={`/appointments/delete?id=${appointment.id}`}
                    />
                )}
                <div className="row">
                    <div className="col-md-12 col-lg-3">
                        <div className="card text-primary mb-3">
                            <div className="card-header">Appointment </div>
                            <div className="card-body">
                                <h4 className="card-title">
                                    {appointment && appointment.notes}
                                </h4>
                                <hr />
                                <p className="card-text">
                                    <FontAwesomeIcon icon="calendar" />{" "}
                                    {appointment && this.getAppointmentDate()}
                                </p>
                                <p className="card-text">
                                    <FontAwesomeIcon icon="hourglass-start" />{" "}
                                    {appointment && appointment.start_time}
                                </p>
                                <p className="card-text">
                                    <FontAwesomeIcon icon="hourglass-end" />{" "}
                                    {appointment && appointment.end_time}
                                </p>
                                <p>
                                    Created By:{" "}
                                    {appointment &&
                                        ViewHelper.getCreatedByLink(
                                            appointment.user
                                        )}
                                </p>
                            </div>
                        </div>
                    </div>

                    <div className="col-md-12 col-lg-9">
                        {client && (
                            <ClientCard
                                client={client}
                                title="Client"
                                textColor="text-primary"
                                link="true"
                            />
                        )}
                    </div>
                </div>
            </AppLayout>
        );
    }
}

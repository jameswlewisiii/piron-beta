import AppLayout from "../../components/layout/appLayout";
import TopInfo from "../../components/topInfo";
import ReactTable from "react-table";
import IronApi from "../../modules/IronApi";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Link from "next/link";
import Calendar from "../../components/appointments/calendar";

export default class AppointmentsIndex extends React.Component {
    breadcrumbs = [
        {
            href: "/dashboard/",
            name: "Dashboard"
        },
        {
            href: "/appointmetns",
            name: "Appointments",
            active: true
        }
    ];
    state = {
        userView: false,
        accountView: true,
        appointment: [],
        tableView: false
    };
    columns = [
        {
            id: "notes",
            Header: "Name",
            accessor: a => {
                return (
                    <Link href={`/appointments/view?id=${a.id}`}>
                        <a>{a.notes}</a>
                    </Link>
                );
            },
            filterMethod: (filter, row) => {
                let name = row._original.notes;
                return name.search(filter.value) !== -1;
            },
            filterable: true
        },
        {
            id: "date",
            Header: "Date",
            accessor: d => {
                let date = new Date(d.date);
                return (
                    date.toLocaleString("en-us", { month: "long" }) +
                    " " +
                    date.getUTCDate() +
                    ", " +
                    date.getFullYear()
                );
            },
            filterable: true
        },
        {
            id: "time",
            Header: "Time",
            accessor: a => a.start_time + " - " + a.end_time,
            filterable: true
        },
        {
            id: "clientName",
            Header: "Client's Name",
            accessor: a => a.client.first_name + " " + a.client.last_name,
            filterable: true
        },
        {
            id: "options",
            Header: "Options",
            accessor: a => {
                if (IronApi.isCurrentUserAdmin() || this.state.userView) {
                    return (
                        <div>
                            <Link href={`/appointments/view?id=${a.id}`}>
                                <button className="btn btn-sm btn-info ">
                                    <FontAwesomeIcon icon="eye" />
                                </button>
                            </Link>
                            <Link href={`/appointments/edit?id=${a.id}`}>
                                <button className="btn btn-sm btn-warning">
                                    <FontAwesomeIcon icon="edit" />
                                </button>
                            </Link>
                            <button
                                className="btn btn-sm btn-danger"
                                onClick={() => this.deleteAppointment(a.id)}
                            >
                                <FontAwesomeIcon icon="trash" />
                            </button>
                        </div>
                    );
                } else {
                    return (
                        <Link href={`/appointments/view?id=${a.id}`}>
                            <button className="btn btn-sm btn-info ">
                                <FontAwesomeIcon icon="eye" />
                            </button>
                        </Link>
                    );
                }
            }
        }
    ];
    async componentDidMount() {
        this.iron = new IronApi();
        this.getAppointments();
    }
    async getAppointments() {
        let res = await this.iron.getAppointments(null, null);
        this.setState({
            appointments: res.data.appointments,
            userView: false,
            accountView: true
        });
    }
    async handleUserView() {
        let response = await this.iron.getUserAppointments();
        this.setState({
            appointments: response.data.appointments,
            userView: true,
            accountView: false
        });
    }
    async deleteAppointment(id) {
        let response = await this.iron.deleteAppointment(id);
        this.getAppointments();
    }
    render(props) {
        return (
            <AppLayout>
                <TopInfo
                    title="Appointments"
                    breadcrumbs={this.breadcrumbs}
                    createHref="/appointments/new"
                    // settingsHref="/appointments/new"
                    userViewToggle={true}
                    userViewFunc={this.handleUserView.bind(this)}
                    accountViewFunc={this.getAppointments.bind(this)}
                />
                <div className="row">
                    <div className="col-12">
                        <div className="card">
                            <ul className="nav nav-tabs">
                                <li className="nav-item dropdown">
                                    <a
                                        className="nav-link dropdown-toggle"
                                        data-toggle="dropdown"
                                        href="#"
                                        role="button"
                                        aria-haspopup="true"
                                        aria-expanded="false"
                                    >
                                        Appoitnment View
                                    </a>
                                    <div className="dropdown-menu">
                                        <a
                                            className="dropdown-item"
                                            href="#"
                                            onClick={() => {
                                                this.setState({
                                                    tableView: false
                                                });
                                            }}
                                        >
                                            Calendar
                                        </a>
                                        <a
                                            className="dropdown-item"
                                            href="#"
                                            onClick={() => {
                                                this.setState({
                                                    tableView: true
                                                });
                                            }}
                                        >
                                            Table
                                        </a>
                                    </div>
                                </li>
                            </ul>
                            {this.state.tableView ? (
                                <ReactTable
                                    data={this.state.appointments}
                                    columns={this.columns}
                                />
                            ) : (
                                <Calendar />
                            )}
                        </div>
                    </div>
                </div>
            </AppLayout>
        );
    }
}

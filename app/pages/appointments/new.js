import AppLayout from '../../components/layout/appLayout'
import Breadcrumbs from '../../components/breadcrumbs'
import AppointmentForm from '../../components/appointments/appointmentForm'

export default class NewAppointment extends React.Component {
    breadcrumbs = [
        {
            href: '/dashboard/',
            name: 'Dashboard',
        },
        {
            href: '/appointments',
            name: 'Appointments',
        },
        {
            href: '/appointments/new',
            name: 'New',
            active: true
        }
    ]
    render() {
        return (
            <AppLayout>                
                <h1>Add A New Appointment</h1>
                <div className="row"> 
                    <Breadcrumbs crumbs={this.breadcrumbs}/>
                </div>
                <div className="row">
                    <div className="col-12">
                        <div className="card">
                            <div className="card-body">
                                <AppointmentForm/>
                            </div>
                        </div>
                    </div>
                </div>
            </AppLayout> 
        )
    }
}
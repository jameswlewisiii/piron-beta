import AppLayout from '../../components/layout/appLayout'
import IronApi from '../../modules/IronApi'
import Router from 'next/router'

export default class DeleteAppointment extends React.Component {

    static getInitialProps({query}) {
        return {query}
    }
    async componentDidMount() {
        let iron = new IronApi
        await iron.deleteAppointment(this.props.query.id)
        Router.push('/appointments/')
    }
    render() {
        return (
            <AppLayout>                
                Deleteing Appointment....
            </AppLayout> 
        )
    }
}
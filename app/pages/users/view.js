import IronApi from "../../modules/IronApi";
import AppLayout from "../../components/layout/appLayout";
import TopInfo from "../../components/topInfo";
import AppointmentList from "../../components/appointments/appointmentList";
import TransactionList from "../../components/transactions/transactionList";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

export default class UsersView extends React.Component {
    state = {
        user: null
    };
    breadcrumbs = [
        {
            href: "/dashboard",
            name: "Dashboard"
        },
        {
            href: "/users",
            name: "Users",
            active: true
        },
        {
            href: "/view",
            name: "View",
            active: true
        }
    ];
    static getInitialProps({ query }) {
        return { query };
    }
    componentDidMount() {
        this.loadUser();
    }
    async loadUser() {
        let iron = new IronApi();
        let user = await iron.getUser(this.props.query.id);
        this.setState({
            user: user.data.user
        });
    }
    render() {
        let user = this.state.user;

        return (
            <AppLayout>
                <TopInfo title="Manage Users" breadcrumbs={this.breadcrumbs} />
                <div className="row">
                    <div className="col-md-12 col-lg-4">
                        <div className="card">
                            <div className="card-header">User</div>
                            <div className="card-body">
                                <h1 className="card-title">
                                    {user &&
                                        user.first_name + " " + user.last_name}
                                </h1>
                                <hr />
                                <p className="card-text">
                                    <FontAwesomeIcon icon="envelope" />{" "}
                                    {user && user.email}
                                </p>
                            </div>
                        </div>
                    </div>

                    <div className="col-md-12 col-lg-8">
                        <div className="card border-light">
                            <div className="card border-light">
                                <div className="card-header text-primary">
                                    Appointments History
                                </div>
                                {user && (
                                    <AppointmentList
                                        appointments={user.appointments}
                                    />
                                )}
                            </div>
                        </div>
                        {user && (
                            <div className="card border-light mt-3">
                                <div className="card-header text-primary">
                                    Transactions History
                                </div>

                                {user && (
                                    <TransactionList
                                        transactions={user.transactions}
                                    />
                                )}
                            </div>
                        )}
                    </div>
                </div>
            </AppLayout>
        );
    }
}

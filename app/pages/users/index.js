import ErrorNotification from "../../components/errorNotification";
import IronApi from "../../modules/IronApi";
import Link from "next/link";
import AppLayout from "../../components/layout/appLayout";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import TopInfo from "../../components/topInfo";
import ReactTable from "react-table";

export default class UsersIndex extends React.Component {
    breadcrumbs = [
        {
            href: "/dashboard",
            name: "Dashboard"
        },
        {
            href: "/users",
            name: "Users",
            active: true
        }
    ];
    userColumns = [
        {
            id: "fullName",
            Header: "Full Name",
            accessor: user => {
                let name =
                    user.first_name === null
                        ? "Name not set"
                        : user.first_name + " " + user.last_name;
                return (
                    <Link href={`/users/view?id=${user.id}`}>
                        <a>{name}</a>
                    </Link>
                );
            },
            filterable: true
        },
        {
            id: "email",
            Header: "Email",
            accessor: "email",
            filterable: true
        },
        {
            id: "role",
            Header: "Role",
            accessor: user => {
                return user.role_id == IronApi.getAdminRoleId()
                    ? "Admin"
                    : "User";
            }
        },
        {
            id: "options",
            Header: "Options",
            accessor: user => {
                return (
                    <div>
                        <button
                            className="btn btn-sm btn-info"
                            onClick={() =>
                                this.changeRole(user.id, user.role_id)
                            }
                        >
                            Change Role
                        </button>
                    </div>
                );
            }
        }
    ];
    state = {
        users: [],
        setup: false,
        menu: {
            users: 1
        }
    };

    async changeRole(userId, currentRoleId) {
        // Toggle user id
        let newRoleId =
            currentRoleId == IronApi.getAdminRoleId()
                ? IronApi.getUserRoleId()
                : IronApi.getAdminRoleId();

        let iron = new IronApi();
        let response = await iron.changeUserRole(userId, newRoleId);
        if (response.data.success === true) {
            this.loadData();
        } else {
            alert("ERROR");
        }
    }

    async removeUserFromAccount(id) {
        let iron = new IronApi();
        let response = await iron.removeUserFromAccount(id);
        if (response.data.success === true) {
            this.loadData();
        } else {
            alert("ERROR");
        }
    }

    async handleSubmit(event) {
        event.preventDefault();
        let iron = new IronApi();
        let response = await iron.createUser({
            first_name: document.getElementById("newUserFirstName").value,
            last_name: document.getElementById("newUserLastName").value,
            email: document.getElementById("newUserEmail").value
        });

        if (response.status === 201) {
            // Success Alert.
            $("#add-user-modal").modal("toggle");
            this.loadData();
        } else {
            this.setState({ errorMessage: response.message });
        }
    }
    componentDidMount() {
        this.handleSubmit = this.handleSubmit.bind(this);
        this.iron = new IronApi();
        this.loadData();
    }
    async loadData() {
        let users = await this.iron.getAccountUsers();
        this.setState({ users: users.data.users });
    }
    displayUsers = event => {
        this.setState({
            menu: {
                users: 1,
                invites: 0
            }
        });
    };
    render() {
        let nonActiveClass = "nav-link text-warning";
        let activeClass = nonActiveClass + " active";

        return (
            <AppLayout>
                <TopInfo
                    title="Manage Users"
                    breadcrumbs={this.breadcrumbs}
                    createHref="#"
                    toggle="modal"
                    target="#add-user-modal"
                />

                <div className="row">
                    <div className="col-12">
                        <div className="card">
                            <ul className="nav nav-tabs">
                                <li className="nav-item">
                                    <a
                                        href="#"
                                        id="users"
                                        className={
                                            this.state.menu.users
                                                ? activeClass
                                                : nonActiveClass
                                        }
                                        onClick={this.displayUsers}
                                    >
                                        Users
                                    </a>
                                </li>
                            </ul>
                            <div className="card-body">
                                {this.state.menu.users == 1 && (
                                    <ReactTable
                                        data={this.state.users}
                                        columns={this.userColumns}
                                    />
                                )}
                            </div>
                        </div>
                    </div>
                </div>

                <div
                    className="modal fade"
                    id="add-user-modal"
                    role="dialog"
                    aria-labelledby="add-user-modal"
                    aria-hidden="true"
                >
                    <div className="modal-dialog" role="document">
                        <div className="modal-content">
                            <div className="modal-header">
                                <h5
                                    className="modal-title"
                                    id="add-user-modalLabel"
                                >
                                    Add A User To Your Account
                                </h5>
                                <button
                                    type="button"
                                    className="close"
                                    data-dismiss="modal"
                                    aria-label="Close"
                                >
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div className="modal-body">
                                <p>Create a new users under your account.</p>
                                <ErrorNotification
                                    message={this.state.errorMessage}
                                />
                                <form onSubmit={this.handleSubmit}>
                                    <div className="form-group">
                                        <label>First Name</label>
                                        <input
                                            type="text"
                                            className="form-control"
                                            id="newUserFirstName"
                                            placeholder="First Name"
                                            required
                                        />
                                    </div>
                                    <div className="form-group">
                                        <label>Last Name</label>
                                        <input
                                            type="text"
                                            className="form-control"
                                            id="newUserLastName"
                                            placeholder="Last Name"
                                            required
                                        />
                                    </div>
                                    <div className="form-group">
                                        <label>Email address</label>
                                        <input
                                            type="email"
                                            className="form-control"
                                            id="newUserEmail"
                                            aria-describedby="emailHelp"
                                            placeholder="Enter email"
                                            required
                                        />
                                    </div>

                                    <div className="modal-footer">
                                        <button
                                            type="button"
                                            className="btn btn-secondary"
                                            data-dismiss="modal"
                                        >
                                            Close
                                        </button>
                                        <button
                                            type="submit"
                                            className="btn btn-primary"
                                        >
                                            Add User
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </AppLayout>
        );
    }
}

import React from 'react'
import Head from 'next/head'
import Layout from '../components/layout/main'
import { withRouter } from 'next/router'
import Link from 'next/link'

class ErrorPage extends React.Component {

  static propTypes() {
    return {
      errorCode: React.PropTypes.number.isRequired,
      url: React.PropTypes.string.isRequired
    }
  }

  static getInitialProps({res, xhr}) {
    const errorCode = res ? res.statusCode : (xhr ? xhr.status : null)
    return {errorCode}
  }

  render() {
    var response
    switch (this.props.errorCode) {
      case 200: // Also display a 404 if someone requests /_error explicitly
      case 404:
        response = (
          <Layout>
            <div className="jumbotron">
              <div className="container">
                <div className="row">
                  <div className="col-12 text-center">
                    <h1 className="pb-2"> A wild <span className="bg-danger">404</span> error has appeared</h1> 
                  </div>
                </div>
              </div>
            </div>
            <div className="container">
              <div className="row">
                <div className="col-sm-12 col-md-6 offset-md-3 mt-3">
                  <p className="lead">
                    The page you are looking for doesn't exist. 
                    You can do one of the following to make this error dispear.
                    <ul>
                        <li>Double check the url. There might be a typo</li>
                        <li>Click the back button in your browser</li>
                        <li>Click the large button that says "Run Away"</li>
                    </ul>
                  </p>
                  <div className="text-center">
                    <p className="">
                      <Link href="/">
                          <h1>
                              <a className="btn btn-primary text-warning" href="#">Run Away</a>
                          </h1>
                      </Link>
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </Layout>
        )
        break
    }

    return response
  }

}

export default withRouter(ErrorPage)
import AppLayout from '../../components/layout/appLayout'
import IronApi from '../../modules/IronApi'
import Router from 'next/router'

export default class DeleteTransaction extends React.Component {

    static getInitialProps({query}) {
        return {query}
    }
    async componentDidMount() {
        let iron = new IronApi
        await iron.deleteTransaction(this.props.query.id)
        Router.push('/transactions/')
    }
    render() {
        return (
            <AppLayout>                
                Deleteing Transaction....
            </AppLayout> 
        )
    }
}
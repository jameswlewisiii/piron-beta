import IronApi from '../../modules/IronApi'
import AppLayout from '../../components/layout/appLayout'
import TopInfo from '../../components/topInfo'
import TransactionCard from '../../components/transactions/transactionCard'
import ClientCard from '../../components/clients/clientCard'

export default class ViewTransaction extends React.Component {
    breadcrumbs = [
        {
            href: '/dashboard/',
            name: 'Dashboard',
        },
        {
            href: '/transactions',
            name: 'Transactions',
        },
        {
            href: '/transactions/view',
            name: 'View',
            active: true
        }
    ]
    state = {
        error: false,
        errorMessage: 'A wild error has appear.',
        transaction: null,
    }
    static getInitialProps({query}) {
        return {query}
    }
    async componentDidMount() {    
        this.loadClient()
    }
    async loadClient() {
        let id = this.props.query.id
        let iron = new IronApi
        let res = await iron.getTransaction(id)
        let transaction = res.data.transaction
        this.setState({transaction: transaction}) 
    }
    render() {
        let transaction = this.state.transaction
        let client = null
        if(transaction) {
            client = transaction.client
        }

        return (
            <AppLayout>                
                {
                    transaction &&
                    <TopInfo 
                        title={transaction.description} 
                        breadcrumbs={this.breadcrumbs}
                        editHref={`/transactions/edit?id=${transaction.id}`}
                        deleteHref={`/transactions/delete?id=${transaction.id}`}
                    />
                }
                <div className="row">
                    <div className="col-md-12 col-lg-3">
                        <TransactionCard 
                            transaction={transaction} 
                        />
                    </div>

                    <div className="col-md-12 col-lg-9">
                        {client && <ClientCard client={client} title="Client" textColor="text" link="true"/>}
                    </div>
                </div>
            </AppLayout> 
        )
    }
}
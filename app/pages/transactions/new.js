import AppLayout from '../../components/layout/appLayout'
import Notifcation from '../../components/notification'
import TopInfo from '../../components/topInfo'
import TransactionForm from '../../components/transactions/transactionForm'

export default class NewTransaction extends React.Component {
    breadcrumbs = [
        {
            href: '/dashboard/',
            name: 'Dashboard',
        },
        {
            href: '/transactions',
            name: 'Transaction',
        },
        {
            href: '/transaction/new',
            name: 'New',
            active: true
        }
    ]
    state = {
        error: false,
        errorMessage: 'A wild error has appear.',
        clients: null,
    }
    async componentDidMount() {
    }
    render() {
        let error = this.state.error
        let errorMsg = this.state.errorMessage
    
        return (
            <AppLayout>                
                <TopInfo 
                    title="Create A New Transaction"
                    breadcrumbs={this.breadcrumbs}
                />
                <div className="row">
                    <div className="col-12">
                        <div className="card">
                            <div className="card-body">
                                {error === true ? <Notifcation alert="danger" message={errorMsg}/>: ''}
                                <TransactionForm />
                            </div>
                        </div>
                    </div>
                </div>
            </AppLayout> 
        )
    }
}
import AppLayout from '../../components/layout/appLayout'
import TopInfo from '../../components/topInfo'
import TransactionForm from '../../components/transactions/transactionForm'

export default class EditTransaction extends React.Component {
    breadcrumbs = [
        {
            href: '/dashboard/',
            name: 'Dashboard',
        },
        {
            href: '/transactions',
            name: 'Transaction',
        },
        {
            href: '/transaction/edit',
            name: 'Edit',
            active: true
        }
    ]
    state = {
        transaction: null,

        clients: null,
    }
    static getInitialProps({query}) {
        return {query}
    }
    async componentDidMount() {  
    }
    render() {
        let {transaction, errorCode} = this.state

        return (
            <AppLayout>           
                <TopInfo 
                    title="Edit Transaction"
                    breadcrumbs={this.breadcrumbs}
                />
                <div className="row">
                    <div className="col-12">
                        <div className="card">
                            <div className="card-body">
                                <TransactionForm transactionId={this.props.query.id} />
                            </div>
                        </div>
                    </div>
                </div>
            </AppLayout> 
        )
    }
}

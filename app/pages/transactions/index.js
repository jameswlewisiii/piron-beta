import AppLayout from "../../components/layout/appLayout";
import ReactTable from "react-table";
import Link from "next/link";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import IronApi from "../../modules/IronApi";
import TopInfo from "../../components/topInfo";

export default class Index extends React.Component {
    breadcrumbs = [
        {
            href: "/dashboard",
            name: "Dashboard"
        },
        {
            href: "/transactions",
            name: "Transactions",
            active: true
        }
    ];
    columns = [
        {
            id: "description",
            Header: "Description",
            accessor: t => {
                return (
                    <Link href={`/transactions/view?id=${t.id}`}>
                        <a>{t.description}</a>
                    </Link>
                );
            },
            filterMethod: (filter, row) => {
                let desc = row._original.description;
                return desc.search(filter.value) !== -1;
            },
            filterable: true
        },
        {
            id: "amount",
            Header: "Amount",
            accessor: t => "$" + t.amount,
            filterable: true
        },
        {
            id: "date",
            Header: "Date",
            accessor: d => {
                let date = new Date(d.date);
                return (
                    date.toLocaleString("en-us", { month: "long" }) +
                    " " +
                    date.getUTCDate() +
                    ", " +
                    date.getFullYear()
                );
            },
            filterable: true
        },
        {
            id: "options",
            Header: "Options",
            accessor: t => {
                if (IronApi.isCurrentUserAdmin() || this.state.userView) {
                    return (
                        <div>
                            <Link href={`/transactions/view?id=${t.id}`}>
                                <button className="btn btn-sm btn-info ">
                                    <FontAwesomeIcon icon="eye" />
                                </button>
                            </Link>
                            <Link href={`/transactions/edit?id=${t.id}`}>
                                <button className="btn btn-sm btn-warning">
                                    <FontAwesomeIcon icon="edit" />
                                </button>
                            </Link>
                            <button
                                className="btn btn-sm btn-danger"
                                onClick={() => this.deleteTransaction(t.id)}
                            >
                                <FontAwesomeIcon icon="trash" />
                            </button>
                        </div>
                    );
                } else {
                    return (
                        <div>
                            <Link href={`/transactions/view?id=${t.id}`}>
                                <button className="btn btn-sm btn-info ">
                                    <FontAwesomeIcon icon="eye" />
                                </button>
                            </Link>
                        </div>
                    );
                }
            }
        }
    ];
    options = {};
    state = {
        userView: false,
        accountView: true,
        transactions: []
    };
    componentDidMount() {
        this.iron = new IronApi();
        this.getTransactions();
    }
    async getTransactions() {
        let response = await this.iron.getTransactions();
        this.setState({
            transactions: response.data.transactions,
            userView: false,
            accountView: true
        });
    }
    async deleteTransaction(clientId) {
        let res = await this.iron.deleteTransaction(clientId);
        this.getTransactions();
    }
    async handleUserView() {
        let response = await this.iron.getUserTransactions();
        this.setState({
            transactions: response.data.transactions,
            userView: true,
            accountView: false
        });
    }
    render() {
        const { transactions } = this.state;

        return (
            <AppLayout>
                <TopInfo
                    title="Transactions"
                    userViewToggle={true}
                    userViewFunc={this.handleUserView.bind(this)}
                    accountViewFunc={this.getTransactions.bind(this)}
                    breadcrumbs={this.breadcrumbs}
                    createHref="/transactions/new"
                />
                <div className="row">
                    <div className="col-12">
                        <div className="card">
                            <div className="body">
                                <ReactTable
                                    data={transactions}
                                    columns={this.columns}
                                />
                            </div>
                        </div>
                    </div>
                </div>
            </AppLayout>
        );
    }
}

import IronApi from "../modules/IronApi";
import Link from "next/link";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

export default class AppointmentManager extends React.Component {
    state = {
        appointments: null,
        timespan: null
    };
    async componentDidMount() {
        let date = new Date();
        let iron = new IronApi();
        let res = await iron.getAppointments(null, date);
        this.setState({
            appointments: res.data.appointments
        });
    }
    changeTimespan(span) {
        this.setState({ timespan: span });
        for (let link of document.getElementsByClassName("timespan-link")) {
            link.className = "nav-link timespan-link";
        }
        document.getElementById(span).className =
            "nav-link timespan-link active";
    }
    getWeek() {
        let today = new Date();
        let week = [];
        for (let i = 0; i < 7; i++) {
            week.push(
                new Date(
                    today.getFullYear(),
                    today.getMonth(),
                    today.getDate() + i
                )
            );
        }
        return week;
    }
    getMonth() {
        let today = new Date();
        let month = [];
        for (let i = 0; i < 30; i++) {
            month.push(
                new Date(
                    today.getFullYear(),
                    today.getMonth(),
                    today.getDate() + i
                )
            );
        }
        return month;
    }
    getDay() {
        return [new Date()];
    }
    getTimeRange(timespan) {
        switch (this.state.timespan) {
            case "week":
                timespan = this.getWeek();
                break;
            case "month":
                timespan = this.getMonth();
                break;
            case "day":
            default:
                timespan = this.getDay();
        }

        return timespan;
    }
    getAppointmentCountByDate(date) {
        let count = 0;

        if (this.state.appointments) {
            this.state.appointments.map((appointment, i) => {
                let appDate = new Date(appointment.date);
                if (appDate.getDate() === date.getDate()) {
                    count += 1;
                }
            });
        }

        return count;
    }
    sameDate(d1, d2) {
        if (d1.getFullYear !== d2.getFullYear) return false;
        if (d1.getUTCDate() !== d2.getUTCDate()) return false;
        if (d1.getMonth() !== d2.getMonth()) return false;

        return true;
    }
    getAppointmentsListings(date) {
        let listings = null;

        if (this.state.appointments) {
            listings = this.state.appointments.map((appointment, i) => {
                if (this.sameDate(new Date(appointment.date), date)) {
                    return (
                        <Link
                            href={`/appointments/view?id=${appointment.id}`}
                            key={i}
                        >
                            <a
                                href={`/appointments/view?id=${appointment.id}`}
                                className="list-group-item list-group-item-action"
                            >
                                <div className="row">
                                    <div className="col-12">
                                        <span className="badge badge-primary float-right">
                                            {appointment.start_time} -{" "}
                                            {appointment.end_time}
                                        </span>
                                        <h4>{appointment.notes}</h4>
                                        <p>
                                            <FontAwesomeIcon icon="user" />{" "}
                                            {appointment.client.first_name}{" "}
                                            {appointment.client.last_name}
                                        </p>
                                    </div>
                                </div>
                            </a>
                        </Link>
                    );
                } else {
                    return null;
                }
            });
        }

        return listings;
    }
    render() {
        let appointments = this.getTimeRange(this.props.timespan).map(
            (date, i) => {
                let appointmentListings = this.getAppointmentsListings(date);
                return (
                    <div key={i}>
                        <a className="list-group-item list-group-item-action active">
                            {date.toLocaleString("en-us", { month: "long" })}{" "}
                            {date.getDate()}, {date.getFullYear()}
                        </a>
                        {appointmentListings !== null && appointmentListings}
                    </div>
                );
            }
        );

        return (
            <div className="manager">
                <div className="list-group">
                    {this.props.datespanOption && (
                        <ul className="nav nav-tabs mb-1">
                            <li className="nav-item">
                                <a
                                    className="nav-link active timespan-link"
                                    id="day"
                                    href="#"
                                    onClick={() => {
                                        this.changeTimespan("day");
                                    }}
                                >
                                    Day
                                </a>
                            </li>
                            <li className="nav-item">
                                <a
                                    className="nav-link timespan-link"
                                    href="#"
                                    id="week"
                                    onClick={() => {
                                        this.changeTimespan("week");
                                    }}
                                >
                                    Week
                                </a>
                            </li>
                            <li className="nav-item">
                                <a
                                    className="nav-link timespan-link"
                                    href="#"
                                    id="month"
                                    onClick={() => {
                                        this.changeTimespan("month");
                                    }}
                                >
                                    Month
                                </a>
                            </li>
                        </ul>
                    )}
                    {appointments}
                </div>
            </div>
        );
    }
}

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
export default class ErrorNotification extends React.Component {
    state = {
        active: false
    };
    render() {
        let className = "alert alert-danger";

        if (this.props.message === undefined) {
            className += " d-none";
        }

        return (
            <div
                className={className}
                id="error-notification-component"
                role="alert"
            >
                <FontAwesomeIcon icon="exclamation-circle" />{" "}
                {this.props.message}
            </div>
        );
    }
}

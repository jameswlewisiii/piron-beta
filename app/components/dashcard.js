import Link from "next/link";
export default class Dashcard extends React.Component {
    render(props) {
        let { link, background, number, infoText } = this.props;
        let cardClass = "card text-white bg-" + background;
        return (
            <Link href={link}>
                <a href="#">
                    <div className={cardClass}>
                        <div className="card-body">
                            <h1 className="card-title">{number}</h1>
                            <h6 className="card-subtitle mb-2 text-primary">
                                {infoText}
                            </h6>
                        </div>
                    </div>
                </a>
            </Link>
        );
    }
}

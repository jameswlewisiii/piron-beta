import Link from "next/link";
export default class BreadCrumbs extends React.Component {
    render(props) {
        return (
            <ol className="breadcrumb">
                {this.props.crumbs.map((breadcrumb, index) => {
                    let className =
                        breadcrumb.active == true
                            ? "breadcrumb-item active"
                            : "breadcrumb-item";
                    if (breadcrumb.active) {
                        return (
                            <li className={className} key={breadcrumb.name}>
                                <a>{breadcrumb.name}</a>
                            </li>
                        );
                    } else {
                        return (
                            <li className={className} key={breadcrumb.name}>
                                <Link href={breadcrumb.href}>
                                    <a>{breadcrumb.name}</a>
                                </Link>
                            </li>
                        );
                    }
                })}
            </ol>
        );
    }
}

import IronApi from "../modules/IronApi";

export default class SupportForm extends React.Component {
    state = {
        errorMessage: null,
        successMessage: false
    };
    handleSubmit = event => {
        let user = IronApi.getUser();
        if (user === null) {
            alert("You must be logged in to submit feedback.");
            return;
        }
        event.preventDefault();
        let data = {
            first_name: document.getElementById("support-first_name").value,
            last_name: document.getElementById("support-last_name").value,
            email: document.getElementById("support-email").value,
            message: document.getElementById("support-feedback").value,
            proAccount: IronApi.isProAccount()
        };
        let iron = new IronApi();
        iron.sendFeedback(data);

        if (this.props.afterSubmit) {
            this.props.afterSubmit();
        }

        document.getElementById("feedback-form").reset();
    };
    render(props) {
        return (
            <form onSubmit={this.handleSubmit} id="feedback-form">
                <p className="text-danger">
                    {this.state.errorMessage &&
                        "Error: " + this.state.errorMessage[1]}
                </p>
                <div className="form-group row">
                    <label className="col-sm-2 col-form-label">
                        First Name
                    </label>
                    <div className="col-sm-10">
                        <input
                            type="text"
                            className="form-control"
                            id="support-first_name"
                            name="support-first_name"
                            placeholder="John"
                            required
                        />
                    </div>
                </div>
                <div className="form-group row">
                    <label className="col-sm-2 col-form-label">Last Name</label>
                    <div className="col-sm-10">
                        <input
                            type="text"
                            className="form-control"
                            id="support-last_name"
                            name="support-last_name"
                            placeholder="Smith"
                            required
                        />
                    </div>
                </div>
                <div className="form-group row">
                    <label className="col-sm-2 col-form-label">Email</label>
                    <div className="col-sm-10">
                        <input
                            type="email"
                            className="form-control"
                            id="support-email"
                            name="support-email"
                            placeholder="johnsmith@email.com"
                            required
                        />
                    </div>
                </div>
                <div className="form-group row">
                    <label className="col-sm-2 col-form-label">Feedback</label>
                    <div className="col-sm-10">
                        <textarea
                            className="form-control"
                            id="support-feedback"
                            name="support-feedback"
                            rows="3"
                            required
                        ></textarea>
                    </div>
                </div>
                <div className="form-group row container">
                    <button type="submit" className="btn btn-primary">
                        Submit
                    </button>
                </div>
            </form>
        );
    }
}

import Link from "next/link";
import Breadcrumbs from "./breadcrumbs";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import IronApi from "../modules/IronApi";

export default class TopInfo extends React.Component {
    state = {
        isProAccount: false,
        viewToggle: false
    };
    componentDidMount() {
        this.setState({
            isProAccount: IronApi.isProAccount()
        });
    }
    changeView() {
        if (this.state.viewToggle) {
            this.props.accountViewFunc();
        } else {
            this.props.userViewFunc();
        }

        this.setState({ viewToggle: !this.state.viewToggle });
    }
    getCreateBtn() {
        if (this.props.toggle) {
            return (
                <button
                    title="Send"
                    className="btn btn-sm btn-success"
                    data-toggle={this.props.toggle}
                    data-target={this.props.target}
                >
                    <FontAwesomeIcon icon="plus" /> Add User
                </button>
            );
        } else {
            return (
                <button title="Create" className="btn btn-sm btn-success">
                    <FontAwesomeIcon icon="plus" /> New
                </button>
            );
        }
    }
    getViews() {
        if (this.state.isProAccount) {
            return (
                <span>
                    {this.props.userViewToggle && (
                        <button
                            title="Click to change the view of data"
                            className={
                                this.state.viewToggle
                                    ? "btn btn-sm btn-primary disabled"
                                    : "btn btn-sm btn-primary"
                            }
                            id="data-view"
                            onClick={this.changeView.bind(this)}
                        >
                            {this.state.viewToggle
                                ? "Viewing User " + this.props.title
                                : "Viewing Account " + this.props.title}
                        </button>
                    )}
                </span>
            );
        }
    }
    render(props) {
        return (
            <div>
                <div className="row">
                    <div className="col-12">
                        <div className="float-right">
                            {this.getViews()}
                            {this.props.createHref && (
                                <Link href={this.props.createHref}>
                                    {this.getCreateBtn()}
                                </Link>
                            )}

                            {this.props.editHref &&
                                IronApi.isCurrentUserAdmin() && (
                                    <Link href={this.props.editHref}>
                                        <button
                                            title="edit"
                                            className="btn btn-sm btn-warning"
                                        >
                                            <FontAwesomeIcon icon="edit" /> Edit{" "}
                                        </button>
                                    </Link>
                                )}
                            {this.props.deleteHref &&
                                IronApi.isCurrentUserAdmin() && (
                                    <Link href={this.props.deleteHref}>
                                        <button
                                            title="delete"
                                            className="btn btn-sm btn-danger"
                                        >
                                            <FontAwesomeIcon icon="trash" />{" "}
                                            Delete{" "}
                                        </button>
                                    </Link>
                                )}
                            {this.props.settingsHref && (
                                <Link href={this.props.settingsHref}>
                                    <button
                                        title="settings"
                                        className="btn btn-sm btn-info"
                                    >
                                        <FontAwesomeIcon icon="cog" />
                                    </button>
                                </Link>
                            )}
                        </div>
                        <h1>{this.props.title}</h1>
                    </div>
                    <Breadcrumbs crumbs={this.props.breadcrumbs} />
                </div>
            </div>
        );
    }
}

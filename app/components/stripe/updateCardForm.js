import IronApi from "../../modules/IronApi";
import { CardElement, injectStripe } from "react-stripe-elements";

class UpdateCardForm extends React.Component {
    state = {
        errorMessage: null,
        loading: false,
        redirectPage: "/settings",
        upgrade: false
    };

    constructor(props) {
        super(props);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    getBtn() {
        if (this.state.loading) {
            return (
                <button
                    type="submit"
                    className="btn btn-primary w-100"
                    disabled
                >
                    <span
                        class="spinner-grow spinner-grow-sm mr-1"
                        role="status"
                        aria-hidden="true"
                    ></span>
                    Loading...
                </button>
            );
        } else {
            return (
                <button type="submit" className="btn btn-primary w-100">
                    Save changes
                </button>
            );
        }
    }

    getErrorMessage(customClass) {
        if (!customClass) {
            customClass = "alert alert-danger";
        }
        return (
            <div class={customClass} role="alert">
                {this.state.errorMessage}
            </div>
        );
    }
    async handleSubmit(event) {
        event.preventDefault();
        this.setState({ errorMessage: null, loading: true });

        let { token } = await this.props.stripe.createToken({
            currency: "usd"
        });

        let iron = new IronApi();
        let res = await iron.updateCardInfo({
            stripe_token: token,
            account_upgrade: this.props.upgrade
        });

        if (res.data.success == true) {
            window.location.href = this.state.redirectPage;
        } else {
            this.setState({ errorMessage: res.data.message });
        }
        this.setState({ loading: false });
    }
    getCardInfo() {
        return (
            <form onSubmit={this.handleSubmit}>
                <div className="modal-body">
                    {this.state.errorMessage && this.getErrorMessage()}
                    <div className="form-group">
                        <CardElement />
                    </div>
                </div>
                <div className="modal-footer">
                    {this.getBtn()}
                    <button
                        type="button"
                        className="btn btn-secondary"
                        data-dismiss="modal"
                    >
                        Close
                    </button>
                </div>
            </form>
        );
    }

    render() {
        return <div>{this.getCardInfo()}</div>;
    }
}

export default injectStripe(UpdateCardForm);

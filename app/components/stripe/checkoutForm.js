import React, { Component } from "react";
import { CardElement, injectStripe } from "react-stripe-elements";
import IronApi from "../../modules/IronApi";

class CheckoutForm extends Component {
    constructor(props) {
        super(props);
        this.state = { errorMessage: null };
        this.submit = this.submit.bind(this);
    }

    async submit(event) {
        event.preventDefault();
        this.setState({ errorMessage: null, loading: true });

        let firstName = document.getElementById("first_name").value;
        let lastName = document.getElementById("last_name").value;
        let cardName = document.getElementById("cardname").value;
        let email = document.getElementById("email").value;
        let password = document.getElementById("password").value;
        let address = document.getElementById("address").value;
        let city = document.getElementById("city").value;
        let state = document.getElementById("state").value;
        let zip = document.getElementById("zip").value;

        let { token } = await this.props.stripe.createToken({
            currency: "usd",
            name: cardName,
            address_line1: address,
            address_city: city,
            address_state: state,
            address_zip: zip
        });

        if (!token) {
            this.setState({
                errorMessage:
                    "A valid credit card is required. Please fill out the credit card fields with valid information.",
                loading: false
            });
            return;
        }

        // Send request to backend
        let iron = new IronApi();
        let res = await iron.registerPro({
            stripe_token: token,
            first_name: firstName,
            last_name: lastName,
            email: email,
            password: password,
            address: address,
            city: city,
            state: state,
            zip: zip
        });

        if (res.data.success == true) {
            window.location.href = "/dashboard";
        } else {
            this.setState({ errorMessage: res.data.message });
        }
        this.setState({ loading: false });
    }

    getBtn() {
        if (this.state.loading) {
            return (
                <button
                    type="submit"
                    className="btn btn-primary w-100"
                    disabled
                >
                    <span
                        className="spinner-grow spinner-grow-sm mr-1"
                        role="status"
                        aria-hidden="true"
                    ></span>
                    Loading...
                </button>
            );
        } else {
            return (
                <button type="submit" className="btn btn-primary w-100">
                    Purchase
                </button>
            );
        }
    }

    getErrorMessage(customClass) {
        if (!customClass) {
            customClass = "alert alert-danger";
        }

        return (
            <div className={customClass} role="alert">
                {this.state.errorMessage}
            </div>
        );
    }

    render() {
        return (
            <form onSubmit={this.submit} id="checkout-form" className="form">
                {this.state.errorMessage && this.getErrorMessage()}
                Account Information
                <hr></hr>
                <div className="form-group">
                    <input
                        type="text"
                        name="first_name"
                        className="form-control"
                        id="first_name"
                        placeholder="First Name *"
                        required
                    />
                </div>
                <div className="form-group">
                    <input
                        type="text"
                        name="last_name"
                        className="form-control"
                        id="last_name"
                        placeholder="Last Name *"
                        required
                    />
                </div>
                <div className="form-group">
                    <input
                        type="email"
                        name="email"
                        className="form-control"
                        id="email"
                        placeholder="Email *"
                        required
                    />
                </div>
                <div className="form-group">
                    <input
                        type="password"
                        name="password"
                        className="form-control"
                        id="password"
                        placeholder="Password *"
                        required
                    />
                </div>
                Address Information
                <hr></hr>
                <div className="form-group">
                    <input
                        type="text"
                        name="address"
                        className="form-control"
                        id="address"
                        placeholder="address *"
                        required
                    />
                </div>
                <div className="form-group">
                    <input
                        type="text"
                        name="city"
                        className="form-control"
                        id="city"
                        placeholder="city *"
                        required
                    />
                </div>
                <div className="form-group">
                    <input
                        type="text"
                        name="state"
                        className="form-control"
                        id="state"
                        placeholder="state *"
                        required
                    />
                </div>
                <div className="form-group">
                    <input
                        type="text"
                        name="zip"
                        className="form-control"
                        id="zip"
                        placeholder="zip *"
                        required
                    />
                </div>
                Payment Information
                <hr></hr>
                <div className="form-group">
                    <input
                        type="text"
                        name="cardname"
                        className="form-control"
                        id="cardname"
                        placeholder="Name on card *"
                        required
                    />
                </div>
                <div className="form-group">
                    <CardElement />
                </div>
                {this.getBtn()}
                {this.state.errorMessage &&
                    this.getErrorMessage("alert alert-danger mt-3")}
            </form>
        );
    }
}

export default injectStripe(CheckoutForm);

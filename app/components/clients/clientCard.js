import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Link from "next/link";
import ViewHelper from "../../modules/ViewHelper";

export default class ClientCard extends React.Component {
    getFullName(client) {
        return client.first_name + " " + client.last_name;
    }
    getReadableDate() {
        let date = new Date(this.props.client.dob);
        return (
            date.toLocaleString("en-us", { month: "long" }) +
            " " +
            date.getUTCDate() +
            ", " +
            date.getFullYear()
        );
    }

    render(props) {
        let { client, title, bgColor, textColor } = this.props;
        let className = "card mb-3 " + bgColor + " " + textColor;
        let cardTitle = "";
        if (client && this.props.link == "true") {
            cardTitle = (
                <Link href={`/clients/view?id=${client.id}`}>
                    <a>{this.getFullName(client)}</a>
                </Link>
            );
        } else if (client) {
            cardTitle = <span> {this.getFullName(client)}</span>;
        }
        return (
            <div className="">
                <div className={className}>
                    {title && <div className="card-header">{title}</div>}
                    <div className="card-body">
                        <h1 className="card-title">{cardTitle}</h1>
                        <hr />
                        <p className="card-text">
                            <FontAwesomeIcon icon="envelope" />{" "}
                            {client && client.email}
                        </p>
                        <p className="card-text">
                            <FontAwesomeIcon icon="phone" />{" "}
                            {client && client.phone}
                        </p>
                        <p className="card-text">
                            <FontAwesomeIcon icon="map-marker" />{" "}
                            {client && client.address1}{" "}
                            {client && client.address2}
                            <br />
                            {client && client.city}, {client && client.state}{" "}
                            {client && client.zip}
                        </p>
                        <p className="card-text">
                            <FontAwesomeIcon icon="birthday-cake" />{" "}
                            {client && this.getReadableDate()}
                        </p>
                        <p>
                            Created by:{" "}
                            {client && ViewHelper.getCreatedByLink(client.user)}
                        </p>
                    </div>
                </div>
            </div>
        );
    }
}


import Link from 'next/link'
import IronApi from '../../modules/IronApi'
export default class CientList extends React.Component {
    state = {
        clients: null
    }
    async componentDidMount() {
        let date = new Date()
        let iron = new IronApi
        let lessThanDate = null
        let greaterThanDate = date.getFullYear() + '-' + (date.getMonth()+1) + '-01'
        let res = await iron.getClients(lessThanDate, greaterThanDate)
        this.setState({clients: res.data.clients})
    }
    render(props) {
        
        let clientsList = ''
        if(this.state.clients) {
            clientsList = this.state.clients.map((client, i) => {
                let date = new Date(client.created_at)
                return (
                    <Link href={`/clients/view?id=${client.id}`} key={client.id}>
                        <a className="list-group-item list-group-item-action">
                            {client.first_name} {client.last_name}
                            <span className="badge badge-primary float-right">
                                {date.toLocaleString('en-us', { month: 'long' })} {date.getDate()}, {date.getFullYear()}
                            </span>
                        </a>
                    </Link>    
                )
            })
        }
            

        return (
            <div className="list-group">
                {clientsList}
            </div>
        )
    }
}
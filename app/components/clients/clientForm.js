import DatePicker from "react-datepicker";
import IronApi from "../../modules/IronApi";
import Router from "next/router";

export default class ClientForm extends React.Component {
    state = {
        date: null,
        errorMessage: null,
        client: null
    };
    componentDidMount() {
        this.setState({ date: new Date("1991/01/01") });
        if (this.props.clientId) {
            this.loadClient();
        }
    }
    createClient = event => {
        event.preventDefault();
        let iron = new IronApi();
        let clientData = {};
        let data = document.getElementById("client-form");
        let formData = new FormData(data);
        for (var key of formData.keys()) {
            clientData[key] = formData.get(key);
        }
        iron.createClient(clientData).then(res => {
            if (res.status === 201) {
                Router.push("/clients");
            } else {
                this.setState({
                    error: true,
                    errorMessage: res.message.match(/\[(.*?)\]/)
                });
            }
        });
    };
    updateClient = event => {
        event.preventDefault();
        let iron = new IronApi();
        let clientData = {};
        let data = document.getElementById("client-form");
        let formData = new FormData(data);
        for (var key of formData.keys()) {
            clientData[key] = formData.get(key);
        }
        iron.updateClient(this.state.client.id, clientData).then(res => {
            if (res.data.success) {
                Router.push("/clients");
            } else {
                this.setState({
                    error: true,
                    errorMessage: res.message.match(/\[(.*?)\]/)
                });
            }
        });
    };
    async loadClient() {
        let id = this.props.clientId;
        let iron = new IronApi();
        try {
            let res = await iron.getClient(id);
            let client = res.data.client;
            this.setState({ client: client });
            let data = document.getElementById("client-form");
            let formData = new FormData(data);
            for (var key of formData.keys()) {
                if (client[key] !== null) {
                    document.getElementById(key).value = client[key];
                    if (key == "dob") {
                        let dob = new Date(client[key]);
                        dob =
                            dob.getMonth() +
                            1 +
                            "/" +
                            dob.getUTCDate() +
                            "/" +
                            dob.getFullYear();

                        document.getElementById(key).value = dob;
                    }
                }
            }
        } catch (err) {
            this.setState({ errorCode: 404 });
            return;
        }
    }
    render() {
        return (
            <form
                id="client-form"
                onSubmit={
                    this.props.clientId ? this.updateClient : this.createClient
                }
            >
                <p className="text-danger">
                    {this.state.errorMessage &&
                        "Error: " + this.state.errorMessage[1]}
                </p>
                <div className="form-group row">
                    <label className="col-sm-2 col-form-label">
                        First Name
                    </label>
                    <div className="col-sm-10">
                        <input
                            type="text"
                            className="form-control"
                            id="first_name"
                            name="first_name"
                            placeholder="John"
                            required
                        />
                    </div>
                </div>
                <div className="form-group row">
                    <label className="col-sm-2 col-form-label">Last Name</label>
                    <div className="col-sm-10">
                        <input
                            type="text"
                            className="form-control"
                            id="last_name"
                            name="last_name"
                            placeholder="Smith"
                            required
                        />
                    </div>
                </div>
                <div className="form-group row">
                    <label className="col-sm-2 col-form-label">Email</label>
                    <div className="col-sm-10">
                        <input
                            type="email"
                            className="form-control"
                            id="email"
                            name="email"
                            placeholder="johnsmith@email.com"
                        />
                    </div>
                </div>
                <div className="form-group row">
                    <label className="col-sm-2 col-form-label">Phone</label>
                    <div className="col-sm-10">
                        <input
                            type="text"
                            className="form-control"
                            id="phone"
                            name="phone"
                            placeholder="XXX-XXX-XXXX"
                        />
                    </div>
                </div>
                <div className="form-group row">
                    <label className="col-sm-2 col-form-label">Address</label>
                    <div className="col-sm-10">
                        <input
                            type="text"
                            className="form-control"
                            id="address1"
                            name="address1"
                            placeholder="1 Address lane"
                        />
                    </div>
                </div>
                <div className="form-group row">
                    <label className="col-sm-2 col-form-label">City</label>
                    <div className="col-sm-10">
                        <input
                            type="text"
                            className="form-control"
                            id="city"
                            name="city"
                            placeholder="Miami"
                        />
                    </div>
                </div>
                <div className="form-group row">
                    <label className="col-sm-2 col-form-label">State</label>
                    <div className="col-sm-10">
                        <input
                            type="text"
                            className="form-control"
                            id="state"
                            name="state"
                            placeholder="Florida"
                        />
                    </div>
                </div>
                <div className="form-group row">
                    <label className="col-sm-2 col-form-label">Zip</label>
                    <div className="col-sm-10">
                        <input
                            type="text"
                            className="form-control"
                            id="zip"
                            name="zip"
                            placeholder="XXXXX"
                        />
                    </div>
                </div>
                <div className="form-group row">
                    <label className="col-sm-2 col-form-label">
                        Date of Birth
                    </label>
                    <div className="col-sm-10">
                        <DatePicker
                            selected={this.state.date}
                            onChange={d => {
                                this.setState({ date: d });
                            }}
                            className="form-control"
                            showMonthDropdown
                            showYearDropdown
                            name="dob"
                            id="dob"
                            required
                        />
                    </div>
                </div>
                <button type="submit" className="btn btn-primary">
                    Submit
                </button>
            </form>
        );
    }
}

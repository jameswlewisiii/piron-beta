import IronApi from '../../modules/IronApi'
import Router from 'next/router'

export default class EmailForm extends React.Component {
    state = {
        successMessage: null,
        errorMessage: null,
        user: null,
    }
    componentDidMount() {
        let user = IronApi.getUser()
        this.setState({user: user})
        document.getElementById('user_email').value = user.email
        document.getElementById('first_name').value = user.first_name
        document.getElementById('last_name').value = user.last_name    
    }
    async loadUser() {
            
    }
    updateUser = (event) => {
        event.preventDefault()
        let iron = new IronApi()
        let userData = {
            email: document.getElementById('user_email').value,
            first_name: document.getElementById('first_name').value,
            last_name: document.getElementById('last_name').value
        }
        iron.updateUser(userData).then(
            res => {
                if(res.status == 200) {
                    IronApi.setUser(res.data.user)
                    window.scrollTo(0, 0);
                    this.setState({successMessage: 'Your account has been successfully update.'})
                } else {
                    this.setState({errorMessage: res.message})
                }
            }
        )
    }

    render() {
        return (
            <form onSubmit={this.updateUser}>
                
                {
                    this.state.successMessage && 
                    (
                    <div className="alert alert-success" role="alert">
                        {this.state.successMessage}
                    </div>
                    )
                }
                {
                    this.state.errorMessage && 
                    (
                    <div className="alert alert-danger" role="alert">
                        {this.state.errorMessage}
                    </div>
                    )
                }
                <h2>Account Information</h2>
                
                <p>
                    Update your login account email.
                </p>
                <h3>Account Email</h3>
                <hr></hr>
                <div className="form-group row">
                    <label className="col-sm-2 col-form-label">Email</label>
                    <div className="col-sm-10">
                    <input type="email" className="form-control" id="user_email" name="user_email" required/>
                    </div>
                </div>
                <h3>Account Owners Name</h3>
                <hr></hr>
                <div className="form-group row">
                    <label className="col-sm-2 col-form-label">First Name</label>
                    <div className="col-sm-10">
                    <input type="text" className="form-control" id="first_name" name="first_name"/>
                    </div>
                </div>
                <div className="form-group row">
                    <label className="col-sm-2 col-form-label">Last Name</label>
                    <div className="col-sm-10">
                    <input type="text" className="form-control" id="last_name" name="last_name"/>
                    </div>
                </div>
                <button type="submit" className="btn btn-primary">Submit</button>
            </form>
        )
    }

}
import IronApi from '../../modules/IronApi'
import Router from 'next/router'

export default class PasswordForm extends React.Component {
    state = {
        successMessage: null,
        errorMessage: null,
    }
    componentDidMount() {
    }
    updatePassword = (event) => {
        event.preventDefault()
        let iron = new IronApi()
        let passwordData = {
            current_password: document.getElementById('current_password').value,
            new_password: document.getElementById('new_password').value,
            confirm_new_password: document.getElementById('confirm_new_password').value,
        }
        iron.updateUserPassword(passwordData).then(
            res => {
                if(res.status == 200) {
                    this.setState({successMessage: 'Your account has been successfully update.'})
                } else {
                    this.setState({errorMessage: res.message})
                }
            }
        )
    }

    render() {
        return (
            <form onSubmit={this.updatePassword}>
                
                {
                    this.state.successMessage && 
                    (
                    <div className="alert alert-success" role="alert">
                        {this.state.successMessage}
                    </div>
                    )
                }
                {
                    this.state.errorMessage && 
                    (
                    <div className="alert alert-danger" role="alert">
                        {this.state.errorMessage}
                    </div>
                    )
                }
                <h2>Account Password</h2>
                
                <p>
                    Update the your account password.
                </p>
                <h3>Current Password</h3>
                <hr></hr>
                <div className="form-group row">
                    <label className="col-sm-2 col-form-label">Current Password</label>
                    <div className="col-sm-10">
                    <input type="text" className="form-control" id="current_password" name="current_password" required/>
                    </div>
                </div>
                <h3>New Password</h3>
                <hr></hr>
                <div className="form-group row">
                    <label className="col-sm-2 col-form-label">New Password</label>
                    <div className="col-sm-10">
                    <input type="text" className="form-control" id="new_password" name="new_password"/>
                    </div>
                </div>
                <div className="form-group row">
                    <label className="col-sm-2 col-form-label">Confirm New Password</label>
                    <div className="col-sm-10">
                    <input type="text" className="form-control" id="confirm_new_password" name="confirm_new_password"/>
                    </div>
                </div>
                <button type="submit" className="btn btn-primary">Submit</button>
            </form>
        )
    }

}
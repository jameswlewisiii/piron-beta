import IronApi from "../../modules/IronApi";
import Router from "next/router";

export default class AccountForm extends React.Component {
    state = {
        successMessage: null,
        errorMessage: null,
        account: null
    };
    async componentDidMount() {
        this.loadAccount();
    }
    async loadAccount() {
        try {
            let iron = new IronApi();
            let res = await iron.getAccount();
            let account = res.data.account;
            this.setState({ account: account });
            let data = document.querySelector("form");
            let formData = new FormData(data);
            for (var key of formData.keys()) {
                if (account[key] !== null) {
                    document.getElementById(key).value = account[key];
                }
            }
        } catch (err) {
            this.setState({ errorCode: 404 });
            return;
        }
    }
    updateAccount = event => {
        event.preventDefault();
        let iron = new IronApi();
        let accountData = {};
        let data = document.querySelector("form");
        let formData = new FormData(data);
        for (var key of formData.keys()) {
            accountData[key] = formData.get(key);
        }
        iron.updateAccount(accountData).then(res => {
            if (res.status == 200) {
                window.scrollTo(0, 0);
                this.setState({
                    successMessage: "Your account has been successfully update."
                });
            } else {
            }
        });
    };

    render() {
        return (
            <form onSubmit={this.updateAccount}>
                {this.state.successMessage && (
                    <div class="alert alert-success" role="alert">
                        {this.state.successMessage}
                    </div>
                )}
                {this.state.errorMessage && (
                    <div class="alert alert-danger" role="alert">
                        {this.state.errorMessage}
                    </div>
                )}
                <h2>General Account Information</h2>

                <p>
                    Update the general shop/studio information for this account.
                </p>
                <h3>Account Infomation</h3>
                <hr></hr>
                <div className="form-group row">
                    <label className="col-sm-2 col-form-label">Name</label>
                    <div className="col-sm-10">
                        <input
                            type="text"
                            className="form-control"
                            id="name"
                            name="name"
                        />
                    </div>
                </div>
                <div className="form-group row">
                    <label className="col-sm-2 col-form-label">Phone</label>
                    <div className="col-sm-10">
                        <input
                            type="text"
                            className="form-control"
                            id="phone"
                            name="phone"
                        />
                    </div>
                </div>
                <div className="form-group row">
                    <label className="col-sm-2 col-form-label">Website</label>
                    <div className="col-sm-10">
                        <input
                            type="text"
                            className="form-control"
                            id="website"
                            name="website"
                        />
                    </div>
                </div>
                <div className="form-group row">
                    <label className="col-sm-2 col-form-label">Facebook</label>
                    <div className="col-sm-10">
                        <input
                            type="text"
                            className="form-control"
                            id="facebook"
                            name="facebook"
                        />
                    </div>
                </div>
                <h3>Shop Address</h3>
                <hr></hr>
                <div className="form-group row">
                    <label className="col-sm-2 col-form-label">Address 1</label>
                    <div className="col-sm-10">
                        <input
                            type="text"
                            className="form-control"
                            id="address1"
                            name="address1"
                        />
                    </div>
                </div>
                <div className="form-group row">
                    <label className="col-sm-2 col-form-label">Address 2</label>
                    <div className="col-sm-10">
                        <input
                            type="text"
                            className="form-control"
                            id="address2"
                            name="address2"
                        />
                    </div>
                </div>
                <div className="form-group row">
                    <label className="col-sm-2 col-form-label">City</label>
                    <div className="col-sm-10">
                        <input
                            type="text"
                            className="form-control"
                            id="city"
                            name="city"
                        />
                    </div>
                </div>
                <div className="form-group row">
                    <label className="col-sm-2 col-form-label">State</label>
                    <div className="col-sm-10">
                        <input
                            type="text"
                            className="form-control"
                            id="state"
                            name="state"
                        />
                    </div>
                </div>
                <div className="form-group row">
                    <label className="col-sm-2 col-form-label">Zip</label>
                    <div className="col-sm-10">
                        <input
                            type="text"
                            className="form-control"
                            id="zip"
                            name="zip"
                        />
                    </div>
                </div>
                <button type="submit" className="btn btn-primary">
                    Submit
                </button>
            </form>
        );
    }
}

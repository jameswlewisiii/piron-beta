import IronApi from "../../modules/IronApi";
import { Elements, StripeProvider } from "react-stripe-elements";
import UpdateCardForm from "../stripe/updateCardForm";

export default class BillingForm extends React.Component {
    state = {
        user: null,
        stripe: null,
        errorMessage: null,
        loading: false
    };
    constructor(props) {
        super();
        this.deleteAccount = this.deleteAccount.bind(this);
    }
    componentDidMount() {
        this.loadData();
        if (window.Stripe) {
            this.setState({
                stripe: window.Stripe(IronApi.getStripeKey())
            });
        }
    }
    async loadData() {
        let iron = new IronApi();
        let user = IronApi.getUser();
        let account = await iron.getAccount();
        this.setState({
            user: user,
            account: account.data.account,
            customer: account.data.customer
        });
    }
    getBtn() {
        if (this.state.loading) {
            return (
                <button
                    type="submit"
                    className="btn btn-primary w-100"
                    disabled
                >
                    <span
                        class="spinner-grow spinner-grow-sm mr-1"
                        role="status"
                        aria-hidden="true"
                    ></span>
                    Loading...
                </button>
            );
        } else {
            return (
                <button type="submit" className="btn btn-primary w-100">
                    Save changes
                </button>
            );
        }
    }

    getErrorMessage(customClass) {
        if (!customClass) {
            customClass = "alert alert-danger";
        }
        return (
            <div class={customClass} role="alert">
                {this.state.errorMessage}
            </div>
        );
    }
    async deleteAccount(event) {
        event.preventDefault();
        let iron = new IronApi();
        let res = await iron.deleteAccount();
        if (res.data.success == true) {
            await iron.logout();
            window.location.href = "/";
        }
    }
    getCardInfo() {
        let customer = this.state.customer;
        if (customer) {
            return (
                <div>
                    <div className="card">
                        <div className="card-header">Card on File</div>
                        <div className="card-body">
                            <p>
                                Card Type: {customer.sources.data[0].brand}{" "}
                                <br />
                                Last 4: {customer.sources.data[0].last4} <br />
                                Expiration: {customer.sources.data[0].exp_month}
                                /{customer.sources.data[0].exp_year} <br />
                            </p>
                            <button
                                className="btn btn-primary"
                                data-toggle="modal"
                                data-target="#updateCard"
                            >
                                Update Card
                            </button>
                            <button
                                className="btn btn-danger float-right"
                                data-toggle="modal"
                                data-target="#deleteAccount"
                            >
                                Delete account
                            </button>
                        </div>
                    </div>

                    {/* Delete account modal */}
                    <div className="modal" id="deleteAccount" role="dialog">
                        <div className="modal-dialog" role="document">
                            <div className="modal-content">
                                <div className="modal-header">
                                    <h5 className="modal-title">
                                        Delete Account
                                    </h5>
                                    <button
                                        type="button"
                                        className="close"
                                        data-dismiss="modal"
                                        aria-label="Close"
                                    >
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div className="modal-body">
                                    If you choose to delete your account your
                                    card information will be removed and you
                                    will no longer be billed.
                                    <div className="alert alert-danger mt-2">
                                        Warning: Deleting your account will
                                        cause the account to be downlowd graded
                                        to a freemium account. You might lose
                                        customer, transaction, user, and
                                        appointment data.
                                    </div>
                                </div>
                                <div className="modal-footer">
                                    <button
                                        type="submit"
                                        className="btn btn-danger"
                                        onClick={this.deleteAccount}
                                    >
                                        Delete
                                    </button>
                                    <button
                                        type="button"
                                        className="btn btn-secondary"
                                        data-dismiss="modal"
                                    >
                                        Close
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    {/* Update card modal */}
                    <div className="modal" id="updateCard" role="dialog">
                        <div className="modal-dialog" role="document">
                            <div className="modal-content">
                                <div className="modal-header">
                                    <h5 className="modal-title">
                                        Update Card on File
                                    </h5>
                                    <button
                                        type="button"
                                        className="close"
                                        data-dismiss="modal"
                                        aria-label="Close"
                                    >
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <StripeProvider stripe={this.state.stripe}>
                                    <Elements>
                                        <UpdateCardForm upgrade="false" />
                                    </Elements>
                                </StripeProvider>
                            </div>
                        </div>
                    </div>
                </div>
            );
        }
    }

    render() {
        return <div>{this.getCardInfo()}</div>;
    }
}

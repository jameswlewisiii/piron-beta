import IronApi from "../../modules/IronApi";
import {
    LineChart,
    ResponsiveContainer,
    Line,
    CartesianGrid,
    XAxis,
    YAxis,
    Tooltip
} from "recharts";

export default class TransactionGraph extends React.Component {
    state = {
        transactions: null,
        data: null
    };
    async componentDidMount() {
        let date = new Date();
        let iron = new IronApi();
        let month = date.getMonth() + 1;
        if (month.toString().length < 2) {
            month = "0" + month;
        }
        let greaterThanDate = date.getFullYear() + "-" + month + "-01";
        let res = await iron.getTransactions(null, greaterThanDate);
        this.setState({ transactions: res.data.transactions });
        this.loadGraphData();
    }
    loadGraphData() {
        let date = new Date();
        let data = [];
        let transactions = this.state.transactions;

        for (let i = 1; i < date.getUTCDate(); i++) {
            let amount = 0;
            transactions.forEach(transaction => {
                let d = new Date(transaction.date);
                if (i == d.getUTCDate()) {
                    amount =
                        parseFloat(transaction.amount) + parseFloat(amount);
                }
            });

            data.push({
                name:
                    date.toLocaleString("en-us", { month: "long" }) +
                    " " +
                    i +
                    ", " +
                    date.getFullYear(),
                amount: amount
            });
        }

        this.setState({ data: data });
    }
    render(props) {
        return (
            <ResponsiveContainer height="100%" width="100%" aspect={4.0 / 2.0}>
                <LineChart data={this.state.data}>
                    <Line type="monotone" dataKey="amount" stroke="#1a1a1a" />
                    <CartesianGrid stroke="#ccc" />
                    <XAxis dataKey="name" />
                    <YAxis />
                    <Tooltip />
                </LineChart>
            </ResponsiveContainer>
        );
    }
}

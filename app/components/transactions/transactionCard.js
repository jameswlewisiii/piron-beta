import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import ViewHelper from "../../modules/ViewHelper";

export default class TransactionCard extends React.Component {
    getReadableDate() {
        let date = new Date(this.props.transaction.date);
        return (
            date.toLocaleString("en-us", { month: "long" }) +
            " " +
            date.getUTCDate() +
            ", " +
            date.getFullYear()
        );
    }
    render(props) {
        let { transaction } = this.props;

        return (
            <div className="card text-primary">
                <div className="card-header">Transaction</div>
                <div className="card-body">
                    <h1 className="card-title">
                        <FontAwesomeIcon
                            className="text-success"
                            icon="dollar-sign"
                        />{" "}
                        {transaction == null ? "0.00" : transaction.amount}
                    </h1>
                    <p className="card-text">
                        <FontAwesomeIcon icon="calendar" />{" "}
                        {transaction && this.getReadableDate()}
                    </p>
                    <p className="card-text">
                        <FontAwesomeIcon icon="hourglass-start" />{" "}
                        {transaction && transaction.start_time}
                    </p>
                    <p className="card-text">
                        <FontAwesomeIcon icon="hourglass-end" />{" "}
                        {transaction && transaction.end_time}
                    </p>
                    <p className="card-text">
                        <FontAwesomeIcon icon="comment" />{" "}
                        {transaction && transaction.description} -{" "}
                        {transaction && transaction.notes}
                    </p>
                    <p>
                        Created by:{" "}
                        {transaction &&
                            ViewHelper.getCreatedByLink(transaction.user)}
                    </p>
                </div>
            </div>
        );
    }
}

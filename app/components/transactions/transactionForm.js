import DatePicker from "react-datepicker";
import IronApi from "../../modules/IronApi";
import Router from "next/router";

export default class TransactionForm extends React.Component {
    state = {
        clients: null,
        transaction: null,
        start_time: null,
        end_time: null,
        date: null,
        error: false,
        errorMessage: null,
        errorCode: null
    };
    async componentDidMount() {
        let iron = new IronApi();
        let res = await iron.getClients();
        this.setState({
            clients: res.data.clients,
            date: new Date()
        });
        if (this.props.transactionId) {
            this.loadTransaction();
        }
    }
    updateTransaction = async event => {
        event.preventDefault();
        let id = this.props.transactionId;
        let iron = new IronApi();
        let transactionData = {};
        let data = document.getElementById("transaction-form");
        let formData = new FormData(data);
        for (var key of formData.keys()) {
            transactionData[key] = formData.get(key);
        }
        console.log(transactionData);
        let res = await iron.updateTransaction(id, transactionData);
        console.log(res.data);
        if (res.data.success) {
            Router.push("/transactions");
        } else {
            this.setState({
                error: true,
                errorMessage: res.message.match(/\[(.*?)\]/)
            });
        }
    };
    createTransaction = async event => {
        event.preventDefault();
        let iron = new IronApi();
        let transactionData = {};
        let data = document.getElementById("transaction-form");
        let formData = new FormData(data);
        for (var key of formData.keys()) {
            transactionData[key] = formData.get(key);
        }
        let res = await iron.createTransaction(transactionData);
        if (res.status === 201) {
            Router.push("/transactions");
        } else {
            this.setState({
                error: true,
                errorMessage: res.message.match(/\[(.*?)\]/)
            });
        }
    };
    async loadTransaction() {
        let id = this.props.transactionId;
        let iron = new IronApi();
        let res = await iron.getTransaction(id);
        let transaction = res.data.transaction;
        this.setState({ transaction: transaction });
        let data = document.getElementById("transaction-form");
        let formData = new FormData(data);
        for (var key of formData.keys()) {
            console.log(key);
            if (transaction[key] !== null) {
                document.getElementById(key).value = transaction[key];
                if (key === "date") {
                    let dob = new Date(transaction[key]);
                    dob =
                        dob.getMonth() +
                        1 +
                        "/" +
                        dob.getUTCDate() +
                        "/" +
                        dob.getFullYear();
                    document.getElementById(key).value = dob;
                }
            }
        }
    }
    render(props) {
        let clientOptions = <option value="null">No clients found</option>;
        if (Array.isArray(this.state.clients)) {
            clientOptions = this.state.clients.map((client, i) => (
                <option value={client.id} key={client.id}>
                    {client.first_name} {client.last_name}
                </option>
            ));
        }

        if (this.state.errorCode) {
            return (
                <AppLayout>
                    <ErrorPage statusCode={this.state.errorCode} />
                </AppLayout>
            );
        }

        return (
            <form
                id="transaction-form"
                onSubmit={
                    this.props.transactionId
                        ? this.updateTransaction
                        : this.createTransaction
                }
            >
                <p className="text-danger">
                    {this.state.errorMessage &&
                        "Error: " + this.state.errorMessage[1]}
                </p>
                <div className="form-group row">
                    <label className="col-sm-2 col-form-label">
                        Description *
                    </label>
                    <div className="col-sm-10">
                        <input
                            type="text"
                            className="form-control"
                            id="description"
                            name="description"
                            placeholder="Description"
                            required
                        />
                    </div>
                </div>
                <div className="form-group row">
                    <label className="col-sm-2 col-form-label">Amount *</label>
                    <div className="col-sm-10">
                        <div className="input-group">
                            <div className="input-group-prepend">
                                <span className="input-group-text">$</span>
                            </div>
                            <input
                                type="number"
                                step="0.01"
                                className="form-control"
                                id="amount"
                                name="amount"
                                placeholder="0.42"
                                required
                            />
                        </div>
                    </div>
                </div>
                <div className="form-group row">
                    <label className="col-sm-2 col-form-label">Notes *</label>
                    <div className="col-sm-10">
                        <input
                            type="text"
                            className="form-control"
                            id="notes"
                            name="notes"
                            placeholder="Notes"
                            required
                        />
                    </div>
                </div>
                <div className="form-group row">
                    <label className="col-sm-2 col-form-label">Client *</label>
                    <div className="col-sm-10">
                        <select
                            className="custom-select"
                            id="client_id"
                            name="client_id"
                            required
                        >
                            <option value="">Select a client</option>
                            {clientOptions}
                        </select>
                    </div>
                </div>
                <div className="form-group row">
                    <label className="col-sm-2 col-form-label">
                        Duration *
                    </label>
                    <div className="col-sm-10">
                        <DatePicker
                            selected={this.state.start_time}
                            onChange={d => {
                                this.setState({ start_time: d });
                            }}
                            showTimeSelect
                            showTimeSelectOnly
                            timeIntervals={15}
                            dateFormat="h:mm aa"
                            timeCaption="Start Time"
                            className="form-control"
                            name="start_time"
                            id="start_time"
                            placeholderText="Start Time"
                            required
                        />
                        <DatePicker
                            selected={this.state.end_time}
                            onChange={d => {
                                this.setState({ end_time: d });
                            }}
                            showTimeSelect
                            showTimeSelectOnly
                            timeIntervals={15}
                            dateFormat="h:mm aa"
                            timeCaption="End Time"
                            className="form-control"
                            name="end_time"
                            id="end_time"
                            placeholderText="End Time"
                            required
                        />
                    </div>
                </div>
                <div className="form-group row">
                    <label className="col-sm-2 col-form-label">Date *</label>
                    <div className="col-sm-10">
                        <DatePicker
                            selected={this.state.date}
                            onChange={d => {
                                this.setState({ date: d });
                            }}
                            className="form-control"
                            showMonthDropdown
                            showYearDropdown
                            name="date"
                            id="date"
                            required
                        />
                    </div>
                </div>
                <p>All fields marked with an * are required</p>
                <button type="submit" className="btn btn-primary">
                    Submit
                </button>
            </form>
        );
    }
}

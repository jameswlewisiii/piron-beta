import Link from "next/link";
import IronApi from "../../modules/IronApi";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
export default class TransactionList extends React.Component {
    state = {
        transactions: []
    };
    async componentDidMount() {
        this.loadTransactions();
    }

    async loadTransactions() {
        if (Array.isArray(this.props.transactions)) {
            this.setState({ transactions: this.props.transactions });
        }
        if (this.props.transactions == "last30days") {
            let date = new Date();
            let iron = new IronApi();
            let lessThanDate = null;
            let month = date.getMonth() + 1;
            if (month.toString().length < 2) {
                month = "0" + month;
            }
            let greaterThanDate = date.getFullYear() + "-" + month + "-01";
            let res = await iron.getTransactions(lessThanDate, greaterThanDate);
            this.setState({ transactions: res.data.transactions.slice(0, 8) });
        }
    }

    async filterResults(event) {
        event.preventDefault();

        let filteredTransactions = [];
        let transactions = this.state.transactions;
        let searchTerm = document.getElementById("transactionsSearchTerm")
            .value;

        if (searchTerm == "" || searchTerm == undefined) {
            this.loadTransactions();
        } else {
            for (let i = 0; i < transactions.length; i++) {
                if (
                    transactions[i].description
                        .toLowerCase()
                        .includes(searchTerm.toLowerCase())
                ) {
                    filteredTransactions.push(transactions[i]);
                }
            }

            this.setState({ transactions: filteredTransactions });
        }
    }

    render(props) {
        let transactionList = "";
        if (this.state.transactions) {
            transactionList = this.state.transactions.map((transaction, i) => {
                return (
                    <Link
                        href={`/transactions/view?id=${transaction.id}`}
                        key={transaction.id}
                    >
                        <a className="list-group-item list-group-item-action">
                            {transaction.description}
                            <span className="badge badge-primary float-right">
                                ${transaction.amount}
                            </span>
                        </a>
                    </Link>
                );
            });
        }

        return (
            <div className="list-group">
                <div className="input-group">
                    <div className="input-group-prepend">
                        <div className="input-group-text">
                            <FontAwesomeIcon icon="search" />
                        </div>
                    </div>
                    <input
                        onChange={this.filterResults.bind(this)}
                        type="text"
                        className="form-control"
                        id="transactionsSearchTerm"
                        placeholder="Search..."
                    />
                </div>
                {transactionList}
            </div>
        );
    }
}

import DatePicker from "react-datepicker";
import IronApi from "../../modules/IronApi";
import Router from "next/router";

export default class AppointmentForm extends React.Component {
    state = {
        error: false,
        errorMessage: null,
        date: null,
        start_time: null,
        end_time: null,
        appointment: null,
        clients: null
    };
    async componentDidMount() {
        let iron = new IronApi();
        let res = await iron.getClients();
        this.setState({
            date: new Date(),
            clients: res.data.clients
        });
        if (this.props.appointmentId) {
            this.loadAppointment();
        }
    }
    createAppointment = event => {
        event.preventDefault();
        let iron = new IronApi();
        let appointmentData = {};
        let data = document.getElementById("appointment-form");
        let formData = new FormData(data);
        for (var key of formData.keys()) {
            appointmentData[key] = formData.get(key);
        }
        iron.createAppointment(appointmentData).then(res => {
            console.log(res.statusCode);
            if (res.status === 201) {
                Router.push("/appointments");
            } else {
                this.setState({ error: true, errorMessage: res.data.message });
            }
        });
    };
    updateAppointment = event => {
        event.preventDefault();
        let iron = new IronApi();
        let appointmentData = {};
        let data = document.getElementById("appointment-form");
        let formData = new FormData(data);
        for (var key of formData.keys()) {
            appointmentData[key] = formData.get(key);
        }
        iron.updateAppointment(this.props.appointmentId, appointmentData).then(
            res => {
                if (res.data.success) {
                    Router.push("/appointments");
                } else {
                    this.setState({
                        error: true,
                        errorMessage: res.message.match(/\[(.*?)\]/)
                    });
                }
            }
        );
    };
    async loadAppointment() {
        let id = this.props.appointmentId;
        let iron = new IronApi();
        try {
            let res = await iron.getAppointment(id);
            let appointment = res.data.appointment;
            this.setState({ appointment: appointment });
            let data = document.getElementById("appointment-form");
            let formData = new FormData(data);
            for (var key of formData.keys()) {
                if (appointment[key] !== null) {
                    document.getElementById(key).value = appointment[key];
                    if (key === "date") {
                        let dob = new Date(appointment[key]);
                        dob =
                            dob.getMonth() +
                            1 +
                            "/" +
                            dob.getUTCDate() +
                            "/" +
                            dob.getFullYear();
                        document.getElementById(key).value = dob;
                    }
                }
            }
        } catch (err) {
            this.setState({ errorCode: 404 });
            return;
        }
    }
    render() {
        let clientOptions = <option value="null">No clients found</option>;
        if (Array.isArray(this.state.clients)) {
            clientOptions = this.state.clients.map((client, i) => (
                <option value={client.id} key={client.id}>
                    {client.first_name} {client.last_name}
                </option>
            ));
        }

        return (
            <form
                id="appointment-form"
                onSubmit={
                    this.props.appointmentId
                        ? this.updateAppointment
                        : this.createAppointment
                }
            >
                <p className="text-danger">
                    {this.state.errorMessage &&
                        "Error: " + this.state.errorMessage[1]}
                </p>
                <div className="form-group row">
                    <label className="col-sm-2 col-form-label">Notes</label>
                    <div className="col-sm-10">
                        <input
                            type="text"
                            className="form-control"
                            id="notes"
                            name="notes"
                            placeholder="Notes"
                            required
                        />
                    </div>
                </div>
                <div className="form-group row">
                    <label className="col-sm-2 col-form-label">Client</label>
                    <div className="col-sm-10">
                        <select
                            className="custom-select"
                            id="client_id"
                            name="client_id"
                            required
                        >
                            <option value="">Select a client</option>
                            {clientOptions}
                        </select>
                    </div>
                </div>
                <div className="form-group row">
                    <label className="col-sm-2 col-form-label">Duration</label>
                    <div className="col-sm-10">
                        <DatePicker
                            selected={this.state.start_time}
                            onChange={d => {
                                this.setState({ start_time: d });
                            }}
                            showTimeSelect
                            showTimeSelectOnly
                            timeIntervals={15}
                            dateFormat="h:mm aa"
                            timeCaption="Start Time"
                            className="form-control"
                            name="start_time"
                            id="start_time"
                            placeholderText="Start Time"
                            required
                        />
                        <DatePicker
                            selected={this.state.end_time}
                            onChange={d => {
                                this.setState({ end_time: d });
                            }}
                            showTimeSelect
                            showTimeSelectOnly
                            timeIntervals={15}
                            dateFormat="h:mm aa"
                            timeCaption="End Time"
                            className="form-control"
                            name="end_time"
                            id="end_time"
                            placeholderText="End Time"
                            required
                        />
                    </div>
                </div>
                <div className="form-group row">
                    <label className="col-sm-2 col-form-label">Date</label>
                    <div className="col-sm-10">
                        <DatePicker
                            selected={this.state.date}
                            onChange={d => {
                                this.setState({ date: d });
                            }}
                            className="form-control"
                            showMonthDropdown
                            showYearDropdown
                            name="date"
                            id="date"
                        />
                    </div>
                </div>
                <button type="submit" className="btn btn-primary">
                    Submit
                </button>
            </form>
        );
    }
}

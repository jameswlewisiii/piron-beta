import Day from "./day";

export default class Week extends React.Component {
    state = {
        data: null
    };
    async componentDidMount() {}

    render() {
        let days = this.props.week.map((date, i) => {
            return (
                <td scope="row" key={i}>
                    <Day date={date} />
                </td>
            );
        });

        return <tr className="p-0">{days}</tr>;
    }
}

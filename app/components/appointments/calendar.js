import Week from "./week";
import Day from "./day";
import IronApi from "../../modules/IronApi";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
export default class Calendar extends React.Component {
    state = {
        data: null,
        currentMonth: new Date().getMonth(),
        currentYear: new Date().getFullYear(),
        daysArr: [],
        appointments: []
    };
    monthNames = [
        "January",
        "February",
        "March",
        "April",
        "May",
        "June",
        "July",
        "August",
        "September",
        "October",
        "November",
        "December"
    ];

    monthNumbers = [
        "01",
        "02",
        "03",
        "04",
        "05",
        "06",
        "07",
        "08",
        "09",
        "10",
        "11",
        "12"
    ];

    super() {
        this.changeMonth.bind(this);
    }

    async changeMonth(amount) {
        await this.setState({ currentMonth: this.state.currentMonth + amount });
        this.loadAppointments();
    }

    startingDayOfTheMonth() {
        return new Date(
            this.state.currentYear,
            this.state.currentMonth
        ).getDay();
    }

    daysInMonth() {
        return (
            32 -
            new Date(
                this.state.currentYear,
                this.state.currentMonth,
                32
            ).getDate()
        );
    }

    getDays() {
        let days = [];
        let weeks = [];
        let dayCounter = 0;

        for (let i = 0; i < this.startingDayOfTheMonth(); i++) {
            dayCounter++;
            days.push(<Day date={null} key={dayCounter} />);
        }

        for (let i = 1; i < this.daysInMonth() + 1; i++) {
            dayCounter++;
            let date = {
                year: this.state.currentYear,
                month: this.state.currentMonth,
                day: i,
                appointments: this.state.appointments.filter(appointment => {
                    let currentDate =
                        this.state.currentYear +
                        "-" +
                        this.monthNumbers[this.state.currentMonth] +
                        "-" +
                        i;
                    return (
                        new Date(currentDate).toJSON().slice(0, 10) ==
                        new Date(appointment.date).toJSON().slice(0, 10)
                    );
                })
            };
            days.push(<Day date={date} key={dayCounter} />);
            if (dayCounter % 7 === 0) {
                weeks.push(<tr key={weeks.length}>{days}</tr>);
                days = [];
            }
        }
        weeks.push(<tr key={weeks.length}>{days}</tr>);

        return weeks;
    }

    async componentDidMount() {
        this.loadAppointments();
    }

    async loadAppointments() {
        let currentMonth = this.state.currentMonth + 1;
        let iron = new IronApi();
        let appointments = await iron.getAppointments(
            this.state.currentYear +
                "-" +
                currentMonth +
                "-" +
                this.daysInMonth(),
            this.state.currentYear + "-" + currentMonth + "-01"
        );
        if (appointments.status == 200 && appointments.data.appointments) {
            this.setState({ appointments: appointments.data.appointments });
        }
    }

    render() {
        return (
            <div>
                <div className="text-center">
                    <div className="row">
                        <div className="col-1 offset-3">
                            {this.state.currentMonth > 0 && (
                                <a
                                    href="#"
                                    className="display-4 text-primary"
                                    onClick={() => {
                                        this.changeMonth(-1);
                                    }}
                                >
                                    <FontAwesomeIcon
                                        icon="caret-left"
                                        className=""
                                    />
                                </a>
                            )}
                        </div>
                        <div className="col-4">
                            <h1 className="display-4 text-primary ml-2 mr-2">
                                {this.monthNames[this.state.currentMonth]}
                            </h1>
                        </div>
                        <div className="col-1">
                            {this.state.currentMonth < 11 && (
                                <a
                                    href="#"
                                    className="display-4 text-primary"
                                    onClick={() => {
                                        this.changeMonth(1);
                                    }}
                                >
                                    <FontAwesomeIcon
                                        icon="caret-right"
                                        className=""
                                    />
                                </a>
                            )}
                        </div>
                    </div>
                </div>
                <table className="table table-sm table-bordered">
                    <thead className="thead-dark">
                        <tr className="text-center">
                            <th>Sunday</th>
                            <th>Monday</th>
                            <th>Tuesday</th>
                            <th>Wednesday</th>
                            <th>Thursday</th>
                            <th>Friday</th>
                            <th>Saturday</th>
                        </tr>
                    </thead>
                    <tbody>{this.getDays()}</tbody>
                </table>
            </div>
        );
    }
}

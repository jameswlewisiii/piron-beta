import Link from "next/link";

export default class AppointmentList extends React.Component {
    getReadableDate(d) {
        let date = new Date(d);
        return (
            date.toLocaleString("en-us", { month: "long" }) +
            " " +
            date.getDate() +
            ", " +
            date.getFullYear()
        );
    }
    render() {
        let appointmentList = "";

        if (this.props.appointments) {
            appointmentList = this.props.appointments.map((appointment, i) => {
                return (
                    <Link
                        href={`/appointments/view?id=${appointment.id}`}
                        key={appointment.id}
                    >
                        <a className="list-group-item list-group-item-action">
                            {appointment.notes}
                            <span className="badge badge-primary float-right">
                                {this.getReadableDate(appointment.date)}
                            </span>
                        </a>
                    </Link>
                );
            });
        }

        return <div className="list-group">{appointmentList}</div>;
    }
}

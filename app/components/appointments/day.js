import ReactTable from "react-table";
import Router from "next/router";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
export default class Day extends React.Component {
    state = {
        data: null
    };
    days = [
        "Sunday",
        "Monday",
        "Tuesday",
        "Wednesday",
        "Thursday",
        "Friday",
        "Saturday"
    ];

    columns = [
        {
            id: "notes",
            Header: "Name",
            accessor: a => {
                return (
                    <a
                        href="#"
                        onClick={() => {
                            $(".modal").modal("hide");
                            Router.push("/appointments/view?id=" + a.id);
                        }}
                    >
                        {a.notes}
                    </a>
                );
            },
            filterMethod: (filter, row) => {
                let name = row._original.notes;
                return name.search(filter.value) !== -1;
            },
            filterable: true
        },
        {
            id: "time",
            Header: "Time",
            accessor: a => a.start_time + " - " + a.end_time,
            filterable: true
        },
        {
            id: "clientName",
            Header: "Client's Name",
            accessor: a => a.client.first_name + " " + a.client.last_name,
            filterable: true
        }
    ];

    async componentDidMount() {}

    getDateIndicator(date) {
        let klass =
            new Date().getUTCDate() == date.day
                ? "badge badge-pill badge-danger float-right"
                : "badge badge-pill badge-grey float-right";

        return <span className={klass}>{this.props.date.day}</span>;
    }

    getDayName(date) {
        let d = new Date(date.year, date.month, date.day);
        return this.days[d.getDay()];
    }

    render() {
        if (this.props.date === null) {
            return <td></td>;
        }

        let modelName =
            "modal-" +
            this.props.date.year +
            "-" +
            this.props.date.month +
            "-" +
            this.props.date.day;

        let hasAppointments = this.props.date.appointments.length > 0;
        let appointmentStyle = hasAppointments ? { cursor: "pointer" } : {};

        return (
            <td
                key={this.props.date.day}
                onClick={() => {
                    $("#" + modelName).modal("show");
                }}
                style={appointmentStyle}
            >
                {this.getDateIndicator(this.props.date)}
                {hasAppointments && (
                    <span className="badge badge-pill badge-primary">
                        <FontAwesomeIcon icon="calendar-check" />{" "}
                    </span>
                )}
                <div className="card border-light">
                    <div className="card-body"></div>
                </div>
                {/* Modal */}
                {hasAppointments && (
                    <div
                        className="modal fade"
                        id={modelName}
                        tabIndex="-1"
                        aria-labelledby={modelName}
                        role="dialog"
                        aria-hidden="true"
                    >
                        <div className="modal-dialog modal-lg">
                            <div className="modal-content">
                                <div className="modal-header">
                                    <h5 className="modal-title">
                                        {this.getDayName(this.props.date)}{" "}
                                        {this.props.date.day},{" "}
                                        {this.props.date.year}
                                    </h5>
                                    <button
                                        type="button"
                                        className="close"
                                        data-dismiss="modal"
                                        aria-label="Close"
                                    >
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div className="modal-body">
                                    <ReactTable
                                        data={this.props.date.appointments}
                                        columns={this.columns}
                                        defaultPageSize={5}
                                    />
                                </div>
                                <div className="modal-footer">
                                    <button
                                        type="button"
                                        className="btn btn-secondary"
                                        data-dismiss="modal"
                                    >
                                        Close
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                )}
            </td>
        );
    }
}

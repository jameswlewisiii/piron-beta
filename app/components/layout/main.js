import Footer from "./footer";
import Head from "./head";
import Navbar from "./mainNavbar";

export default class MainLayout extends React.Component {
    render(props) {
        return (
            <div>
                <Head />
                <Navbar />
                <div className="app">
                    {this.props.children}
                    <Footer />
                </div>
            </div>
        );
    }
}

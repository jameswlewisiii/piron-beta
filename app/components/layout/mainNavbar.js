import Link from "next/link";
import IronApi from "../../modules/IronApi";

export default class MainLayout extends React.Component {
    state = {
        auth: false
    };
    componentDidMount() {
        this.setState({
            auth: IronApi.authenticated(),
            user: IronApi.getUser()
        });
    }
    getNav() {
        if (this.state.auth) {
            return (
                <ul className="nav navbar-nav ml-auto">
                    <li className="nav-item"></li>
                    <li className="nav-item dropdown">
                        <a
                            className="nav-link dropdown-toggle"
                            href="#"
                            id="navbarDropdown"
                            role="button"
                            data-toggle="dropdown"
                            aria-haspopup="true"
                            aria-expanded="false"
                        >
                            {this.state.user
                                ? this.state.user.email
                                : "Account"}
                        </a>
                        <div
                            className="dropdown-menu"
                            aria-labelledby="navbarDropdown"
                        >
                            <Link href="/dashboard">
                                <a className="dropdown-item" href="#">
                                    Dashboard
                                </a>
                            </Link>
                            <Link href="/settings">
                                <a className="dropdown-item" href="#">
                                    Settings
                                </a>
                            </Link>
                            <div className="dropdown-divider"></div>
                            <Link href="/logout">
                                <a className="dropdown-item">Log Out</a>
                            </Link>
                        </div>
                    </li>
                </ul>
            );
        } else {
            return (
                <ul className="nav navbar-nav ml-auto">
                    {/* <li className="nav-item">
                        <Link href="/features">
                            <a className="nav-link">Features</a>
                        </Link>
                    </li> */}
                    {/* <li className="nav-item">
                        <Link href="/register">
                            <a className="nav-link">Pricing</a>
                        </Link>
                    </li> */}
                    <li className="nav-item">
                        <Link href="/quickstart">
                            <a className="nav-link">Documentation</a>
                        </Link>
                    </li>
                    <li className="nav-item">
                        <Link href="/login">
                            <a className="nav-link">Login</a>
                        </Link>
                    </li>
                    <li className="nav-item">
                        <Link href="/register/freemium">
                            <a className="nav-link ">Sign Up</a>
                        </Link>
                    </li>
                </ul>
            );
        }
    }
    render() {
        return (
            <nav className="navbar navbar-expand-lg navbar-dark bg-primary ">
                <Link href="/">
                    <a className="navbar-brand">Piron.io</a>
                </Link>
                <button
                    className="navbar-toggler"
                    type="button"
                    data-toggle="collapse"
                    data-target="#navbarNav"
                    aria-controls="navbarNav"
                    aria-expanded="false"
                    aria-label="Toggle navigation"
                >
                    <span className="navbar-toggler-icon"></span>
                </button>
                <div className="collapse navbar-collapse" id="navbarNav">
                    {this.getNav()}
                </div>
            </nav>
        );
    }
}

import IronApi from "../../modules/IronApi";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Link from "next/link";

export default class AppointmentExpandList extends React.Component {
    state = {
        isExpanded: false,
        appointments: []
    };
    componentDidMount() {
        this.loadAppointments();
    }
    async loadAppointments() {
        let iron = new IronApi();
        let appointments = await iron.getAppointments(null, new Date());
        if (appointments.status == 200) {
            this.setState({ appointments: appointments.data.appointments });
        }
    }
    handleClick(event) {
        let toggle = this.state.isExpanded === true ? false : true;
        this.setState({ isExpanded: toggle });
    }
    render(props) {
        let isExpanded = this.state.isExpanded;
        let items = this.state.appointments.map((appointment, i) => {
            return (
                <li className="nav-item" key={i}>
                    <Link href={`/appointments/view?id=${appointment.id}`}>
                        <a className="nav-link" href="#">
                            {appointment.notes}
                        </a>
                    </Link>
                </li>
            );
        });

        return (
            <div>
                <h6 className="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
                    <a
                        className="d-flex align-items-center text-muted"
                        href="#"
                        onClick={this.handleClick.bind(this)}
                    >
                        <span>
                            Upcoming Appointments{" "}
                            <FontAwesomeIcon
                                icon={isExpanded ? "minus" : "plus"}
                            />
                        </span>
                    </a>
                </h6>
                <ul
                    className={
                        isExpanded
                            ? "nav flex-column mb-2"
                            : "nav flex-column mb-2 d-none"
                    }
                >
                    {items}
                </ul>
            </div>
        );
    }
}

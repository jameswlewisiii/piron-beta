import Router from "next/router";
import SupportModal from "./supportModal";
import UpgradeModal from "./upgradeModal";
import Link from "next/link";
import Footer from "./footer";
import { library } from "@fortawesome/fontawesome-svg-core";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
    faTrash,
    faEye,
    faEdit,
    faCalendar,
    faHourglassStart,
    faHourglassEnd,
    faPlus,
    faDollarSign,
    faComment,
    faUser,
    faPhone,
    faEnvelope,
    faMapMarker,
    faBirthdayCake,
    faCog,
    faUsers,
    faTachometerAlt,
    faUserCircle,
    faChartBar,
    faMinus,
    faExclamationCircle,
    faSearch,
    faCaretRight,
    faCaretLeft,
    faCalendarCheck
} from "@fortawesome/free-solid-svg-icons";
import "../../node_modules/react-datepicker/dist/react-datepicker-cssmodules.min.css";
import Header from "./head";
import IronApi from "../../modules/IronApi";
import UserExpandList from "./userExpandList";
import AppointmentExpandList from "./appointmentExpandList";

library.add([
    faTrash,
    faEye,
    faEdit,
    faCalendar,
    faHourglassStart,
    faHourglassEnd,
    faPlus,
    faMinus,
    faDollarSign,
    faComment,
    faUser,
    faPhone,
    faEnvelope,
    faMapMarker,
    faBirthdayCake,
    faCog,
    faUsers,
    faTachometerAlt,
    faUserCircle,
    faChartBar,
    faExclamationCircle,
    faSearch,
    faCaretRight,
    faCaretLeft,
    faCalendarCheck
]);

export default class AppLayout extends React.Component {
    state = {
        auth: false,
        isProAccount: false
    };
    componentDidMount() {
        let auth = IronApi.authenticated();
        if (!auth) {
            Router.push("/login");
        }
        this.setState({
            auth: auth,
            user: IronApi.getUser(),
            isProAccount: IronApi.isProAccount()
        });
    }

    render(props) {
        let style = {
            backgroundColor: "#f5f8fa"
        };

        return (
            <div>
                <Header />
                <nav className="navbar navbar-expand-lg navbar-dark bg-primary p-1">
                    <button
                        className="navbar-toggler"
                        type="button"
                        data-toggle="collapse"
                        data-target="#navbarNav"
                        aria-controls="navbarNav"
                        aria-expanded="false"
                        aria-label="Toggle navigation"
                    >
                        <span className="navbar-toggler-icon"></span>
                    </button>
                    <div className="collapse navbar-collapse" id="navbarNav">
                        <ul className="navbar-nav">
                            <li className="nav-item">
                                <Link href="/">
                                    <a className="nav-link app-link">
                                        Piron.io
                                    </a>
                                </Link>
                            </li>
                        </ul>
                        <ul className="navbar-nav ml-auto">
                            <li className="nav-item">
                                {this.state.isProAccount && <SupportModal />}
                            </li>
                            <li className="nav-item">
                                {!this.state.isProAccount && <UpgradeModal />}
                            </li>
                            <li className="nav-item dropdown">
                                <a
                                    className="nav-link dropdown-toggle"
                                    href="#"
                                    id="navbarDropdown"
                                    role="button"
                                    data-toggle="dropdown"
                                    aria-haspopup="true"
                                    aria-expanded="false"
                                >
                                    {this.state.user
                                        ? this.state.user.email
                                        : "Account"}
                                </a>
                                <div
                                    className="dropdown-menu"
                                    aria-labelledby="navbarDropdown"
                                >
                                    <Link href="/dashboard">
                                        <a className="dropdown-item" href="#">
                                            Dashboard
                                        </a>
                                    </Link>
                                    <Link href="/settings">
                                        <a className="dropdown-item" href="#">
                                            Settings
                                        </a>
                                    </Link>
                                    <div className="dropdown-divider"></div>
                                    <Link href="/logout">
                                        <a className="dropdown-item">Log Out</a>
                                    </Link>
                                </div>
                            </li>
                        </ul>
                    </div>
                </nav>
                <div className="app container-fluid">
                    <div className="row">
                        <nav className="col-md-2 d-none d-md-block bg-light sidebar">
                            <div className="sidebar-sticky">
                                <ul className="nav flex-column">
                                    <li className="nav-item">
                                        <Link href="/dashboard">
                                            <a className="nav-link">
                                                <FontAwesomeIcon icon="tachometer-alt" />{" "}
                                                Dashboard
                                            </a>
                                        </Link>
                                    </li>
                                    <li className="nav-item">
                                        <Link href="/clients">
                                            <a className="nav-link">
                                                <FontAwesomeIcon icon="users" />{" "}
                                                Clients
                                            </a>
                                        </Link>
                                    </li>
                                    <li className="nav-item">
                                        <Link href="/transactions">
                                            <a className="nav-link">
                                                <FontAwesomeIcon icon="dollar-sign" />{" "}
                                                Transactions
                                            </a>
                                        </Link>
                                    </li>
                                    <li className="nav-item">
                                        <Link href="/appointments">
                                            <a className="nav-link">
                                                <FontAwesomeIcon icon="calendar" />{" "}
                                                Appointments
                                            </a>
                                        </Link>
                                    </li>
                                    {/* <li className="nav-item">
                                        <Link href="/appointments">
                                            <a className="nav-link"><FontAwesomeIcon icon="chart-bar" /> Reports</a>
                                        </Link>
                                    </li> */}
                                    {this.state.isProAccount && (
                                        <li className="nav-item">
                                            <Link href="/users">
                                                <a className="nav-link">
                                                    <FontAwesomeIcon icon="user-circle" />{" "}
                                                    Users
                                                </a>
                                            </Link>
                                        </li>
                                    )}
                                </ul>

                                <hr />
                                {this.state.isProAccount && <UserExpandList />}
                                {this.state.isProAccount && (
                                    <AppointmentExpandList />
                                )}
                            </div>
                        </nav>
                        <main
                            className="col-md-9 ml-sm-auto col-lg-10 px-4 pt-3"
                            style={style}
                        >
                            {this.props.children}
                            <Footer />
                        </main>
                    </div>
                </div>
            </div>
        );
    }
}

import Link from "next/link";

export default class Footer extends React.Component {
    render(props) {
        return (
            <footer className="page-footer bg-primary mt-5">
                <div className="container">
                    <div className="row">
                        <div className="mt-5 col-sm-3 col-xs-12">
                            <h4 className="text-white">Site</h4>
                            <p>
                                <Link href="/">
                                    <a href="#" className="text-secondary">
                                        Home
                                    </a>
                                </Link>
                            </p>
                            <p>
                                <Link href="/http://localhost/register/freemium">
                                    <a href="#" className="text-secondary">
                                        Register
                                    </a>
                                </Link>
                            </p>
                            <p>
                                <Link href="/login">
                                    <a href="#" className="text-secondary">
                                        Login
                                    </a>
                                </Link>
                            </p>
                            <p>
                                <Link href="/password-reset">
                                    <a href="#" className="text-secondary">
                                        Password Reset
                                    </a>
                                </Link>
                            </p>
                        </div>
                        <div className="mt-5 col-sm-3 col-xs-12">
                            <h4 className="text-white">Resources</h4>
                            <p>
                                <Link href="/quickstart">
                                    <a href="#" className="text-secondary">
                                        Documentation
                                    </a>
                                </Link>
                            </p>
                            {/* <p>
                                <Link href="/documentation">  
                                    <a href="#" className="text-secondary">Documentation</a>
                                </Link>
                            </p> */}
                            <p>
                                <Link href="/feedback">
                                    <a href="#" className="text-secondary">
                                        Send Us Feedback
                                    </a>
                                </Link>
                            </p>
                        </div>
                        <div className="mt-5 col-sm-3 col-xs-12">
                            <h4 className="text-white">Legal</h4>
                            <p>
                                <Link href="/terms">
                                    <a href="#" className="text-secondary">
                                        Terms & Services
                                    </a>
                                </Link>
                            </p>
                        </div>
                        <div className="mt-5 col-sm-3 col-xs-12">
                            <h4 className="text-white">Social</h4>
                            {/* <p>Instagram</p> */}
                            <p>
                                <a
                                    className="text-secondary"
                                    href="https://www.facebook.com/piron.ioapp/"
                                    target="_blank"
                                >
                                    Facebook
                                </a>
                            </p>
                        </div>
                    </div>
                </div>
                <div className="footer-copyright text-center py-3">
                    © 2020 Copyright:
                    <a href="/" className="text-warning">
                        {" "}
                        Piron.io
                    </a>
                </div>
            </footer>
        );
    }
}

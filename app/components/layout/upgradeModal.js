import IronApi from "../../modules/IronApi";
import UpdateCardForm from "../stripe/updateCardForm";
import { Elements, StripeProvider } from "react-stripe-elements";
export default class UpgradeModal extends React.Component {
    state = {
        user: null,
        stripe: null,
        errorMessage: null,
        loading: false
    };
    afterSubmit() {
        $("#upgradeModal").modal("toggle");
    }
    componentDidMount() {
        if (window.Stripe) {
            this.setState({
                stripe: window.Stripe(IronApi.getStripeKey())
            });
        }
    }
    render(props) {
        return (
            <div>
                <a
                    href="#"
                    className="nav-link btn-outline-warning"
                    role="button"
                    data-toggle="modal"
                    data-target="#upgradeModal"
                >
                    Upgrade Account
                </a>
                <div
                    className="modal fade"
                    id="upgradeModal"
                    tabIndex="-1"
                    role="dialog"
                    aria-labelledby="upgradeModal"
                    aria-hidden="true"
                >
                    <div className="modal-dialog" role="document">
                        <div className="modal-content">
                            <div className="modal-header">
                                <h5 className="modal-title">
                                    Upgrade to a pro account
                                </h5>
                                <button
                                    type="button"
                                    className="close"
                                    data-dismiss="modal"
                                    aria-label="Close"
                                >
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div className="modal-body">
                                <div className="">
                                    <div className="bg-warning">
                                        <div className="card-body">
                                            <h5 className="text-center">Pro</h5>
                                            <h3 className="display-3 text-center mt-3">
                                                $9.99
                                                <span className="h4">
                                                    / month
                                                </span>
                                            </h3>
                                        </div>
                                    </div>
                                    <div className="card-body text-center">
                                        <h5 className="card-title">
                                            30 day free trial{" "}
                                        </h5>
                                        <h5 className="card-title m-4">
                                            All freemium features{" "}
                                        </h5>
                                        <h5 className="card-title m-4">
                                            Unlimited User Accounts{" "}
                                        </h5>
                                        <h5 className="card-title m-4">
                                            Access to all features{" "}
                                        </h5>
                                        <h5 className="card-title m-4">
                                            Account support and help{" "}
                                        </h5>
                                    </div>
                                </div>
                                <p>
                                    Invoice will be automatically billed at the
                                    end of the 30 day free trial.
                                </p>
                                <StripeProvider stripe={this.state.stripe}>
                                    <Elements>
                                        <UpdateCardForm
                                            redirectPage="/dashboard"
                                            upgrade="true"
                                        />
                                    </Elements>
                                </StripeProvider>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

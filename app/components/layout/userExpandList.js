import IronApi from "../../modules/IronApi";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Link from "next/link";

export default class UserExpandList extends React.Component {
    state = {
        isExpanded: false,
        users: []
    };
    componentDidMount() {
        this.loadUsers();
    }
    async loadUsers() {
        let iron = new IronApi();
        let users = await iron.getAccountUsers();
        if (users.status == 200) {
            this.setState({ users: users.data.users });
        }
    }
    handleClick(event) {
        let toggle = this.state.isExpanded === true ? false : true;
        this.setState({ isExpanded: toggle });
    }
    render(props) {
        let isExpanded = this.state.isExpanded;
        let items = this.state.users.map((user, i) => {
            return (
                <li className="nav-item" key={i}>
                    <Link href={`/users/view?id=${user.id}`}>
                        <a className="nav-link" href="#">
                            {user.first_name} {user.last_name}
                        </a>
                    </Link>
                </li>
            );
        });

        return (
            <div>
                <h6 className="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
                    <a
                        className="d-flex align-items-center text-muted"
                        href="#"
                        onClick={this.handleClick.bind(this)}
                    >
                        <span>
                            Users{" "}
                            <FontAwesomeIcon
                                icon={isExpanded ? "minus" : "plus"}
                            />
                        </span>
                    </a>
                </h6>
                <ul
                    className={
                        isExpanded
                            ? "nav flex-column mb-2"
                            : "nav flex-column mb-2 d-none"
                    }
                >
                    {items}
                </ul>
            </div>
        );
    }
}

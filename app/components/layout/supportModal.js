import SupportForm from "../supportForm";
export default class SupportModal extends React.Component {
    afterSubmit() {
        $("#supportModal").modal("toggle");
    }
    render(props) {
        return (
            <div>
                <a
                    href="#"
                    className="nav-link"
                    role="button"
                    data-toggle="modal"
                    data-target="#supportModal"
                >
                    Support
                </a>
                <div
                    className="modal fade"
                    id="supportModal"
                    tabIndex="-1"
                    role="dialog"
                    aria-labelledby="supportModal"
                    aria-hidden="true"
                >
                    <div className="modal-dialog" role="document">
                        <div className="modal-content">
                            <div className="modal-header">
                                <h5 className="modal-title">
                                    Submit a support ticket
                                </h5>
                                <button
                                    type="button"
                                    className="close"
                                    data-dismiss="modal"
                                    aria-label="Close"
                                >
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div className="modal-body">
                                <SupportForm afterSubmit={this.afterSubmit} />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

import Link from "next/link";

export default class ViewHelper {
    static getCreatedByLink(user) {
        if (user) {
            if (user.first_name && user.last_name) {
                return (
                    <Link href={`/users/view?id=${user.id}`}>
                        <a>{user.first_name + " " + user.last_name}</a>
                    </Link>
                );
            }
        }
    }
}

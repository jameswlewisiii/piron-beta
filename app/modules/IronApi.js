import Axios from "axios";

// Setting freemium to 1 to give it access to the complete application
// Doing this because I am moving away from the paid version to a completely free app
// In order to get more users.
const FREEMIUM_PLAN_ID = 1;
const PRO_PLAN_ID = 1;

const ROLE_ADMIN = 1;
const ROLE_USER = 2;

const STRIPE_KEY = "pk_test_CYblZwS2zOf08PS71udBkMbz00D2yonCOl";

export default class IronApi {
    constructor() {
        this.token = sessionStorage.getItem("jwt");
        this.baseURL = "http://localhost:3000/";

        this.axios = Axios.create({
            baseURL: this.baseURL,
            // timeout: 1000,
            headers: { Authorization: this.token }
        });

        this.axios.interceptors.response.use(
            res => {
                if (res.data.update_token) {
                    sessionStorage.setItem(
                        "user",
                        JSON.stringify(res.data.update_token.user)
                    );
                }

                return res;
            },
            err => {
                if (err.request) {
                    return JSON.parse(err.request.response);
                } else {
                    return err;
                }
            }
        );
    }

    getBaseUrl() {
        return this.baseURL;
    }
    getToken() {
        return this.token;
    }

    /**
     * Auth API calls
     */
    async registerFreemium(data) {
        let url = "/register/freemium";
        let res = await this.axios.post(url, data);

        if (res.data.token) {
            sessionStorage.setItem("jwt", res.data.token);
            sessionStorage.setItem("user", JSON.stringify(res.data.user));
        }

        return res;
    }

    async registerPro(data) {
        let url = "/register/pro";
        let res = await this.axios.post(url, data);

        if (res.data.token) {
            sessionStorage.setItem("jwt", res.data.token);
            sessionStorage.setItem("user", JSON.stringify(res.data.user));
        }

        return res;
    }

    async login(email, password) {
        let url = "/login";
        let res = await this.axios.post(url, {
            // withCredentials: true,
            email: email,
            password: password
        });

        if (res.data.token) {
            sessionStorage.setItem("jwt", res.data.token);
            sessionStorage.setItem("user", JSON.stringify(res.data.user));
        }

        return res;
    }
    logout() {
        let url = "/logout";
        sessionStorage.removeItem("jwt");
        sessionStorage.removeItem("user");
        return this.axios.get(url);
    }
    static authenticated() {
        return sessionStorage.getItem("jwt") !== null;
    }
    static getUser() {
        let user = sessionStorage.getItem("user");
        return JSON.parse(user);
    }
    static setUser(data) {
        sessionStorage.setItem("user", JSON.stringify(data));
    }
    static isProAccount() {
        let user = JSON.parse(sessionStorage.getItem("user"));
        if (user) {
            return user.account.plan_id == PRO_PLAN_ID;
        }
        return false;
    }
    static isCurrentUserAdmin() {
        let user = JSON.parse(sessionStorage.getItem("user"));
        if (user) {
            return user.role_id == ROLE_ADMIN;
        }
        return false;
    }
    static getAdminRoleId() {
        return ROLE_ADMIN;
    }
    static getUserRoleId() {
        return ROLE_USER;
    }
    static getStripeKey() {
        return STRIPE_KEY;
    }
    resetPassword(email) {
        let url = "/password-reset";
        return this.axios.post(url, { email: email });
    }

    /**
     * Feedback API calls
     */
    sendFeedback(data) {
        let url = "/feedback";
        return this.axios.post(url, data);
    }

    /**
     * User API calls
     */
    createUser(data) {
        let url = "/user";
        return this.axios.post(url, data);
    }
    changeUserRole(userId, roleId) {
        let url = "/user/role/" + userId;
        return this.axios.put(url, { role_id: roleId });
    }
    getUser(id) {
        let url = "/user/" + id;
        return this.axios.get(url);
    }
    updateUser(data) {
        let url = "/user";
        return this.axios.put(url, data);
    }
    updateUserPassword(data) {
        let url = "/user/password";
        return this.axios.put(url, data);
    }
    getAccountUsers() {
        let url = "/user/account";
        return this.axios.get(url);
    }
    removeUserFromAccount(userId) {
        let url = "/user/account/" + userId;
        return this.axios.delete(url);
    }

    /**
     * Client API calls
     */
    createClient(data) {
        let url = "/client";
        return this.axios.post(url, data);
    }
    updateClient(id, data) {
        let url = "/client/" + id;
        return this.axios.put(url, data);
    }
    getClient(id) {
        let url = "/client/" + id;
        return this.axios.get(url);
    }
    getUserClients() {
        let url = "/clients/user";
        return this.axios.get(url);
    }
    deleteClient(id) {
        let url = "/client/" + id;
        return this.axios.delete(url);
    }
    getClients(lessThanDate, greaterThanDate) {
        let url = "/clients/account";
        let query = {
            lessThanDate: lessThanDate,
            greaterThanDate: greaterThanDate
        };
        return this.axios.get(url, { params: query });
    }
    getEmail() {
        let url = "/jwt";
        return this.axios.get(url);
    }
    /**
     * Transaction API Calls
     */

    createTransaction(data) {
        let url = "/transaction";
        return this.axios.post(url, data);
    }
    deleteTransaction(id) {
        let url = "/transaction/" + id;
        return this.axios.delete(url);
    }
    getTransaction(id) {
        let url = "/transaction/" + id;
        return this.axios.get(url);
    }
    getTransactions(lessThanDate, greaterThanDate) {
        let url = "/transaction/account";
        let query = {
            lessThanDate: lessThanDate,
            greaterThanDate: greaterThanDate
        };
        return this.axios.get(url, { params: query });
    }
    getUserTransactions() {
        let url = "/transaction/user";
        return this.axios.get(url);
    }
    updateTransaction(id, data) {
        let url = "/transaction/" + id;
        return this.axios.put(url, data);
    }

    /**
     * Appointment API calls
     */
    createAppointment(data) {
        let url = "/appointment";
        return this.axios.post(url, data);
    }
    getAppointments(lessThanDate, greaterThanDate) {
        let url = "/appointments/account";
        let query = {
            lessThanDate: lessThanDate,
            greaterThanDate: greaterThanDate
        };
        return this.axios.get(url, { params: query });
    }
    getUserAppointments() {
        let url = "/appointments/user";
        return this.axios.get(url);
    }
    updateAppointment(id, data) {
        let url = "/appointment/" + id;
        return this.axios.put(url, data);
    }

    deleteAppointment(id) {
        let url = "/appointment/" + id;
        return this.axios.delete(url);
    }

    getAppointment(id) {
        let url = "/appointment/" + id;
        return this.axios.get(url);
    }

    getDashboardData() {
        let url = "/analytics/dashboard";
        return this.axios.get(url);
    }

    /**
     * Account API calls
     */
    updateAccount(data) {
        let url = "/account";
        return this.axios.put(url, data);
    }
    getAccount() {
        let url = "/account";
        return this.axios.get(url);
    }
    deleteAccount() {
        let url = "/account";
        return this.axios.delete(url);
    }
    updateCardInfo(data) {
        let url = "/account/card";
        return this.axios.put(url, data);
    }
}

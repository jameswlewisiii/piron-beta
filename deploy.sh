#!/bin/bash
# My first script

production=0

echo "*****Bring the stack down*****"
if [ $production -gt 0 ]
then
    docker-compose down
else
    docker-compose -f docker-compose-dev.yml down
fi

echo "*****Removing old images*****"
if [ $production -gt 0 ]
then
    docker image rm piron-beta_app
    docker image rm piron-beta_api
else
    docker image rm iron_app
    docker image rm iron_api
fi

echo "*****Building new images*****"
if [ $production -gt 0 ]
then
    docker image build -t piron-beta_app app/.
    docker image build -t piron-beta_api api/.
else
    docker image build -t iron_app app/.
    docker image build -t iron_api api/.
fi

echo "*****Deploying the stack*****"
if [ $production -gt 0 ]
then
    docker-compose up -d
else
    docker-compose -f docker-compose-dev.yml up -d
fi
const Faker = require("faker");
const User = require("../models/user");
const Account = require("../models/account");

// Create new users
generateUsers(10);

async function generateUsers(amount) {
    try {
        for (let i = 0; i < amount; i++) {
            let account = await new Account().save({
                plan_id: Account.getProPlanId(),
                customer_id: Faker.random.uuid(),
                subscription_id: Faker.random.uuid(),
                address1: Faker.address.streetAddress(),
                city: Faker.address.city(),
                state: Faker.address.state(),
                zip: Faker.address.zipCode(),
                active: true
            });

            // Set up user
            let user = await new User({
                role_id: User.getAdminRoleId(),
                email: Faker.internet.email(),
                password: User.bcryptPassword("password"),
                account_id: account.get("id"),
                first_name: Faker.name.firstName(),
                last_name: Faker.name.lastName()
            }).save();

            console.log(user.get("firstName"));
        }

        return true;
    } catch (err) {
        console.log(err);
        return err;
    }
}

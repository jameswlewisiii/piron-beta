const Joi = require('joi');
const Transcation = require('../models/transaction');
const Session = require('../modules/session');
const Logger = require('../config/log');
const Jwt  = require('jsonwebtoken');
const Response = require('../modules/Response');
const DateTime = require('date-and-time');

let transactionValidation = {
    payload: {
        client_id: Joi.number().required().error(errors => {
            return {
                message: "Client is required. You must select a client."
            };
        }),
        description: Joi.string().required(),
        notes: Joi.string().required(),
        amount: Joi.string().required(),
        date: Joi.string().required(),
        start_time: Joi.string().required(),
        end_time: Joi.string().required(),
    },
    failAction: (request, h, err) => {
        throw err;
        return;
    }
};

let routes = [
    /**
     * Creates a new transaction record in the client_transactions table.
     */
    {
        method: "POST",
        path: "/transaction",
        options: {
            auth: 'jwt',
            validate: transactionValidation
        },
        handler: async(request, h) => {
            let data = request.payload;
            data.user_id = await Session.getUserId(request.auth.token);
            data.account_id = await Session.getUserAccountId(request.auth.token);
            data.date = DateTime.format(new Date(request.payload.date), 'YYYY-MM-DD');
            let transaction = await new Transcation(data).save();
            if(transaction) {
                return h.response(Response.build(true, 'Created a new transaction', {transaction: transaction.serialize()})).code(201);
            } else {
                return h.response(Response.build(false, 'Unable to create a new transaction')).code(200)
            }
        }
    },
    /**
     * Returns a transaction record based on id.
     */
    {
        method: "GET",
        path: "/transaction/{id}",
        options: {
            auth: 'jwt',
            validate: {
                params: {
                    id: Joi.number().required(),
                }
            }
        },
        handler: async (request, h) => {
            let id = request.params.id;
            let accountId = await Session.getUserAccountId(request.auth.token);
            let transaction = await Transcation.where({id:id, account_id: accountId}).fetch({withRelated: ['client', 'user']});
            if(!transaction) {
                return h.response(Response.build(false, 'No transaction found using id:' + id)).code(404);
            }
                
            return h.response(Response.build(true, 'Fetched a transaction', {transaction: transaction.serialize()})).code(200);
        }
    },
      /**
     * Returns all transaction based for a given account.
     */
    {
        method: "GET",
        path: "/transaction/account",
        options: {
            auth: 'jwt'
        },
        handler: async (request, h) => {
            let accountId = await Session.getUserAccountId(request.auth.token);
            let transactions = Transcation.where({account_id:accountId});

            if(request.query.lessThanDate) {
                transactions = transactions.where('date', '<', DateTime.format(new Date(request.query.lessThanDate), 'YYYY-MM-DD HH:mm:ss'));
            }
            if(request.query.greaterThanDate) {
                transactions = transactions.where('date', '>', DateTime.format(new Date(request.query.greaterThanDate), 'YYYY-MM-DD HH:mm:ss'));
            }

            transactions = await transactions.query('orderBy', 'date', 'DESC').fetchAll();

            return h.response(Response.build(
                true, 
                'Fetched all transactions by account id',
                {transactions: transactions.serialize()}
            )).code(200);
        }
    },
    /**
     * Returns all transaction based for a given user.
     */
    {
        method: "GET",
        path: "/transaction/user",
        options: {
            auth: 'jwt'
        },
        handler: async (request, h) => {
            let userId = await Session.getUserId(request.auth.token);
            let transactions = Transcation.where({user_id:userId});
            transactions = await transactions.query('orderBy', 'date', 'DESC').fetchAll();

            return h.response(Response.build(
                true, 
                'Fetched all transactions by user id',
                {transactions: transactions.serialize()}
            )).code(200);
        }
    },
    /**
     * Updates a transaction record based on id.
     */
    {
        method: "PUT",
        path: "/transaction/{id}",
        options: {
            auth: 'jwt',
            validate: transactionValidation
        },
        handler: async (request, h) => {
            let id = request.params.id;
            let accountId = await Session.getUserAccountId(request.auth.token);
            let transaction = await Transcation.where({id:id, account_id: accountId}).fetch({required:true});

            if(!transaction) {
                return h.response(Response.build(false, 'Unable to find a transaction with id: ' + id)).code(404);
            }

            let data = request.payload;
            data.user_id = await Session.getUserAccountId(request.auth.token);
            data.account_id = accountId
            data.date = DateTime.format(new Date(request.payload.date), 'YYYY-MM-DD');
            if(await transaction.save(data)) {
                return h.response(Response.build(true, 'Updated a transaction')).code(200);
            } else {
                Logger.log('error', 'Unable to update transaction. Id: ' + id, {
                    error: transaction,
                    time: Date()
                });
                return h.response(Response.build(false, 'Unable to update the transaction')).code(400)
            }
             
        }
    },
    /**
     * Deletes a client_transaction record.
     */
    {
        method: "DELETE",
        path: "/transaction/{id}",
        options: {
            auth: 'jwt',
            validate: {
                params: {
                    id: Joi.number().required(),
                }
            }
        },
        handler: async (request, h) => {
            let id = request.params.id;
            let accountId = await Session.getUserAccountId(request.auth.token);
            let transaction = await Transcation.where({id:id, account_id: accountId}).fetch();
            if(transaction) {
                await transaction.destroy({required:true});
                return h.response(Response.build(true, 'Delete a transaction')).code(200);
            } else {
                return h.response(Response.build(false, "No transaction found using id: " + id)).code(404);
            }
        }
    }
];

module.exports = { routes: function() { return routes } }
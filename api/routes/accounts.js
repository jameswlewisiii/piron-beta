const Joi = require("joi");
const jwt = require("jsonwebtoken");
const Account = require("../models/account");
const Session = require("../modules/session");
const Response = require("../modules/Response");
const Stripe = require("../modules/Stripe");
const Logger = require("../config/log");
const User = require("../models/user");

let routes = [
    /**
     * Get an account
     */
    {
        method: "GET",
        path: "/account",
        options: {
            auth: "jwt"
        },
        handler: async (request, h) => {
            let accountId = await Session.getUserAccountId(request.auth.token);
            let account = await Account.where({ id: accountId }).fetch();
            let customer = await Stripe.getCustomer(account.get("customer_id"));

            if (account) {
                return h
                    .response(
                        Response.build(true, "Fetching an account", {
                            account: account,
                            customer: customer
                        })
                    )
                    .code(200);
            } else {
                return h
                    .response(
                        Response.build(false, "Unable to get the account")
                    )
                    .code(400);
            }
        }
    },
    /**
     * Updates an account
     */
    {
        method: "PUT",
        path: "/account",
        options: {
            auth: "jwt",
            validate: {
                // params: {
                // id: Joi.number().required(),
                // },
                payload: {
                    name: Joi.string().allow(""),
                    address1: Joi.string().allow(""),
                    address2: Joi.string().allow(""),
                    city: Joi.string().allow(""),
                    state: Joi.string().allow(""),
                    zip: Joi.string().allow(""),
                    website: Joi.string().allow(""),
                    facebook: Joi.string().allow(""),
                    phone: Joi.string().allow("")
                },
                failAction: (request, h, err) => {
                    throw err;
                    return;
                }
            }
        },
        handler: async (request, h) => {
            let accountId = await Session.getUserAccountId(request.auth.token);
            let account = await Account.where({ id: accountId }).fetch();
            if (account) {
                let newAcc = await account.save(request.payload);
                return h
                    .response(
                        Response.build(true, "Updated an account", {
                            account: newAcc.serialize()
                        })
                    )
                    .code(200);
            } else {
                return h
                    .response(
                        Response.build(false, "Unable to update the account")
                    )
                    .code(400);
            }
        }
    },
    /**
     * Creates a new account.
     */
    {
        method: "POST",
        path: "/account",
        options: {
            validate: {
                payload: {
                    name: Joi.string().required(),
                    address1: Joi.string(),
                    address2: Joi.string(),
                    city: Joi.string(),
                    state: Joi.string(),
                    zip: Joi.string(),
                    website: Joi.string(),
                    facebook: Joi.string(),
                    phone: Joi.string(),
                    plan_id: Joi.number().required()
                }
            }
        },
        handler: async (request, h) => {
            let account = await new Account(request.payload).save();
            return h
                .response(
                    Response.build(true, "Account created", {
                        account: account.serialize()
                    })
                )
                .code(201);
        }
    },
    /**
     * Deletes an account.
     */
    {
        method: "DELETE",
        path: "/account",
        options: {
            auth: "jwt"
        },
        handler: async (request, h) => {
            let account = await Account.where({
                id: await Session.getUserAccountId(request.auth.token)
            }).fetch();

            if (account.get("plan_id") == Account.getFreemiumPlanId()) {
                // Delete freemium account
                account.save({ active: false }, { patch: true });
            } else {
                // Delete pro account

                // Delete stripe customer.
                let confimation = await Stripe.deleteCustomer(
                    account.get("customer_id")
                );

                if (confimation.deleted) {
                    // Downgrade account to freemium
                    await account.save(
                        {
                            customer_id: null,
                            subscription_id: null,
                            plan_id: Account.getFreemiumPlanId(),
                            active: false
                        },
                        { patch: true }
                    );
                }
            }

            // Remove session
            let sessionId = await jwt.decode(request.auth.token).sessionId;
            Session.deleteUserSession(sessionId);

            return h
                .response(
                    Response.build(true, "Account deleted", {
                        account: account.serialize()
                    })
                )
                .code(200);
        }
    },
    /**
     * Updates an account's card info.
     */
    {
        method: "PUT",
        path: "/account/card",
        options: {
            auth: "jwt",
            validate: {
                payload: {
                    stripe_token: Joi.required(),
                    account_upgrade: Joi.boolean()
                }
            }
        },
        handler: async (request, h) => {
            let customer = null;
            let updateData = null;
            let stripeToken = request.payload.stripe_token;
            let accountId = await Session.getUserAccountId(request.auth.token);
            let userId = await Session.getUserId(request.auth.token);
            let account = await Account.where({
                id: accountId
            }).fetch();

            try {
                // Add credit card to customer account.
                customer = await Stripe.updateCustomerSource(
                    account.get("customer_id"),
                    stripeToken
                );

                if (request.payload.account_upgrade) {
                    Logger.info(
                        "UpdateCardInfo::Attempting to upgrade a user account to pro."
                    );

                    // Subscript to pro plan
                    let subscription = await Stripe.createProPlanSubscription(
                        customer
                    );

                    // Update account record
                    await account.save(
                        {
                            subscription_id: subscription.id,
                            plan_id: Account.getProPlanId()
                        },
                        { patch: true }
                    );

                    // Build update session data
                    updateData = {
                        token: await Session.updateUserSession(
                            request.auth.token,
                            userId,
                            accountId
                        ),
                        user: await User.where({ id: userId }).fetch({
                            withRelated: ["account"]
                        })
                    };
                }
            } catch (err) {
                Logger.info(
                    "UpdateCardInfo::Failed to update card info. " + err
                );
                return h
                    .response(
                        Response.build(
                            false,
                            "We are currently unable to process your request. Error: " +
                                err
                        )
                    )
                    .code(200);
            }

            return h
                .response(
                    Response.build(true, "Account card info updated", {
                        account: account,
                        update_token: updateData
                    })
                )
                .code(200);
        }
    }
];

module.exports = {
    routes: function() {
        return routes;
    }
};

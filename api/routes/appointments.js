const Joi = require("joi");
const Appointment = require("../models/appointment");
const Logger = require("../config/log");
const Jwt = require("jsonwebtoken");
const Session = require("../modules/session");
const DateTime = require("date-and-time");
const Response = require("../modules/Response");

let routes = [
    /**
     * Creates a new appointment record in the appointment table.
     */
    {
        method: "POST",
        path: "/appointment",
        options: {
            auth: "jwt",
            validate: {
                payload: {
                    date: Joi.date().required(),
                    start_time: Joi.string(),
                    end_time: Joi.string(),
                    client_id: Joi.number(),
                    notes: Joi.string()
                },
                failAction: (request, h, err) => {
                    throw err;
                    return;
                }
            }
        },
        handler: async (request, h) => {
            let data = request.payload;
            data.user_id = await Session.getUserId(request.auth.token);
            data.account_id = await Session.getUserAccountId(
                request.auth.token
            );
            data.date = DateTime.format(
                new Date(request.payload.date),
                "YYYY-MM-DD"
            );
            let appointment = await new Appointment(data).save();

            if (!appointment) {
                return h
                    .response(
                        Response.build(false, "Could not create an appointment")
                    )
                    .code(400);
            }

            return h
                .response(
                    Response.build(true, "Created a new appointment", {
                        appointment: appointment.serialize()
                    })
                )
                .code(201);
        }
    },
    {
        method: "GET",
        path: "/appointments/account",
        options: {
            auth: "jwt",
            validate: {
                query: {
                    greaterThanDate: Joi.string(),
                    lessThanDate: Joi.string()
                }
            }
        },
        handler: async (request, h) => {
            try {
                let sessionId = Jwt.decode(request.auth.token).sessionId;
                let sessionData = await Session.getUserSession(sessionId);
                let appointments = Appointment.where({
                    account_id: sessionData.accountId
                });
                if (request.query.lessThanDate) {
                    appointments = appointments.where(
                        "date",
                        "<",
                        DateTime.format(
                            new Date(request.query.lessThanDate),
                            "YYYY-MM-DD"
                        )
                    );
                }
                if (request.query.greaterThanDate) {
                    appointments = appointments.where(
                        "date",
                        ">",
                        DateTime.format(
                            new Date(request.query.greaterThanDate),
                            "YYYY-MM-DD"
                        )
                    );
                }
                appointments = await appointments
                    .orderBy("date", "DESC")
                    .fetchAll({ withRelated: ["client"] });

                if (appointments) {
                    return h
                        .response(
                            Response.build(
                                true,
                                "Fetched all appointments account id",
                                { appointments: appointments }
                            )
                        )
                        .code(200);
                } else {
                    Logger.log(
                        "error",
                        "Unable get all appointments by account",
                        {
                            error: appointment,
                            time: Date()
                        }
                    );
                    return h
                        .response(
                            Response.build(
                                false,
                                "Failed to fetched all appointments account id",
                                { error: appointment }
                            )
                        )
                        .code(200);
                }
            } catch (err) {
                Logger.log("error", "Error: " + err, {
                    error: err,
                    time: Date()
                });
                return h
                    .response(
                        Response.build(
                            false,
                            "Failed to fetched all appointments account id",
                            { error: err }
                        )
                    )
                    .code(400);
            }
        }
    },
    /**
     * Returns all appointments based for a given user.
     */
    {
        method: "GET",
        path: "/appointments/user",
        options: {
            auth: "jwt"
        },
        handler: async (request, h) => {
            let userId = await Session.getUserId(request.auth.token);
            let appointments = await Appointment.where({ user_id: userId })
                .query("orderBy", "date", "DESC")
                .fetchAll({ withRelated: ["client"] });

            return h
                .response(
                    Response.build(
                        true,
                        "Fetched all appointments by user id",
                        { appointments: appointments }
                    )
                )
                .code(200);
        }
    },
    {
        method: "GET",
        path: "/appointment/{id}",
        options: {
            auth: "jwt",
            validate: {
                params: {
                    id: Joi.number().required()
                }
            }
        },
        handler: async (request, h) => {
            let id = request.params.id;
            let accountId = await Session.getUserAccountId(request.auth.token);
            let appointment = await Appointment.where({
                account_id: accountId,
                id: id
            }).fetch({ withRelated: ["client", "user"] });
            if (appointment) {
                return h
                    .response(
                        Response.build(true, "Fetched an appointment", {
                            appointment: appointment.serialize()
                        })
                    )
                    .code(200);
            } else {
                return h
                    .response(
                        Response.build(
                            false,
                            "Unable get appointment with id:" + id
                        )
                    )
                    .code(404);
            }
        }
    },
    /**
     * Deletes an appointment.
     */
    {
        method: "DELETE",
        path: "/appointment/{id}",
        options: {
            auth: "jwt",
            validate: {
                params: {
                    id: Joi.number().required()
                }
            }
        },
        handler: async (request, h) => {
            let id = request.params.id;
            let accountId = await Session.getUserAccountId(request.auth.token);
            let appointment = await Appointment.where({
                account_id: accountId,
                id: id
            }).fetch();
            if (appointment) {
                await appointment.destroy({ required: true });
                return h
                    .response(Response.build(true, "Appointment deleted"))
                    .code(200);
            } else {
                return h
                    .response(
                        Response.build(false, "Unable to find appointment")
                    )
                    .code(404);
            }
        }
    },

    /**
     * Updates an appointment record based on id.
     */
    {
        method: "PUT",
        path: "/appointment/{id}",
        options: {
            auth: "jwt",
            validate: {
                params: {
                    id: Joi.number().required()
                },
                payload: {
                    date: Joi.date().required(),
                    start_time: Joi.string(),
                    end_time: Joi.string(),
                    client_id: Joi.number(),
                    notes: Joi.string()
                },
                failAction: (request, h, err) => {
                    throw err;
                    return;
                }
            }
        },
        handler: async (request, h) => {
            let id = request.params.id;
            let data = request.payload;
            let accountId = await Session.getUserAccountId(request.auth.token);
            let appointment = await Appointment.where({
                account_id: accountId,
                id: id
            }).fetch();
            if (appointment) {
                data.date = DateTime.format(
                    new Date(request.payload.date),
                    "YYYY-MM-DD"
                );
                appointment.save(data, { patch: true });
                return h
                    .response(Response.build(true, "Appointment updated"))
                    .code(200);
            } else {
                return h
                    .response(
                        Response.build(false, "Appointment failed to update")
                    )
                    .code(400);
            }
        }
    }
];

module.exports = {
    routes: function() {
        return routes;
    }
};

const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const User = require("../models/user");
const Account = require("../models/account");
const Session = require("../modules/session");
const Joi = require("joi");
const Mailer = require("../modules/Mailer");
const Response = require("../modules/Response");
const Stripe = require("../modules/Stripe");
const Logger = require("../config/log");
require("dotenv").load();

let routes = [
    /**
     * Logout of current account.
     */
    {
        method: "GET",
        path: "/logout",
        options: {
            auth: "jwt"
        },
        handler: (request, h) => {
            let sessionId = jwt.decode(request.auth.token).sessionId;
            Session.deleteUserSession(sessionId);
            return h.response(Response.build(true, "success")).code(200);
        }
    },
    /**
     * Reset user password
     */
    {
        method: "POST",
        path: "/password-reset",
        options: {
            validate: {
                payload: {
                    email: Joi.string()
                        .email({ minDomainAtoms: 2 })
                        .required()
                },
                failAction: (request, h, err) => {
                    throw err;
                    return;
                }
            }
        },
        handler: async (request, h) => {
            let user = await User.where({
                email: request.payload.email
            }).fetch();

            if (user) {
                let password = await User.generateTemporaryPassword();
                let hashed = await User.bcryptPassword(password);
                user.save({ password: hashed }, { patch: true });
                Mailer.sendMessage(
                    "info@piron.io",
                    user.get("email"),
                    "Password Reset Request",
                    "Your new password is " + password + "."
                );
                return h
                    .response(Response.build(true, "Password reset successful"))
                    .code(200);
            }

            return h
                .response(
                    Response.build(
                        false,
                        "Unable to find an account using that email address."
                    )
                )
                .code(400);
        }
    },
    /**
     * Login to an existing account.
     */
    {
        method: ["POST"],
        path: "/login",
        options: {
            validate: {
                payload: {
                    email: Joi.string()
                        .email({ minDomainAtoms: 2 })
                        .required(),
                    password: Joi.string().required()
                },
                failAction: (request, h, err) => {
                    throw err;
                    return;
                }
            }
        },
        handler: async (request, h) => {
            let { email, password } = request.payload;
            let user = await User.where({ email: email }).fetch({
                withRelated: ["account"]
            });

            if (user && bcrypt.compareSync(password, user.get("password"))) {
                let payload = {
                    exp: Math.floor(Date.now() / 1000) + 60 * 60,
                    sessionId: Session.setUserSession(user)
                };
                let token = jwt.sign(payload, process.env.JWT_SECERT, {
                    algorithm: process.env.JWT_ALGORITHM
                });
                return h
                    .response(
                        Response.build(true, "Successfully logged in.", {
                            token: token,
                            user: user
                        })
                    )
                    .code(200);
            }

            return h
                .response(
                    Response.build(
                        false,
                        "The username or password is invalid."
                    )
                )
                .code(200);
        }
    },
    /**
     * Register for a new freemium user account.
     */
    {
        method: "POST",
        path: "/register/freemium",
        options: {
            validate: {
                payload: {
                    email: Joi.string()
                        .email({ minDomainAtoms: 2 })
                        .required(),
                    password: Joi.string().required(),
                    first_name: Joi.string().required(),
                    last_name: Joi.string().required()
                },
                failAction: (request, h, err) => {
                    throw err;
                    return;
                }
            }
        },
        handler: async (request, h) => {
            let { email, password, first_name, last_name } = request.payload;
            let hashedPassword = await bcrypt.hash(password, 10);
            let emailExist = await User.where({ email: email }).fetch();

            // Verify a user doesn't already have this email
            if (emailExist) {
                return h
                    .response(
                        Response.build(
                            false,
                            "An account has already been registered using with that email address."
                        )
                    )
                    .code(200);
            }

            try {
                // Create stripe customer
                let customer = await Stripe.createCustomer(email, null);

                // Set up freemium account
                let account = await new Account().save({
                    plan_id: Account.getFreemiumPlanId(),
                    customer_id: customer.id
                });

                // Set up user
                var user = await new User({
                    role_id: User.getAdminRoleId(),
                    email: email,
                    password: hashedPassword,
                    account_id: account.get("id"),
                    first_name: first_name,
                    last_name: last_name
                }).save();
            } catch (err) {
                Logger.info(
                    "FreemiumSignUp::Failed to sign up a customer for a freemium account. " +
                        err
                );
            }

            if (user) {
                Mailer.sendUserRegisterMessage(email);
            }

            // Set up user session and token
            let payload = {
                exp: Math.floor(Date.now() / 1000) + 60 * 60,
                sessionId: Session.setUserSession(user)
            };
            let token = jwt.sign(payload, process.env.JWT_SECERT, {
                algorithm: process.env.JWT_ALGORITHM
            });

            return h
                .response(
                    Response.build(true, "An account has been created", {
                        token: token,
                        user: await User.where({ email: email }).fetch({
                            withRelated: ["account"]
                        })
                    })
                )
                .code(201);
        }
    },
    /**
     * Register for a new PRO user account.
     */
    {
        method: "POST",
        path: "/register/pro",
        options: {
            validate: {
                payload: {
                    stripe_token: Joi.required(),
                    email: Joi.string()
                        .email({ minDomainAtoms: 2 })
                        .required(),
                    password: Joi.string().required(),
                    first_name: Joi.string().required(),
                    last_name: Joi.string().required(),
                    address: Joi.string().required(),
                    state: Joi.string().required(),
                    zip: Joi.string().required(),
                    city: Joi.string().required()
                },
                failAction: (request, h, err) => {
                    throw err;
                    return;
                }
            }
        },
        handler: async (request, h) => {
            let {
                email,
                password,
                first_name,
                last_name,
                stripe_token,
                address,
                city,
                state,
                zip
            } = request.payload;
            let hashedPassword = await bcrypt.hash(password, 10);
            let emailExist = await User.where({ email: email }).fetch();

            // Verify a user doesn't already have this email
            if (emailExist) {
                return h
                    .response(
                        Response.build(
                            false,
                            "An account has already been registered using with that email address."
                        )
                    )
                    .code(200);
            }

            try {
                // Create stripe customer with payment
                let customer = await Stripe.createCustomer(email, stripe_token);
                // Subscript customer to pro plan
                let subscription = await Stripe.createProPlanSubscription(
                    customer
                );
                // Set up pro account
                let account = await new Account().save({
                    plan_id: Account.getProPlanId(),
                    customer_id: customer.id,
                    subscription_id: subscription.id,
                    address1: address,
                    city: city,
                    state: state,
                    zip: zip,
                    active: true
                });

                // Set up user
                var user = await new User({
                    role_id: User.getAdminRoleId(),
                    email: email,
                    password: hashedPassword,
                    account_id: account.get("id"),
                    first_name: first_name,
                    last_name: last_name
                }).save();
            } catch (err) {
                Logger.info(
                    "ProSignUp::Failed to sign up a customer for a pro account. " +
                        err
                );
                return h
                    .response(
                        Response.build(
                            false,
                            "We are currently unable to process your request. Error: " +
                                err
                        )
                    )
                    .code(200);
            }

            // Email user welcome letter
            if (user) {
                Mailer.sendUserRegisterMessage(email);
            }

            // Set up user session and token
            let payload = {
                exp: Math.floor(Date.now() / 1000) + 60 * 60,
                sessionId: Session.setUserSession(user)
            };
            let token = jwt.sign(payload, process.env.JWT_SECERT, {
                algorithm: process.env.JWT_ALGORITHM
            });

            return h
                .response(
                    Response.build(true, "An account has been created", {
                        token: token,
                        user: await User.where({ email: email }).fetch({
                            withRelated: ["account"]
                        })
                    })
                )
                .code(201);
        }
    }
];

module.exports = {
    routes: function() {
        return routes;
    }
};

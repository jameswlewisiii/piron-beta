const Joi = require("joi");
const Feedback = require("../models/feedback");
const Response = require("../modules/Response");
const Mailer = require("../modules/Mailer");
const Account = require("../models/account");
const Session = require("../modules/session");

let routes = [
    /**
     * Store feedback
     */
    {
        method: "POST",
        path: "/feedback",
        options: {
            validate: {
                payload: {
                    first_name: Joi.string()
                        .allow("")
                        .required(),
                    last_name: Joi.string()
                        .allow("")
                        .required(),
                    email: Joi.string()
                        .email()
                        .required(),
                    message: Joi.string().required(),
                    proAccount: Joi.bool()
                },
                failAction: (request, h, err) => {
                    throw err;
                    return;
                }
            }
        },
        handler: async (request, h) => {
            new Feedback({
                first_name: request.payload.first_name,
                last_name: request.payload.last_name,
                message: request.payload.message,
                email: request.payload.email
            }).save();
            let subject = request.payload.proAccount
                ? "Support Ticket"
                : "New Feedback";

            Mailer.sendMessage(
                "info@piron.io",
                process.env.ADMIN_EMAIL,
                subject,
                request.payload.message + " FROM: " + request.payload.email
            );
            return h
                .response(Response.build(true, "Stored feedback"))
                .code(201);
        }
    }
];

module.exports = {
    routes: function() {
        return routes;
    }
};

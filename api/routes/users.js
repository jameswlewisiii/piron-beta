const Joi = require("joi");
const User = require("../models/user");
const Session = require("../modules/session");
const bcrypt = require("bcrypt");
const Response = require("../modules/Response");
const Mailer = require("../modules/Mailer");

let routes = [
    /**
     * Fetches a users info
     */
    {
        method: "GET",
        path: "/user/{id}",
        options: {
            auth: "jwt",
            validate: {
                params: {
                    id: Joi.number().required()
                }
            }
        },
        handler: async (request, h) => {
            let id = request.params.id;
            let accountId = await Session.getUserAccountId(request.auth.token);
            let user = await User.where({
                id: id
            }).fetch({
                withRelated: ["appointments", "transactions", "clients"]
            });

            if (!user) {
                return h
                    .response(Response.build(false, "User not found."))
                    .code(404);
            }

            if (user.get("account_id") != accountId) {
                return h
                    .response(Response.build(false, "Not authorized."))
                    .code(401);
            }

            return h
                .response(Response.build(true, "User fetched.", { user: user }))
                .code(200);
        }
    },
    /**
     * Create a new user under the current account.
     */
    {
        method: "POST",
        path: "/user",
        options: {
            auth: "jwt",
            validate: {
                payload: {
                    first_name: Joi.string().required(),
                    last_name: Joi.string().required(),
                    email: Joi.string()
                        .email()
                        .required()
                },
                failAction: (request, h, err) => {
                    throw err;
                    return;
                }
            }
        },
        handler: async (request, h) => {
            let { email, first_name, last_name } = request.payload;
            let admin = await User.where({
                id: await Session.getUserId(request.auth.token)
            }).fetch();

            // Verify the email isn't already being used.
            if (await User.where({ email: email }).fetch()) {
                return h
                    .response(
                        Response.build(
                            false,
                            "Email already being used by another account."
                        )
                    )
                    .code(400);
            }

            // Verify the admin's account is pro.
            if (!admin.isAdmin()) {
                return h
                    .response(Response.build(false, "User unauthorized"))
                    .code(401);
            }

            let password = User.generateTemporaryPassword();
            let hash = await User.bcryptPassword(password);
            let user = await new User({
                role_id: User.getUserRoleId(),
                email: email,
                first_name: first_name,
                last_name: last_name,
                password: hash,
                account_id: admin.get("account_id")
            }).save();

            if (user) {
                Mailer.sendNewUserMessage(email, password);
            }

            return h
                .response(
                    Response.build(true, "New user created", { user: user })
                )
                .code(201);
        }
    },
    /**
     * Update a user
     */
    {
        method: "PUT",
        path: "/user",
        options: {
            auth: "jwt",
            validate: {
                payload: {
                    first_name: Joi.string().allow(""),
                    last_name: Joi.string().allow(""),
                    email: Joi.string().email()
                },
                failAction: (request, h, err) => {
                    throw err;
                    return;
                }
            }
        },
        handler: async (request, h) => {
            let userId = await Session.getUserId(request.auth.token);
            let user = await User.where({ id: userId }).fetch();

            if (request.payload.email) {
                let userEmailExist = await User.where({
                    email: request.payload.email
                }).fetch();

                if (
                    userEmailExist &&
                    userEmailExist.get("email") != user.get("email")
                ) {
                    return h
                        .response(
                            Response.build(
                                false,
                                "This email is already being used by another user."
                            )
                        )
                        .code(400);
                }
            }

            if (user) {
                await user.save(request.payload, {
                    patch: true
                });
                return h
                    .response(
                        Response.build(true, "Updated a user", {
                            user: {
                                email: user.get("email"),
                                first_name: user.get("first_name"),
                                last_name: user.get("last_name")
                            }
                        })
                    )
                    .code(200);
            } else {
                return h
                    .response(
                        Response.build(false, "Unable to update the user")
                    )
                    .code(400);
            }
        }
    },
    /**
     * Update user password
     */
    {
        method: "PUT",
        path: "/user/password",
        options: {
            auth: "jwt",
            validate: {
                payload: {
                    current_password: Joi.string().required(),
                    new_password: Joi.string().required(),
                    confirm_new_password: Joi.string().required()
                },
                failAction: (request, h, err) => {
                    throw err;
                    return;
                }
            }
        },
        handler: async (request, h) => {
            let {
                current_password,
                new_password,
                confirm_new_password
            } = request.payload;
            if (new_password != confirm_new_password)
                return h
                    .response(
                        Response.build(
                            false,
                            "Confirm New Password does not match New Password"
                        )
                    )
                    .code(400);

            let userId = await Session.getUserId(request.auth.token);
            let user = await User.where({ id: userId }).fetch();

            if (
                user &&
                bcrypt.compareSync(current_password, user.get("password"))
            ) {
                let hashed = await bcrypt.hash(new_password, 10);
                user.save({ password: hashed }, { patch: true });
                return h
                    .response(Response.build(true, "Updated password"))
                    .code(200);
            } else {
                return h
                    .response(
                        Response.build(false, "Incorrect current password.")
                    )
                    .code(400);
            }
        }
    },
    /**
     * Returns all users based on the given account.
     */
    {
        method: "GET",
        path: "/user/account",
        options: {
            auth: "jwt"
        },
        handler: async (request, h) => {
            let accountId = await Session.getUserAccountId(request.auth.token);
            let userId = await Session.getUserId(request.auth.token);
            let users = await User.where({ account_id: accountId }).fetchAll();
            return h
                .response(
                    Response.build(true, "Fetched all users by account", {
                        users: users
                    })
                )
                .code(200);
        }
    },
    /**
     * Remove user from account
     */
    {
        method: "DELETE",
        path: "/user/account/{id}",
        options: {
            auth: "jwt",
            validate: {
                params: {
                    id: Joi.number().required()
                }
            }
        },
        handler: async (request, h) => {
            let id = request.params.id;
            let accountId = await Session.getUserAccountId(request.auth.token);
            let user = await User.where({
                id: id,
                account_id: accountId
            }).fetch();

            if (user) {
                let latestUser = await User.query(
                    "orderBy",
                    "account_id",
                    "DESC",
                    {
                        limit: 1
                    }
                ).fetch();
                let newAccountId = latestUser.get("account_id") + 1;
                if (await user.changeAccount(newAccountId)) {
                    return h
                        .response(
                            Response.build(true, "User removed from account")
                        )
                        .code(200);
                }
            } else {
                return h
                    .response(
                        Response.build(
                            true,
                            "Could not find user on the account by that id"
                        )
                    )
                    .code(404);
            }
        }
    },
    /**
     * Updates user's role by id.
     */
    {
        method: "put",
        path: "/user/role/{id}",
        options: {
            auth: "jwt",
            validate: {
                payload: {
                    role_id: Joi.number().required()
                },
                params: {
                    id: Joi.number().required()
                },
                failAction: (request, h, err) => {
                    throw err;
                    return;
                }
            }
        },
        handler: async (request, h) => {
            let accountId = await Session.getUserAccountId(request.auth.token);
            let role_id = request.payload.role_id;
            let admin = await User.where({
                id: await Session.getUserId(request.auth.token),
                account_id: accountId
            }).fetch();
            let user = await User.where({
                id: request.params.id,
                account_id: accountId
            }).fetch();

            // Make sure user exist.
            if (!user) {
                return h
                    .response(
                        Response.build(
                            false,
                            "Could not find user on the account by that id"
                        )
                    )
                    .code(404);
            }

            // Make sure admin has the admin role.
            if (!admin.isAdmin()) {
                return h
                    .response(Response.build(false, "User unauthorized"))
                    .code(401);
            }

            user.setRole(role_id);
            user.save();
            return h
                .response(Response.build(true, "User role updated"))
                .code(200);
        }
    }
];

module.exports = {
    routes: function() {
        return routes;
    }
};

const Joi = require("joi");
const Client = require("../models/client");
const Jwt = require("jsonwebtoken");
const Session = require("../modules/session");
const Logger = require("../config/log");
const Response = require("../modules/Response");
const DateTime = require("date-and-time");

/**
 * Validate data before createing/updating client model.
 */
let clientValidation = {
    payload: {
        first_name: Joi.string()
            .min(1)
            .max(10)
            .required(),
        last_name: Joi.string()
            .min(1)
            .max(10)
            .required(),
        email: Joi.string().email({ minDomainAtoms: 2 }),
        phone: Joi.string(),
        address1: Joi.string(),
        address2: Joi.string(),
        city: Joi.string(),
        state: Joi.string(),
        zip: Joi.string(),
        dob: Joi.string()
    },
    failAction: (request, h, err) => {
        throw err;
        return;
    }
};
let routes = [
    /**
     * Creates a new client record in the clients table.
     */
    {
        method: "POST",
        path: "/client",
        options: {
            auth: "jwt",
            validate: clientValidation
        },
        handler: async (request, h) => {
            let accountId = await Session.getUserAccountId(request.auth.token);
            let userId = await Session.getUserId(request.auth.token);
            request.payload.dob = DateTime.format(
                new Date(request.payload.dob),
                "YYYY-MM-DD"
            );
            request.payload.account_id = accountId;
            request.payload.user_id = userId;
            let client = await new Client(request.payload).save();
            if (client) {
                return h
                    .response(
                        Response.build(true, "New client created", {
                            client: client.serialize()
                        })
                    )
                    .code(201);
            } else {
                Logger.log("error", "Unable to create new client", {
                    error: reason,
                    time: Date()
                });
                return h
                    .response(
                        Response.build(false, "Unable to create new client")
                    )
                    .code(400);
            }
        }
    },
    /**
     * Returns a client record based on id.
     */
    {
        method: "GET",
        path: "/client/{id}",
        options: {
            auth: "jwt",
            validate: {
                params: {
                    id: Joi.number().required()
                }
            }
        },
        handler: async (request, h) => {
            let id = request.params.id;
            let accountId = await Session.getUserAccountId(request.auth.token);
            let client = await Client.where({
                id: id,
                account_id: accountId
            }).fetch({ withRelated: ["appointments", "transactions", "user"] });
            if (client) {
                return h
                    .response(
                        Response.build(true, "Fetched client", {
                            client: client.serialize()
                        })
                    )
                    .code(200);
            } else {
                return h
                    .response(
                        Response.build(false, "No client found with id: " + id)
                    )
                    .code(404);
            }
        }
    },
    /**
     * Returns all clients for the given account.
     */
    {
        method: "GET",
        path: "/clients/account",
        options: {
            auth: "jwt",
            validate: {
                query: {
                    greaterThanDate: Joi.string(),
                    lessThanDate: Joi.string()
                }
            }
        },
        handler: async (request, h) => {
            let accountId = await Session.getUserAccountId(request.auth.token);
            let clients = Client.where({ account_id: accountId });

            if (request.query.lessThanDate) {
                clients = clients.where(
                    "created_at",
                    "<",
                    DateTime.format(
                        new Date(request.query.lessThanDate),
                        "YYYY-MM-DD HH:mm:ss"
                    )
                );
            }
            if (request.query.greaterThanDate) {
                clients = clients.where(
                    "created_at",
                    ">",
                    DateTime.format(
                        new Date(request.query.greaterThanDate),
                        "YYYY-MM-DD HH:mm:ss"
                    )
                );
            }

            clients = await clients.fetchAll();

            if (clients) {
                return h
                    .response(
                        Response.build(true, "Fetched clients", {
                            clients: clients.serialize()
                        })
                    )
                    .code(200);
            } else {
                return h
                    .response(
                        Response.build(false, "No clients found with id: " + id)
                    )
                    .code(404);
            }
        }
    },
    /**
     * Returns all clients for the given user id.
     */
    {
        method: "GET",
        path: "/clients/user",
        options: {
            auth: "jwt"
        },
        handler: async (request, h) => {
            let userId = await Session.getUserId(request.auth.token);
            let clients = await Client.where({ user_id: userId })
                .orderBy("created_at", "DESC")
                .fetchAll();
            if (clients) {
                return h
                    .response(
                        Response.build(true, "Fetched clients", {
                            clients: clients.serialize()
                        })
                    )
                    .code(200);
            } else {
                return h
                    .response(
                        Response.build(
                            false,
                            "No clients found with id: " + userId
                        )
                    )
                    .code(404);
            }
        }
    },
    /**
     * Updates a client record based on id.
     */
    {
        method: "PUT",
        path: "/client/{id}",
        options: {
            auth: "jwt",
            validate: clientValidation
        },
        handler: async (request, h) => {
            let id = request.params.id;
            let accountId = await Session.getUserAccountId(request.auth.token);
            let client = await Client.where({
                id: id,
                account_id: accountId
            }).fetch();

            if (client) {
                request.payload.dob = DateTime.format(
                    new Date(request.payload.dob),
                    "YYYY-MM-DD"
                );
                request.payload.account_id = accountId;
                await client.save(request.payload);
                return h
                    .response(
                        Response.build(true, "Updated the client", {
                            client: client.serialize()
                        })
                    )
                    .code(200);
            } else {
                return h
                    .response(
                        Response.build(
                            false,
                            "Unable to find a client with id: " + id
                        )
                    )
                    .code(404);
            }
        }
    },
    /**
     * Deletes a client.
     */
    {
        method: "DELETE",
        path: "/client/{id}",
        options: {
            auth: "jwt",
            validate: {
                params: {
                    id: Joi.number().required()
                }
            }
        },
        handler: async (request, h) => {
            let id = request.params.id;
            let accountId = await Session.getUserAccountId(request.auth.token);
            let client = await Client.where({
                id: id,
                account_id: accountId
            }).fetch();
            if (client) {
                await client.destroy();
                return h
                    .response(Response.build(true, "Client deleted"))
                    .code(200);
            } else {
                return h
                    .response(
                        Response.build(
                            false,
                            "Unable to find client by that id"
                        )
                    )
                    .code(404);
            }
        }
    }
];

module.exports = {
    routes: function() {
        return routes;
    }
};

const Client = require("../models/client");
const Appointment = require("../models/appointment");
const Transaction = require("../models/transaction");
const Session = require("../modules/session");
const DateTime = require("date-and-time");
const Response = require("../modules/Response");

let routes = [
    {
        method: "GET",
        path: "/analytics/dashboard",
        options: {
            auth: "jwt"
        },
        handler: async (request, h) => {
            /**
             * @todo Add the ablity to use a query param as a start date for the data.
             */
            let month = DateTime.format(new Date(), "YYYY-MM-01");
            let monthEnd = DateTime.format(new Date(), "YYYY-MM-31");
            let accountId = await Session.getUserAccountId(request.auth.token);
            let data = {
                clients: await Client.where("account_id", accountId)
                    .where("created_at", ">", month)
                    .where("created_at", "<", monthEnd)
                    .count(),
                appointments: await Appointment.where("account_id", accountId)
                    .where("date", ">", month)
                    .where("date", "<", monthEnd)
                    .count(),
                transactions: await Transaction.where("account_id", accountId)
                    .where("date", ">", month)
                    .where("date", "<", monthEnd)
                    .count(),
                income: await Transaction.getMonthlyIncome(
                    accountId,
                    month,
                    monthEnd
                )
            };
            return h
                .response(
                    Response.build(true, "Fetched dashboard data", {
                        analytics: data
                    })
                )
                .code(200);
        }
    }
];

module.exports = {
    routes: function() {
        return routes;
    }
};

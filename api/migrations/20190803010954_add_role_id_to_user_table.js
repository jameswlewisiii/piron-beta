exports.up = function(knex, Promise) {
    return knex.schema.table('users', function(t) {
        t.integer('role_id').notNull();
    });
};

exports.down = function(knex, Promise) {
    return knex.schema.table('users', function(t) {
        t.dropColumn('role_id');
    });
};
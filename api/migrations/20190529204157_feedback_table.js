
exports.up = function(knex, Promise) {
    return knex.schema
    .createTable('feedback', function(t) {
        t.increments('id').primary();
        t.string('first_name');
        t.string('last_name');
        t.string('email').notNull();
        t.text('message').notNull();
        t.timestamp('created_at').defaultTo(knex.fn.now())
    })
    ;
};

exports.down = function(knex, Promise) {
    return knex.schema.dropTable('feedback');
};


exports.up = function(knex, Promise) {
    return knex.schema
    .createTable('user_account_invites', function(t) {
        t.increments('id').primary();
        t.integer('account_id').notNull()
        t.integer('sender_id').notNull()
        t.string('email').notNull();
        t.timestamp('created_at').defaultTo(knex.fn.now())
    })
    ;
};

exports.down = function(knex, Promise) {
    return knex.schema.dropTable('user_account_invites');
};

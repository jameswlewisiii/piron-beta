
exports.up = function(knex, Promise) {
    return knex.schema
        .createTable('users', function(t) {
            t.increments('id').primary();
            t.integer('account_id').notNull();
            t.string('first_name');
            t.string('last_name');
            t.string('email').unique().notNull();
            t.string('password').notNull();
            t.timestamp('created_at').defaultTo(knex.fn.now())
        })
        .createTable('plans', function(t) {
            t.increments('id').primary();
            t.string('name').notNull();
            t.timestamp('created_at').defaultTo(knex.fn.now())
        })
        .createTable('accounts', function(t) {
            t.increments('id').primary();
            t.string('name');
            t.string('address1');
            t.string('address2');
            t.string('city');
            t.string('state');
            t.string('zip');
            t.string('phone');
            t.string('website');
            t.string('facebook');
            t.integer('plan_id').notNull();
            t.timestamp('created_at').defaultTo(knex.fn.now())
        })
        .createTable('clients', function(t) {
            t.increments('id');
            t.integer('account_id').notNull();
            t.string('first_name');
            t.string('last_name');
            t.string('email');
            t.string('phone');
            t.string('address1');
            t.string('address2');
            t.string('city');
            t.string('state');
            t.string('zip');
            t.string('dob');
            t.timestamp('created_at').defaultTo(knex.fn.now())
        })
        .createTable('client_transactions', function(t) {
            t.increments('id');
            t.integer('client_id').notNull();
            t.integer('account_id').notNull();
            t.integer('user_id');
            t.string('description');
            t.string('notes');
            t.decimal('amount', 8, 2)
            t.string('date');
            t.string('start_time');
            t.string('end_time');
            t.timestamp('created_at').defaultTo(knex.fn.now())
        })
        .createTable('appointments', function(t) {
            t.increments('id');
            t.integer('user_id');
            t.integer('client_id').notNull();
            t.integer('account_id').notNull();
            t.string('date');
            t.string('start_time');
            t.string('end_time');
            t.string('notes');
            t.timestamp('created_at').defaultTo(knex.fn.now())
        })
    ;
};

exports.down = function(knex, Promise) {
    return knex.schema
        .dropTable('users')
        .dropTable('plans')
        .dropTable('accounts')
        .dropTable('clients')
        .dropTable('client_transactions')
        .dropTable('appointments')
    ;
};

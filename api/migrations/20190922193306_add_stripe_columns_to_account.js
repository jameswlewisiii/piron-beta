exports.up = function(knex, Promise) {
    return knex.schema.table("accounts", function(t) {
        t.string("customer_id");
        t.string("subscription_id");
        t.boolean("active");
    });
};

exports.down = function(knex, Promise) {
    return knex.schema.table("accounts", function(t) {
        t.dropColumn("customer_id");
        t.dropColumn("subscription_id");
        t.dropColumn("active");
    });
};

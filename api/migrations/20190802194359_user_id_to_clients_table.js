exports.up = function(knex, Promise) {
    return knex.schema.table('clients', function(t) {
        t.integer('user_id').notNull();
    });
};

exports.down = function(knex, Promise) {
    return knex.schema.table('clients', function(t) {
        t.dropColumn('user_id');
    });
};
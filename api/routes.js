
const client = require('./routes/clients');
const transaction = require('./routes/transactions');
const appointment = require('./routes/appointments');
const accounts = require('./routes/accounts');
const analytics = require('./routes/analytics');
const auth = require('./routes/auth');
const users = require('./routes/users');
const feedback = require('./routes/feedback');
const redis = require('./config/redis');

var routes = [];

routes = routes.concat(auth.routes());
routes = routes.concat(client.routes());
routes = routes.concat(appointment.routes());
routes = routes.concat(transaction.routes());
routes = routes.concat(accounts.routes());
routes = routes.concat(analytics.routes());
routes = routes.concat(users.routes());
routes = routes.concat(feedback.routes());

// Export routes.
module.exports = { routes: function() { return routes } }
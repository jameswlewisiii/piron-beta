const crypto = require("crypto");
const bcrypt = require("bcrypt");
let Bookshelf = require("../config/database");
require("./account");
require("./client");
require("./transaction");
require("./appointment");

/**
 * User Model
 *
 * @property int id
 * @property string email
 * @property string password
 * @property timestamp created_at
 * @property int account_id
 * @property int role_id
 * @property int first_name
 * @property int last_name
 */

const ROLE_ADMIN = 1;
const ROLE_USER = 2;

let User = Bookshelf.Model.extend(
    {
        tableName: "users",
        hasTimestamps: true,
        hasTimestamps: ["created_at"],
        hidden: ["password"],

        clients() {
            return this.hasMany("Client", "user_id", "id");
        },
        appointments() {
            return this.hasMany("Appointment");
        },
        transactions() {
            return this.hasMany("Transaction");
        },
        account() {
            return this.belongsTo("Account");
        },
        isAdmin() {
            return this.get("role_id") == ROLE_ADMIN;
        },
        isUser() {
            return this.get("role_id") == ROLE_USER;
        },
        changeAccount(accountId) {
            let saveConfig = {
                method: "update",
                patch: true,
                require: false
            };
            Bookshelf.model("Client")
                .where({ user_id: this.id })
                .save({ account_id: accountId }, saveConfig);
            Bookshelf.model("Transaction")
                .where({ user_id: this.id })
                .save({ account_id: accountId }, saveConfig);
            Bookshelf.model("Appointment")
                .where({ user_id: this.id })
                .save({ account_id: accountId }, saveConfig);
            return this.save({ account_id: accountId }, { patch: true });
        },
        setRoleAdmin() {
            this.set({ role_id: ROLE_ADMIN });
        },
        setRoleUser() {
            this.set({ role_id: ROLE_USER });
        },
        setRole(id) {
            this.set({ role_id: id });
        }
    },
    {
        getUserRoleId() {
            return ROLE_USER;
        },
        getAdminRoleId() {
            return ROLE_ADMIN;
        },
        generateTemporaryPassword() {
            return crypto.randomBytes(20).toString("hex");
        },
        bcryptPassword(password) {
            return bcrypt.hash(password, 10);
        }
    }
);

module.exports = Bookshelf.model("User", User);

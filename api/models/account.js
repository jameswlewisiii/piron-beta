let Bookshelf = require("../config/database");
require("./user");

/**
 * Account Model
 *
 * @property int id
 * @property string name
 * @property string address1
 * @property string address2
 * @property string city
 * @property string state
 * @property string zip
 * @property string website
 * @property string facebook
 * @property string phone
 * @property int plan_id
 * @property string customer_id
 * @property string subscription_id
 * @property boolean active
 */

const PRO_PLAN_ID = 1;
// Setting freemium to 1 to give it access to the complete application
// Doing this because I am moving away from the paid version to a completely free app
// In order to get more users
const FREEMIUM_PLAN_ID = 1;

let Account = Bookshelf.Model.extend(
    {
        tableName: "accounts",
        hasTimestamps: true,
        hasTimestamps: ["created_at"],
        hidden: ["customer_id", "subscription_id"],

        users() {
            return this.hasMany("User");
        }
    },
    {
        getProPlanId() {
            return PRO_PLAN_ID;
        },
        getFreemiumPlanId() {
            return FREEMIUM_PLAN_ID;
        }
    }
);

module.exports = Bookshelf.model("Account", Account);

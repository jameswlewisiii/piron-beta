let Bookshelf = require("../config/database");
require("./client");
require("./user");

/**
 * Transaction Model
 *
 * @property int client_id
 * @property int user_id
 * @property string description
 * @property string notes
 * @property string amount
 * @property datetime date
 * @property string time
 * @property string start_time
 * @property string end_time
 * @property timestamp created_at
 * @property int account_id
 */
let Transaction = Bookshelf.Model.extend(
    {
        tableName: "client_transactions",
        hasTimestamps: true,
        hasTimestamps: ["created_at"],

        client() {
            return this.belongsTo("Client");
        },
        user() {
            return this.belongsTo("User");
        }
    },
    {
        getMonthlyIncome: async function(accountId, month, monthEnd) {
            let amount = 0;
            let transactions = await Transaction.where("account_id", accountId)
                .where("date", ">", month)
                .where("date", "<", monthEnd)
                .fetchAll();
            transactions.serialize().forEach((transaction, i) => {
                amount += parseFloat(transaction.amount);
            });
            return amount.toFixed(2);
        }
    }
);

module.exports = Bookshelf.model("Transaction", Transaction);

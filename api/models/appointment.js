let Bookshelf = require("../config/database");
require("./client");
require("./user");

/**
 * Appointment Model
 *
 * @property int user_id
 * @property int client_id
 * @property date date
 * @property timestamp created_at
 * @property int account_id
 * @property string notes
 * @property string start_time
 * @property string end_time
 */
let Appointment = Bookshelf.Model.extend({
    tableName: "appointments",
    hasTimestamps: true,
    hasTimestamps: ["created_at"],

    client() {
        return this.belongsTo("Client");
    },
    user() {
        return this.belongsTo("User");
    }
});

module.exports = Bookshelf.model("Appointment", Appointment);

let Bookshelf = require('../config/database');

/**
 * Feedback Model
 * 
 * @property string first_name
 * @property string last_name
 * @property string email
 * @property string message
 * @property timestamp created_at
 */
let Feedback = Bookshelf.Model.extend({
    tableName: 'feedback',
    hasTimestamps: true,
    hasTimestamps: ['created_at'],
});

module.exports = Bookshelf.model('Feedback', Feedback);
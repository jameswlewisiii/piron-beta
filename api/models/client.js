let Bookshelf = require('../config/database');
require('./appointment');
require('./transaction');
require('./user');

/**
 * Client Model
 * 
 * @property string first_name
 * @property string last_name
 * @property string email
 * @property string phone
 * @property string address1
 * @property string address2
 * @property string city
 * @property string state
 * @property string zip
 * @property string dob
 * @property timestamp created_at
 * @property int account_id
 * @property int user_id
 */
let Client = Bookshelf.Model.extend({
    tableName: 'clients',
    hasTimestamps: true,
    hasTimestamps: ['created_at'],

    appointments() {
        return this.hasMany('Appointment');
    },
    transactions() {
        return this.hasMany('Transaction');
    },
    user() {
        return this.belongsTo('User');
    }
});

module.exports = Bookshelf.model('Client', Client);
const Faker = require("faker");
const Account = require("../models/account");
const User = require("../models/user");
const DateTime = require("date-and-time");

const accountTotal = 1;
const userTotal = 1;
const clientTotal = 50;
const appointmentTotal = 50;
const transactionTotal = 50;

exports.seed = function(knex) {
    return (
        knex("users")
            .del()
            .then(async () => {
                let users = [];
                for (let i = 1; i < userTotal + 1; i++) {
                    users.push({
                        id: userTotal,
                        role_id: User.getAdminRoleId(),
                        email: Faker.internet.email(),
                        password: await User.bcryptPassword("password"),
                        account_id: Math.ceil(Math.random() * accountTotal),
                        first_name: Faker.name.firstName(),
                        last_name: Faker.name.lastName()
                    });
                }
                return knex("users").insert(users);
            })
            /**
             * Accounts
             */
            .then(() => {
                return knex("accounts").del();
            })
            .then(() => {
                let accounts = [];
                for (let i = 1; i < accountTotal + 1; i++) {
                    accounts.push({
                        id: i,
                        name: "Test account #" + i,
                        address1: Faker.address.streetAddress(),
                        address2: Faker.address.streetAddress(),
                        city: Faker.address.city(),
                        state: Faker.address.state(),
                        zip: Faker.address.zipCode(),
                        website: Faker.internet.domainName(),
                        phone: Faker.phone.phoneNumber(),
                        plan_id: Account.getProPlanId(),
                        customer_id: Faker.random.uuid(),
                        subscription_id: Faker.random.uuid(),
                        active: 1
                    });
                }
                return knex("accounts").insert(accounts);
            })
            /**
             * Clients
             */
            .then(() => {
                return knex("clients").del();
            })
            .then(() => {
                let clients = [];
                for (let i = 1; i < clientTotal + 1; i++) {
                    clients.push({
                        id: i,
                        first_name: Faker.name.firstName(),
                        last_name: Faker.name.lastName(),
                        email: Faker.internet.email(),
                        phone: Faker.phone.phoneNumber(),
                        address1: Faker.address.streetAddress(),
                        address2: Faker.address.streetAddress(),
                        city: Faker.address.city(),
                        state: Faker.address.state(),
                        zip: Faker.address.zipCode(),
                        dob: Faker.date.past(),
                        account_id: Math.ceil(Math.random() * accountTotal),
                        user_id: Math.ceil(Math.random() * userTotal)
                    });
                }
                return knex("clients").insert(clients);
            })
            /**
             * Transactions
             */
            .then(() => {
                return knex("client_transactions").del();
            })
            .then(() => {
                let transactions = [];
                for (let i = 1; i < transactionTotal + 1; i++) {
                    transactions.push({
                        id: i,
                        client_id: Math.ceil(Math.random() * clientTotal),
                        user_id: Math.ceil(Math.random() * userTotal),
                        description: Faker.random.words(),
                        notes: Faker.random.words(),
                        amount: Faker.finance.amount(),
                        date: DateTime.format(
                            new Date(Faker.date.past()),
                            "YYYY-MM-DD"
                        ),
                        start_time: Math.floor(Math.random() * 12) + ":00",
                        end_time: Math.floor(Math.random() * 12) + ":00",
                        account_id: Math.ceil(Math.random() * accountTotal)
                    });
                }
                return knex("client_transactions").insert(transactions);
            })
            /**
             * Appointments
             */
            .then(() => {
                return knex("appointments").del();
            })
            .then(() => {
                let appointments = [];
                for (let i = 1; i < appointmentTotal + 1; i++) {
                    appointments.push({
                        id: i,
                        client_id: Math.ceil(Math.random() * clientTotal),
                        user_id: Math.ceil(Math.random() * userTotal),
                        date: DateTime.format(
                            new Date(Faker.date.future()),
                            "YYYY-MM-DD"
                        ),
                        account_id: Math.ceil(Math.random() * accountTotal),
                        notes: Faker.random.words(),
                        start_time: Math.floor(Math.random() * 12) + ":00",
                        end_time: Math.floor(Math.random() * 12) + ":00"
                    });
                }
                return knex("appointments").insert(appointments);
            })
    );
};

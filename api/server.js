"use strict";

const Hapi = require("hapi");
require("dotenv").load();
const session = require("./modules/session");
const hapiAuthJWT = require("hapi-auth-jwt2");
const router = require("./routes.js");
const server = Hapi.server({
    port: process.env.HAPI_PORT,
    host: process.env.HAPI_HOST,
    routes: {
        cors: {
            origin: [process.env.CORS]
        }
    }
});

const validate = async function(decoded, request, h) {
    let userSession = await session.getUserSession(decoded.sessionId);
    if (userSession.userId > 0) {
        return { isValid: true };
    } else {
        return { isValid: false };
    }
};

const init = async () => {
    await server.register(hapiAuthJWT);
    // see: http://hapijs.com/api#serverauthschemename-scheme
    server.auth.strategy("jwt", "jwt", {
        key: process.env.JWT_SECERT, // Never Share your secret key
        validate: validate, // validate function defined above
        verifyOptions: {
            ignoreExpiration: true, // do reject expired tokens
            algorithms: [process.env.JWT_ALGORITHM] // specify your secure algorithm
        }
    });

    // This causes all routes to require jwt auth.
    // server.auth.default('jwt');

    server.route(router.routes());

    await server.start();
    console.log(
        "Server running at " + server.info.host + ":" + server.info.port
    );
};

process.on("unhandledRejection", err => {
    console.log(err);
    process.exit(1);
});

init();

'use strict';

require('dotenv').load();
process.env.NODE_ENV = 'test';
const Hapi = require('hapi');
const router = require('../routes.js');
const session = require('../modules/session');
const hapiAuthJWT = require('hapi-auth-jwt2')
const server = Hapi.server({
    port: 3000,
    host: '0.0.0.0',
})

const validate = async function (decoded, request, h) {
    let userSession = await session.getUserSession(decoded.sessionId)
    if (userSession.userId > 0) {
      return { isValid: true };
    }
    else {
      return { isValid : false };
    }
  };

server.register(hapiAuthJWT);
// see: http://hapijs.com/api#serverauthschemename-scheme
server.auth.strategy('jwt', 'jwt', {
    key: process.env.JWT_SECERT, // Never Share your secret key
    validate: validate,      // validate function defined above
    verifyOptions: {
        ignoreExpiration: true,    // do reject expired tokens
        algorithms: [ process.env.JWT_ALGORITHM ]    // specify your secure algorithm
    }
})
server.route(router.routes());
  
exports.init = async () => {

    await server.initialize();
    return server;
};

exports.start = async () => {

    await server.start();
    console.log(`Server running at: ${server.info.uri}`);
    return server;
};

process.on('unhandledRejection', (err) => {

    console.log(err);
    process.exit(1);
});

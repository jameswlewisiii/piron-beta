require('dotenv').load();

let user = process.env.DATABASE_USER;
let password = process.env.DATABASE_PASSWORD;
let name = process.env.DATABASE_NAME;

if(process.env.NODE_ENV == 'test') {
    user = process.env.TEST_DATABASE_USER;
    password = process.env.TEST_DATABASE_PASSWORD;
    name = process.env.TEST_DATABASE_NAME;
}

module.exports = {
    migrations: { tableName: 'knex_migrations' },
        seeds: { tableName: './seeds' },
        client: process.env.DATABASE_CLIENT,
        connection: {
            host: process.env.DATABASE_HOST,
            user: user,
            password: password,
            database: name,
            charset: 'utf8',
        }
};
module.exports = {
    newUser() {
        return {
            method: "post",
            url: "/register/freemium",
            payload: {
                email: "jameswlewisiii@gmail.com",
                password: "testme",
                first_name: "James",
                last_name: "Lewis"
            }
        };
    },
    newProUser() {
        return {
            method: "post",
            url: "/register/pro",
            payload: {
                email: "jameswlewisiii@gmail.com",
                password: "testme",
                first_name: "James",
                last_name: "Lewis"
            }
        };
    },
    updateRole(token, userId, roleId) {
        return {
            method: "put",
            url: "/user/role/" + userId,
            headers: { Authorization: token },
            payload: { role_id: roleId }
        };
    },
    newUserInvite(token, name, email) {
        return {
            method: "post",
            url: "/user/account/invite",
            headers: { Authorization: token },
            payload: {
                name: name,
                email: email
            }
        };
    },
    joinAccount(token, id) {
        return {
            method: "put",
            url: "/user/account/invite/" + id,
            headers: { Authorization: token }
        };
    },
    newAccount(token) {
        return {
            method: "post",
            url: "/account",
            headers: { Authorization: token },
            payload: {
                name: "Test Account",
                address1: "Test lane",
                address2: "938 Port",
                city: "Jacksonville",
                state: "Florida",
                zip: "32156",
                website: "piron.io",
                facebook: "/testme",
                phone: "904-544-7777",
                plan_id: 1
            }
        };
    },
    newClient(token) {
        return {
            method: "post",
            url: "/client",
            headers: { Authorization: token },
            payload: {
                first_name: "Test",
                last_name: "Client",
                email: "test@test.com",
                address1: "Test lane",
                address2: "938 Port",
                city: "Jacksonville",
                state: "Florida",
                zip: "32156",
                phone: "904-544-7777",
                dob: "1991-02-12"
            }
        };
    },
    getClientsByAccount(token) {
        return {
            method: "get",
            url: "/clients/account",
            headers: { Authorization: token }
        };
    },
    getClientsByUser(token) {
        return {
            method: "get",
            url: "/clients/user",
            headers: { Authorization: token }
        };
    },
    getClient(token, id) {
        return {
            method: "get",
            url: "/client/" + id,
            headers: { Authorization: token }
        };
    },
    deleteClient(token, id) {
        return {
            method: "delete",
            url: "/client/" + id,
            headers: { Authorization: token }
        };
    },
    newTransaction(token) {
        return {
            method: "post",
            url: "/transaction",
            headers: { Authorization: token },
            payload: {
                client_id: 1,
                description: "Testing this route",
                notes: "Test",
                amount: "9.99",
                date: "06/11/2019",
                start_time: "8:45 AM",
                end_time: "10:00 AM"
            }
        };
    },
    getTransaction(token, id) {
        return {
            method: "get",
            url: "/transaction/" + id,
            headers: { Authorization: token }
        };
    },

    newAppointment(token, data) {
        let payload = {
            date: "2019-06-13",
            start_time: "10:00AM",
            end_time: "11:45AM",
            client_id: 1,
            notes: "This is just a test."
        };

        if (data) payload = Object.assign(payload, data);

        return {
            method: "post",
            url: "/appointment",
            headers: { Authorization: token },
            payload: payload
        };
    },

    getAppointment(token, id) {
        return {
            method: "get",
            url: "/appointment/" + id,
            headers: { Authorization: token }
        };
    },

    deleteAppointment(token, id) {
        return {
            method: "delete",
            url: "/appointment/" + id,
            headers: { Authorization: token }
        };
    }
};

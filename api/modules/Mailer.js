const mailgun = require("mailgun-js")({
    apiKey: process.env.MAIL_GUN_PRIVATE_KEY,
    domain: process.env.MAIL_GUN_DOMAIN
});
const Logger = require("../config/log");

module.exports = {
    sendMessage(from, to, subject, text) {
        let data = {
            from: from,
            to: to,
            subject: subject,
            text: text
        };

        if (process.env.NODE_ENV == "test" || process.env.NODE_ENV == "dev") {
            console.log("Test Mode: Email is disabled.");
            return;
        }

        mailgun.messages().send(data, function(error, body) {
            Logger.log("info", "Email sent.", { time: Date(), body: body });
            if (error) {
                Logger.log("error", "Unable to send email.", {
                    error: error,
                    time: Date(),
                    emailInfo: data
                });
            }
        });
    },
    sendUserRegisterMessage(to) {
        let data = {
            from: "info@piron.io",
            to: to,
            subject: "Welcome to Piron.io",
            template: "register"
        };

        if (process.env.NODE_ENV == "test" || process.env.NODE_ENV == "dev") {
            console.log("Test Mode: Email is disabled.");
            return;
        }

        mailgun.messages().send(data, function(error, body) {
            Logger.log("info", "Email sent.", { time: Date(), body: body });
            if (error) {
                Logger.log("error", "Unable to send email.", {
                    error: error,
                    time: Date(),
                    emailInfo: data
                });
            }
        });
    },
    sendNewUserMessage(to, password) {
        let data = {
            from: "info@piron.io",
            to: to,
            subject: "Welcome to Piron.io",
            template: "new-user",
            "h:X-Mailgun-Variables":
                '{"email": "' + to + 'l","password": "' + password + '"}'
        };

        if (process.env.NODE_ENV == "test" || process.env.NODE_ENV == "dev") {
            console.log("Test Mode: Email is disabled.");
            return;
        }

        mailgun.messages().send(data, function(error, body) {
            Logger.log("info", "Email sent.", { time: Date(), body: body });
            if (error) {
                Logger.log("error", "Unable to send email.", {
                    error: error,
                    time: Date(),
                    emailInfo: data
                });
            }
        });
    }
};

const Stripe = require("stripe")(process.env.STRIPE_API);
const Logger = require("../config/log");

const PRO_PLAN_ID = "plan_GU9ckzsHUWKrd0";

module.exports = {
    deleteCustomer(customerId) {
        return new Promise((resolve, reject) => {
            Logger.info(
                "Stripe::Attempting to delete a customer. Customer id: " +
                    customerId
            );
            Stripe.customers.del(customerId, (err, confirmation) => {
                if (err) {
                    Logger.error("Stripe::Deleting a customer. " + err);
                }
                Logger.info(
                    "Stripe::Deleted a customer. Customer id: " + customerId
                );
                resolve(confirmation);
            });
        });
    },
    updateCustomerSource(customerId, token) {
        return new Promise((resolve, reject) => {
            Logger.info(
                "Stripe::Attempting to update a customer's card info. Customer id: " +
                    customerId
            );

            Stripe.customers.update(
                customerId,
                {
                    source: token.id
                },
                function(err, customer) {
                    if (err) {
                        Logger.error(
                            "Stripe::Updated a customer's card. Error: " + err
                        );

                        if (err.code == "card_declined") {
                            reject("Card Declined");
                        }

                        reject(err);
                    } else {
                        Logger.info(
                            "Stripe::Updated a customer's card." +
                                " customer_id: " +
                                customer.id
                        );
                        resolve(customer);
                    }
                }
            );
        });
    },
    getCustomer(customerId) {
        return new Promise((resolve, reject) => {
            Stripe.customers.retrieve(customerId, (err, customer) => {
                if (err) {
                    Logger.error("Stripe::Fetching a customer. " + err);
                }
                resolve(customer);
            });
        });
    },
    deleteCustomer(customerId) {
        return new Promise((resolve, reject) => {
            Logger.info(
                "Stripe::Attempting to delete a customer. Customer id: " +
                    customerId
            );

            Stripe.customers.del(customerId, (err, confirmation) => {
                if (confirmation.deleted) {
                    Logger.info(
                        "Stripe::Customer deleted. Customer id: " + customerId
                    );
                    resolve(confirmation);
                } else {
                    Logger.info(
                        "Stripe::Failed to deleted. Customer id: " + customerId
                    );
                    reject(err);
                }
            });
        });
    },
    createCustomer(email, token) {
        let data = {
            email: email
        };

        if (token) {
            data.source = token.id;
        }

        return new Promise((resolve, reject) => {
            Logger.info(
                "Stripe::Attepmting to create a new customer. Email:" + email
            );
            Stripe.customers.create(data, (err, customer) => {
                if (err) {
                    Logger.error(err);

                    if (err.code == "card_declined") {
                        reject("Card Declined");
                    }

                    reject(err);
                } else {
                    Logger.info(
                        "Stripe::Created a new customer. Email:" +
                            email +
                            " customer_id: " +
                            customer.id
                    );
                    resolve(customer);
                }
            });
        });
    },
    createProPlanSubscription(customer) {
        return new Promise((resolve, reject) => {
            Logger.info(
                "Stripe::Attepmting to create a new pro subscription plan. customer_id:" +
                    customer.id
            );
            Stripe.subscriptions.create(
                {
                    customer: customer.id,
                    items: [
                        {
                            plan: PRO_PLAN_ID
                        }
                    ],
                    trial_period_days: 30
                },
                (err, subscription) => {
                    if (err) {
                        Logger.error(err);
                        reject(err);
                    } else {
                        Logger.info(
                            "Stripe::Created a new pro subscription plan. customer_id:" +
                                customer.id +
                                " subscription_id: " +
                                subscription.id
                        );
                        resolve(subscription);
                    }
                }
            );
        });
    },
    createTestToken() {
        return new Promise((resolve, reject) => {
            Stripe.tokens.create(
                {
                    card: {
                        number: "4242424242424242",
                        exp_month: 12,
                        exp_year: 2021,
                        cvc: "123"
                    }
                },
                function(err, token) {
                    resolve(token);
                }
            );
        });
    }
};

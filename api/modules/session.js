const crypto = require("crypto");
const redis = require("../config/redis");
const Jwt = require("jsonwebtoken");

function generateSessionKey() {
    let sha = crypto.createHash("sha256");
    sha.update(Math.random().toString());
    return sha.digest("hex");
}

function setUserSession(user) {
    let key = generateSessionKey();
    redis.hmset(key, {
        user_id: user.get("id"),
        account_id: user.get("account_id")
    });
    return key;
}

function updateUserSession(jwt, userId, accountId) {
    let key = Jwt.decode(jwt).sessionId;
    redis.hmset(key, {
        user_id: userId,
        account_id: accountId
    });
    return key;
}

function getUserSession(sessionId) {
    return new Promise((resolve, reject) => {
        redis.hmget(sessionId, ["user_id", "account_id"], (err, data) => {
            resolve({
                userId: data[0],
                accountId: data[1]
            });
        });
    });
}

async function getUserAccountId(jwt) {
    let sessionId = Jwt.decode(jwt).sessionId;
    let data = await getUserSession(sessionId);
    return data.accountId;
}

async function getUserId(jwt) {
    let sessionId = Jwt.decode(jwt).sessionId;
    let data = await getUserSession(sessionId);
    return data.userId;
}

function deleteUserSession(key) {
    redis.hdel(key, ["user_id", "account_id"]);
}

module.exports.setUserSession = setUserSession;
module.exports.updateUserSession = updateUserSession;
module.exports.getUserSession = getUserSession;
module.exports.deleteUserSession = deleteUserSession;
module.exports.getUserAccountId = getUserAccountId;
module.exports.getUserId = getUserId;

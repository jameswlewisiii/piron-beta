module.exports = {
    build(success, message, data) {
        let res = {
            api_version: process.env.API_VERSION,
            date_response_created: new Date(),
            message: message,
            success: success,
        };

        return Object.assign(res, data);
    }
}
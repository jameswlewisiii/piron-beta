module.exports = require('redis').createClient(
    process.env.REDIS_PORT,
    process.env.REDIS_NAME
);
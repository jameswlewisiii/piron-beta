// database.js
'use strict';

var knex      = require('knex')(require('../knexfile')), 
    bookshelf = require('bookshelf')(knex);

    bookshelf.plugin('registry');
    bookshelf.plugin('visibility');
    
module.exports = bookshelf;
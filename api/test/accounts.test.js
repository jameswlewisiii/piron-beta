"use strict";

const Lab = require("@hapi/lab");
const { expect } = require("@hapi/code");
const { afterEach, beforeEach, describe, it } = (exports.lab = Lab.script());
const { init } = require("../lib/server-test");
const TestHelper = require("../modules/TestHelper");
const User = require("../models/user");
const Account = require("../models/account");

describe("GET /account", () => {
    let server;

    beforeEach(async () => {
        server = await init();
    });

    afterEach(async () => {
        await server.stop();
        await User.where("id", "!=", 0).destroy();
        await Account.where("id", "!=", 0).destroy();
    });

    it("Test fetching an account", async () => {
        let registerRes = await server.inject(TestHelper.newUser());
        let res = await server.inject({
            method: "get",
            url: "/account",
            headers: { Authorization: registerRes.result.token }
        });
        expect(res.statusCode).to.equal(200);
        expect(res.result.success).to.be.true();
    });
});

describe("POST /account", () => {
    let server;

    beforeEach(async () => {
        server = await init();
    });

    afterEach(async () => {
        await server.stop();
        await User.where("id", "!=", 0).destroy();
        await Account.where("id", "!=", 0).destroy();
    });

    it("Test creating an account", async () => {
        let registerRes = await server.inject(TestHelper.newUser());
        let res = await server.inject(
            TestHelper.newAccount(registerRes.result.token)
        );

        expect(res.statusCode).to.equal(201);
        expect(res.result.success).to.be.true();
    });
});

describe("PUT /account", () => {
    let server;

    beforeEach(async () => {
        server = await init();
    });

    afterEach(async () => {
        await server.stop();
        await User.where("id", "!=", 0).destroy();
        await Account.where("id", "!=", 0).destroy();
    });

    it("Test updating an account", async () => {
        let newAccountName = "Update Account";
        let registerRes = await server.inject(TestHelper.newUser());

        let res = await server.inject({
            method: "put",
            url: "/account",
            headers: { Authorization: registerRes.result.token },
            payload: { name: newAccountName }
        });

        expect(res.statusCode).to.equal(200);
        expect(res.result.success).to.be.true();
        expect(res.result.account.name).to.be.equal(newAccountName);
    });
});

describe("DELETE /account", () => {
    let server;

    beforeEach(async () => {
        server = await init();
    });

    afterEach(async () => {
        await server.stop();
        await User.where("id", "!=", 0).destroy();
        await Account.where("id", "!=", 0).destroy();
    });

    it("Test deleting a freemium account", async () => {
        let newAccountName = "Delete Account";
        let registerRes = await server.inject(TestHelper.newUser());

        let res = await server.inject({
            method: "delete",
            url: "/account",
            headers: { Authorization: registerRes.result.token },
            payload: { name: newAccountName }
        });

        expect(res.statusCode).to.equal(200);
        expect(res.result.success).to.be.true();
    });

    it("Test deleting a pro account", { timeout: 10000 }, async () => {
        let newAccountName = "Delete Account";
        let registerRes = await server.inject({
            method: "post",
            url: "/register/pro",
            payload: {
                email: "jameswlewisiii@gmail.com",
                password: "testme",
                first_name: "James",
                last_name: "Lewis",
                address: "Test lane",
                state: "Florida",
                zip: "31245",
                city: "Maimi",
                stripe_token: { id: "tok_visa" }
            }
        });

        let res = await server.inject({
            method: "delete",
            url: "/account",
            headers: { Authorization: registerRes.result.token }
        });

        expect(res.statusCode).to.equal(200);
        expect(res.result.success).to.be.true();
    });
});

describe("PUT /account/card", () => {
    let server;

    beforeEach(async () => {
        server = await init();
    });

    afterEach(async () => {
        await server.stop();
        await User.where("id", "!=", 0).destroy();
        await Account.where("id", "!=", 0).destroy();
    });

    it("Test updating card", { timeout: 10000 }, async () => {
        let registerRes = await server.inject({
            method: "post",
            url: "/register/pro",
            payload: {
                email: "jameswlewisiii@gmail.com",
                password: "testme",
                first_name: "James",
                last_name: "Lewis",
                address: "Test lane",
                state: "Florida",
                zip: "31245",
                city: "Maimi",
                stripe_token: { id: "tok_visa" }
            }
        });

        let res = await server.inject({
            method: "put",
            url: "/account/card",
            headers: { Authorization: registerRes.result.token },
            payload: { stripe_token: { id: "tok_mastercard" } }
        });

        expect(res.statusCode).to.equal(200);
        expect(res.result.success).to.be.true();
    });
});

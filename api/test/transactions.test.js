'use strict';

const Lab = require('@hapi/lab');
const { expect } = require('@hapi/code');
const { afterEach, beforeEach, describe, it } = exports.lab = Lab.script();
const { init } = require('../lib/server-test');
const TestHelper = require('../modules/TestHelper');
const User = require('../models/user');
const Transaction = require('../models/transaction');
const DateTime = require('date-and-time');

describe('POST /transaction', () => {
    let server;

    beforeEach(async () => {
        server = await init();
    });

    afterEach(async () => {
        await server.stop();
        await User.where('id', '!=', 0).destroy();
    });

    it('Create a new transactions', async () => {
        // Register an account
        let registerRes = await server.inject(TestHelper.newUser());
        let res = await server.inject(TestHelper.newTransaction(registerRes.result.token));
        
        expect(res.statusCode).to.equal(201); 
        expect(res.result.success).to.be.true(); 
    });

});

describe('GET /transaction/acount', () => {
    let server;

    beforeEach(async () => {
        server = await init();
    });

    afterEach(async () => {
        await server.stop();
        await User.where('id', '!=', 0).destroy();
        await Transaction.where('id', '!=', 0).destroy();
    });

    it('Get all transactions for the given account', async () => {
        // Register an account
        let registerRes = await server.inject(TestHelper.newUser());
        await server.inject(TestHelper.newTransaction(registerRes.result.token));
        await server.inject(TestHelper.newTransaction(registerRes.result.token));
        await server.inject(TestHelper.newTransaction(registerRes.result.token));
        let res = await server.inject({
            method: 'get',
            url: '/transaction/account',
            headers: { 'Authorization': registerRes.result.token },
        });

        expect(res.statusCode).to.equal(200); 
        expect(res.result.success).to.be.true(); 
        expect(res.result.transactions.length >= 3).to.be.true(); 
    });

    it('Get all transactions when account has none', async () => {
        // Register an account
        let registerRes = await server.inject(TestHelper.newUser());
        let res = await server.inject({
            method: 'get',
            url: '/transaction/account',
            headers: { 'Authorization': registerRes.result.token },
        });

        expect(res.statusCode).to.equal(200); 
        expect(res.result.success).to.be.true(); 
        expect(res.result.transactions.length == 0).to.be.true(); 
    });

    it('Get all transactions by account id with date limits', async () => {
        // Register an account
        let registerRes = await server.inject(TestHelper.newUser());
        await server.inject(TestHelper.newTransaction(registerRes.result.token));
        await new Promise(done => setTimeout(done, 1500));
        let date = new Date();
        await server.inject(TestHelper.newTransaction(registerRes.result.token));
        await server.inject(TestHelper.newTransaction(registerRes.result.token));
        let res = await server.inject({
            method: 'get',
            url: '/transaction/account?greaterThanDate=' + date,
            headers: { 'Authorization': registerRes.result.token },
        });

        expect(res.statusCode).to.equal(200); 
        expect(res.result.success).to.be.true(); 
        expect(res.result.transactions.length >= 2).to.be.true(); 
    });

});

describe('GET /transaction/user', () => {
    let server;

    beforeEach(async () => {
        server = await init();
    });

    afterEach(async () => {
        await server.stop();
        await User.where('id', '!=', 0).destroy();
        await Transaction.where('id', '!=', 0).destroy();
    });

    it('Get all transactions for the given user id', async () => {
        // Register an account
        let registerRes = await server.inject(TestHelper.newUser());
        await server.inject(TestHelper.newTransaction(registerRes.result.token));
        await server.inject(TestHelper.newTransaction(registerRes.result.token));
        await server.inject(TestHelper.newTransaction(registerRes.result.token));
        let res = await server.inject({
            method: 'get',
            url: '/transaction/user',
            headers: { 'Authorization': registerRes.result.token },
        });

        expect(res.statusCode).to.equal(200); 
        expect(res.result.success).to.be.true(); 
        expect(res.result.transactions.length == 3).to.be.true(); 
    });

});

describe('GET /transaction/:id', () => {
    let server;

    beforeEach(async () => {
        server = await init();
    });

    afterEach(async () => {
        await server.stop();
        await User.where('id', '!=', 0).destroy();
        await Transaction.where('id', '!=', 0).destroy();
    });

    it('Fetch a transaction', async () => {
        // Register an account
        let registerRes = await server.inject(TestHelper.newUser());
        let transaction = await server.inject(TestHelper.newTransaction(registerRes.result.token));
        let res = await server.inject(TestHelper.getTransaction(registerRes.result.token, transaction.result.transaction.id));
        
        expect(res.statusCode).to.equal(200); 
        expect(res.result.success).to.be.true(); 
    });

    it('Fetch a transaction without a valid transaction id', async () => {
        // Register an account
        let registerRes = await server.inject(TestHelper.newUser());
        let transaction = await server.inject(TestHelper.newTransaction(registerRes.result.token));
        let res = await server.inject(TestHelper.getTransaction(registerRes.result.token, transaction.result.transaction.id + 1));
        
        expect(res.statusCode).to.equal(404); 
        expect(res.result.success).to.be.false(); 
    });

});

describe('PUT /transaction/:id', () => {
    let server;

    beforeEach(async () => {
        server = await init();
    });

    afterEach(async () => {
        await server.stop();
        await User.where('id', '!=', 0).destroy();
        await Transaction.where('id', '!=', 0).destroy();
    });

    it('Update a transaction', async () => {
        let updatedNote = 'Updated Test';

         // Register an account
        let registerRes = await server.inject(TestHelper.newUser());
        // Create a transaction
        let transaction = await server.inject(TestHelper.newTransaction(registerRes.result.token));

        let res = await server.inject({
            method: 'put',
            url: '/transaction/' + transaction.result.transaction.id,
            headers: { 'Authorization': registerRes.result.token },
            payload: {
                client_id: 1,
                description: 'Testing this route',
                notes: updatedNote,
                amount: '9.99',
                date: '06/11/2019',
                start_time: '8:45 AM',
                end_time: '10:00 AM',
            }
        });
        
        expect(res.statusCode).to.equal(200); 
        expect(res.result.success).to.be.true();
        let updatedTransaction = await Transaction.where({id: transaction.result.transaction.id}).fetch();
        expect(updatedTransaction.get('notes')).to.be.equal(updatedNote);
    });

    it('Update a transaction without the correct transaction id', async () => {
        let updatedNote = 'Updated Test';

         // Register an account
        let registerRes = await server.inject(TestHelper.newUser());
        // Create a transaction
        let transaction = await server.inject(TestHelper.newTransaction(registerRes.result.token));

        let res = await server.inject({
            method: 'put',
            url: '/transaction/' + transaction.result.transaction.id + 1,
            headers: { 'Authorization': registerRes.result.token },
            payload: {
                client_id: 1,
                description: 'Testing this route',
                notes: updatedNote,
                amount: '9.99',
                date: '06/11/2019',
                start_time: '8:45 AM',
                end_time: '10:00 AM',
            }
        });
        
        expect(res.statusCode).to.equal(404); 
        expect(res.result.success).to.be.false();
    });

});

describe('DELETE /transaction/:id', () => {
    let server;

    beforeEach(async () => {
        server = await init();
    });

    afterEach(async () => {
        await server.stop();
        await User.where('id', '!=', 0).destroy();
    });

    it('Delete a transactions', async () => {

         // Register an account
        let registerRes = await server.inject(TestHelper.newUser());
        // Create a transaction
        let transaction = await server.inject(TestHelper.newTransaction(registerRes.result.token));

        const res = await server.inject({
            method: 'delete',
            url: '/transaction/' + transaction.result.transaction.id,
            headers: { 'Authorization': registerRes.result.token },
        });
        
        expect(res.statusCode).to.equal(200); 
        expect(res.result.success).to.be.true();
        expect(await Transaction.where({id: transaction.result.transaction.id}).fetch()).to.be.null();
    });

    it('Delete a transactions without a valid transaction id', async () => {

        // Register an account
       let registerRes = await server.inject(TestHelper.newUser());
       // Create a transaction
       let transaction = await server.inject(TestHelper.newTransaction(registerRes.result.token));

       const res = await server.inject({
           method: 'delete',
           url: '/transaction/' + transaction.result.transaction.id + 1,
           headers: { 'Authorization': registerRes.result.token },
       });
       
       expect(res.statusCode).to.equal(404); 
       expect(res.result.success).to.be.false();
   });

});
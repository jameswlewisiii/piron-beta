"use strict";

const Lab = require("@hapi/lab");
const { expect } = require("@hapi/code");
const { afterEach, beforeEach, describe, it } = (exports.lab = Lab.script());
const { init } = require("../lib/server-test");
const User = require("../models/user");
const Session = require("../modules/session");
const TestHelper = require("../modules/TestHelper");
const Stripe = require("../modules/Stripe");

describe("POST /password-reset", () => {
    let server;

    beforeEach(async () => {
        server = await init();
    });

    afterEach(async () => {
        await server.stop();
        await User.where("id", "!=", 0).destroy();
    });

    it("Test password reset", async () => {
        // Register an account
        let registerRes = await server.inject(TestHelper.newUser());

        const res = await server.inject({
            method: "post",
            url: "/password-reset",
            payload: { email: "jameswlewisiii@gmail.com" }
        });

        expect(res.statusCode).to.equal(200);
    });
});

describe("GET /logout", () => {
    let server;

    beforeEach(async () => {
        server = await init();
    });

    afterEach(async () => {
        await server.stop();
        await User.where("id", "!=", 0).destroy();
    });

    it("Test logging out a user.", async () => {
        // Register an account
        let registerRes = await server.inject(TestHelper.newUser());
        const res = await server.inject({
            method: "get",
            url: "/logout",
            headers: { Authorization: registerRes.result.token }
        });

        expect(res.statusCode).to.equal(200);
        expect(res.result.success).to.be.true();
        // Verify the user session was destroyed.
        expect(await Session.getUserId(registerRes.result.token)).to.be.null();
    });
});

describe("POST /login", () => {
    let server;

    beforeEach(async () => {
        server = await init();
    });

    afterEach(async () => {
        await server.stop();
        await User.where("id", "!=", 0).destroy();
    });

    it("Test logining a user in without a username and/or password", async () => {
        const res = await server.inject({
            method: "post",
            url: "/login"
        });

        expect(res.statusCode).to.equal(400);
    });

    it("Test user login", async () => {
        // Register an account
        await server.inject(TestHelper.newUser());
        const res = await server.inject({
            method: "post",
            url: "/login",
            payload: { email: "jameswlewisiii@gmail.com", password: "testme" }
        });
        expect(res.statusCode).to.equal(200);
        expect(res.result.token).to.not.be.null();
        expect(res.result.success).to.be.true();
    });

    it("Test user login with invalid credential", async () => {
        const res = await server.inject({
            method: "post",
            url: "/login",
            payload: { email: "fake@email.com", password: "testme" }
        });
        expect(res.statusCode).to.equal(200);
        expect(res.result.token).to.be.undefined();
        expect(res.result.success).to.be.false();
    });
});

describe("POST /register/freemium", () => {
    let server;

    beforeEach(async () => {
        server = await init();
    });

    afterEach(async () => {
        await server.stop();
        await User.where("id", "!=", 0).destroy();
    });

    it("Test registering a user without a password", async () => {
        const res = await server.inject({
            method: "post",
            url: "/register/freemium",
            payload: {
                email: "jameswlewisiii@gmail.com"
            }
        });
        expect(res.statusCode).to.equal(400);
    });

    it("Test registering a user without a password", async () => {
        const res = await server.inject({
            method: "post",
            url: "/register/freemium",
            payload: {
                email: "jameswlewisiii@gmail.com",
                password: "testme"
            }
        });
        expect(res.statusCode).to.equal(400);
    });

    it("Test registering a freemium user", async () => {
        const res = await server.inject(TestHelper.newUser());
        expect(res.statusCode).to.equal(201);
        expect(res.result.token).to.not.be.null();
        expect(res.result.success).to.be.true();
    });

    it("Test registering a user with an email that already exist.", async () => {
        // Register an account
        await server.inject(TestHelper.newUser());
        const res = await server.inject({
            method: "post",
            url: "/register/freemium",
            payload: {
                email: "jameswlewisiii@gmail.com",
                password: "testme",
                first_name: "James",
                last_name: "Lewis"
            }
        });
        expect(res.statusCode).to.equal(200);
        expect(res.result.token).to.be.undefined();
        expect(res.result.success).to.be.false();
    });
});

/**
 * TEST FOR PRO ACCOUNT REGISTER
 *
 */
describe("POST /register/pro", () => {
    let server;

    beforeEach(async () => {
        server = await init();
    });

    afterEach(async () => {
        await server.stop();
        await User.where("id", "!=", 0).destroy();
    });

    it("Test registering a pro user without a stripe token", async () => {
        const res = await server.inject({
            method: "post",
            url: "/register/pro",
            payload: {
                email: "jameswlewisiii@gmail.com",
                password: "testme",
                first_name: "James",
                last_name: "Lewis",
                address: "Test lane",
                state: "Florida",
                zip: "31245",
                city: "Maimi"
            }
        });
        expect(res.statusCode).to.equal(400);
    });

    it("Test registering a pro", { timeout: 10000 }, async () => {
        const res = await server.inject({
            method: "post",
            url: "/register/pro",
            payload: {
                email: "jameswlewisiii@gmail.com",
                password: "testme",
                first_name: "James",
                last_name: "Lewis",
                address: "Test lane",
                state: "Florida",
                zip: "31245",
                city: "Maimi",
                stripe_token: { id: "tok_visa" }
            }
        });
        expect(res.statusCode).to.equal(201);
        expect(res.result.success).to.be.true();
    });

    it(
        "Test registering a pro with invalid token",
        { timeout: 10000 },
        async () => {
            const res = await server.inject({
                method: "post",
                url: "/register/pro",
                payload: {
                    email: "jameswlewisiii@gmail.com",
                    password: "testme",
                    first_name: "James",
                    last_name: "Lewis",
                    address: "Test lane",
                    state: "Florida",
                    zip: "31245",
                    city: "Maimi",
                    stripe_token: { id: "tok_visa_error" }
                }
            });
            expect(res.statusCode).to.equal(200);
            expect(res.result.success).to.be.false();
        }
    );
});

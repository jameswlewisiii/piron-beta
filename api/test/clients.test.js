'use strict';

const Lab = require('@hapi/lab');
const { expect } = require('@hapi/code');
const { afterEach, beforeEach, describe, it } = exports.lab = Lab.script();
const { init } = require('../lib/server-test');
const TestHelper = require('../modules/TestHelper');
const User = require('../models/user');
const Client = require('../models/client');
const DateTime = require('date-and-time');

describe('POST /client', () => {
    let server;

    beforeEach(async () => {
        server = await init();
    });

    afterEach(async () => {
        await server.stop();
        await User.where('id', '!=', 0).destroy();
        await Client.where('id', '!=', 0).destroy();
    });

    it('Test creating a client', async () => {
        let registerRes = await server.inject(TestHelper.newUser());
        let res = await server.inject(TestHelper.newClient(registerRes.result.token));
        

        expect(res.statusCode).to.equal(201); 
        expect(res.result.success).to.be.true();
        let clients = await Client.fetchAll();
        expect(clients.length > 0).to.be.true(); 
    });

});

describe('GET /client/:id', () => {
    let server;

    beforeEach(async () => {
        server = await init();
    });

    afterEach(async () => {
        await server.stop();
        await User.where('id', '!=', 0).destroy();
        await Client.where('id', '!=', 0).destroy();
    });

    it('Test fetching a client', async () => {
        let registerRes = await server.inject(TestHelper.newUser());
        let clientRes = await server.inject(TestHelper.newClient(registerRes.result.token));
        let res = await server.inject(TestHelper.getClient(registerRes.result.token, clientRes.result.client.id))
        

        expect(res.statusCode).to.equal(200); 
        expect(res.result.success).to.be.true();
        expect(res.result.client.email).to.be.equal(clientRes.result.client.email); 
    });

    it('Test fetching a client with an invalid id', async () => {
        let registerRes = await server.inject(TestHelper.newUser());
        let clientRes = await server.inject(TestHelper.newClient(registerRes.result.token));
        let res = await server.inject(TestHelper.getClient(registerRes.result.token, clientRes.result.client.id + 1))
        

        expect(res.statusCode).to.equal(404); 
        expect(res.result.success).to.be.false();
    });

});

describe('GET /clients/account', () => {
    let server;

    beforeEach(async () => {
        server = await init();
    });

    afterEach(async () => {
        await server.stop();
        await User.where('id', '!=', 0).destroy();
        await Client.where('id', '!=', 0).destroy();
    });

    it('Test getting all client by account', async () => {
        let registerRes = await server.inject(TestHelper.newUser());
        await server.inject(TestHelper.newClient(registerRes.result.token));
        await server.inject(TestHelper.newClient(registerRes.result.token));
        await server.inject(TestHelper.newClient(registerRes.result.token));
        let res = await server.inject(TestHelper.getClientsByAccount(registerRes.result.token));

        expect(res.statusCode).to.equal(200); 
        expect(res.result.success).to.be.true();
        expect(res.result.clients.length == 3).to.be.true(); 
    });

    it('Get all transactions by account id with date limits', async () => {
        // Register an account
        let registerRes = await server.inject(TestHelper.newUser());
        await server.inject(TestHelper.newClient(registerRes.result.token));
        await new Promise(done => setTimeout(done, 1500));
        let date = new Date();
        await server.inject(TestHelper.newClient(registerRes.result.token));
        await server.inject(TestHelper.newClient(registerRes.result.token));
        let res = await server.inject({
            method: 'get',
            url: '/clients/account?greaterThanDate=' + date,
            headers: { 'Authorization': registerRes.result.token },
        });

        expect(res.statusCode).to.equal(200); 
        expect(res.result.success).to.be.true(); 
        expect(res.result.clients.length == 2).to.be.true(); 
    });
});

describe('GET /clients/user', () => {
    let server;

    beforeEach(async () => {
        server = await init();
    });

    afterEach(async () => {
        await server.stop();
        await User.where('id', '!=', 0).destroy();
        await Client.where('id', '!=', 0).destroy();
    });

    it('Test getting all client by user id', async () => {
        let registerRes = await server.inject(TestHelper.newUser());
        await server.inject(TestHelper.newClient(registerRes.result.token));
        await server.inject(TestHelper.newClient(registerRes.result.token));
        let cli = await server.inject(TestHelper.newClient(registerRes.result.token));
        let res = await server.inject(TestHelper.getClientsByUser(registerRes.result.token));

        expect(res.statusCode).to.equal(200); 
        expect(res.result.success).to.be.true();
        expect(res.result.clients.length == 3).to.be.true(); 
    });
});

describe('DELETE /client', () => {
    let server;

    beforeEach(async () => {
        server = await init();
    });

    afterEach(async () => {
        await server.stop();
        await User.where('id', '!=', 0).destroy();
        await Client.where('id', '!=', 0).destroy();
    });

    it('Test deleting a client', async () => {
        let registerRes = await server.inject(TestHelper.newUser());
        let clientRes = await server.inject(TestHelper.newClient(registerRes.result.token));
        let res = await server.inject(TestHelper.deleteClient(registerRes.result.token, clientRes.result.client.id))
        

        expect(res.statusCode).to.equal(200); 
        expect(res.result.success).to.be.true();
        let clients = await Client.fetchAll();
        expect(clients.length == 0).to.be.true(); 
    });

    it('Test deleting a client with an invalid id', async () => {
        let registerRes = await server.inject(TestHelper.newUser());
        let clientRes = await server.inject(TestHelper.newClient(registerRes.result.token));
        let res = await server.inject(TestHelper.deleteClient(registerRes.result.token, clientRes.result.client.id + 1))
        

        expect(res.statusCode).to.equal(404); 
        expect(res.result.success).to.be.false();
    });
});

describe('PUT /client', () => {
    let server;

    beforeEach(async () => {
        server = await init();
    });

    afterEach(async () => {
        await server.stop();
        await User.where('id', '!=', 0).destroy();
        await Client.where('id', '!=', 0).destroy();
    });

    it('Test getting all client by account', async () => {
        let registerRes = await server.inject(TestHelper.newUser());
        let clientRes = await server.inject(TestHelper.newClient(registerRes.result.token));
        let res = await server.inject({
            method: 'put',
            url: '/client/' + clientRes.result.client.id,
            headers: { 'Authorization': registerRes.result.token },
            payload: {
                first_name: "James",
                last_name: "Lewis",
                email: 'test@test.com',
                address1: 'Test lane',
                address2: '938 Port',
                city: 'Jacksonville',
                state: 'Florida',
                zip: '32156',
                phone: '904-544-7777',
                dob: '1991-02-12'
            }
        });

        expect(res.statusCode).to.equal(200); 
        expect(res.result.success).to.be.true();
        let client = await Client.where({email:'test@test.com'}).fetch();
        expect(client).to.not.be.empty();
    });
});

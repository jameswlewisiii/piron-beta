"use strict";

const Lab = require("@hapi/lab");
const { expect } = require("@hapi/code");
const { afterEach, beforeEach, describe, it } = (exports.lab = Lab.script());
const { init } = require("../lib/server-test");
const TestHelper = require("../modules/TestHelper");
const User = require("../models/user");
const Account = require("../models/account");

describe("PUT /user/password", () => {
    let server;

    beforeEach(async () => {
        server = await init();
    });

    afterEach(async () => {
        await server.stop();
        await User.where("id", "!=", 0).destroy();
    });

    it("Test updating a user password", async () => {
        let password = "new_test_password";
        let registerRes = await server.inject(TestHelper.newUser());
        let res = await server.inject({
            method: "put",
            url: "/user/password",
            headers: { Authorization: registerRes.result.token },
            payload: {
                current_password: TestHelper.newUser().payload.password,
                new_password: password,
                confirm_new_password: password
            }
        });
        expect(res.statusCode).to.equal(200);
        expect(res.result.success).to.be.true();
    });

    it("Test updating password with wrong new confirm password", async () => {
        let password = "new_test_password";
        let registerRes = await server.inject(TestHelper.newUser());
        let res = await server.inject({
            method: "put",
            url: "/user/password",
            headers: { Authorization: registerRes.result.token },
            payload: {
                current_password: TestHelper.newUser().payload.password,
                new_password: password,
                confirm_new_password: password + "wrong"
            }
        });
        expect(res.statusCode).to.equal(400);
        expect(res.result.success).to.be.false();
    });

    it("Test updating password with wrong current password", async () => {
        let password = "new_test_password";
        let registerRes = await server.inject(TestHelper.newUser());
        let res = await server.inject({
            method: "put",
            url: "/user/password",
            headers: { Authorization: registerRes.result.token },
            payload: {
                current_password: "wrongpassword",
                new_password: password,
                confirm_new_password: password
            }
        });
        expect(res.statusCode).to.equal(400);
        expect(res.result.success).to.be.false();
    });
});

describe("PUT /user", () => {
    let server;

    beforeEach(async () => {
        server = await init();
    });

    afterEach(async () => {
        await server.stop();
        await User.where("id", "!=", 0).destroy();
    });

    it("Test updating a user info", async () => {
        let firstName = "Tester";
        let registerRes = await server.inject(TestHelper.newUser());
        let res = await server.inject({
            method: "put",
            url: "/user",
            headers: { Authorization: registerRes.result.token },
            payload: {
                first_name: firstName
            }
        });
        expect(res.statusCode).to.equal(200);
        expect(res.result.success).to.be.true();
        expect(res.result.user.first_name).to.be.equal(firstName);
    });

    it("Test updating a user info with an email that already exist", async () => {
        let firstName = "Tester";
        let registerRes = await server.inject(TestHelper.newUser());
        let registerRes2 = await server.inject({
            method: "post",
            url: "/register/freemium",
            payload: {
                email: "test@gmail.com",
                password: "testme",
                first_name: "Nick",
                last_name: "test"
            }
        });
        let res = await server.inject({
            method: "put",
            url: "/user",
            headers: { Authorization: registerRes.result.token },
            payload: {
                first_name: firstName,
                last_name: "last",
                email: registerRes2.result.user.get("email")
            }
        });
        expect(res.statusCode).to.equal(400);
        expect(res.result.success).to.be.false();
    });
});

describe("GET /user/account/", () => {
    let server;

    beforeEach(async () => {
        server = await init();
    });

    afterEach(async () => {
        await server.stop();
        await User.where("id", "!=", 0).destroy();
    });

    it("Test getting users by account", async () => {
        let email = "supertest@gmail.com";
        let registerRes = await server.inject(TestHelper.newUser());
        let registerRes2 = await server.inject({
            method: "post",
            url: "/user",
            headers: { Authorization: registerRes.result.token },
            payload: {
                first_name: "Jack",
                last_name: "White",
                email: email
            }
        });

        // Test get users by account route.
        let res = await server.inject({
            method: "get",
            url: "/user/account",
            headers: { Authorization: registerRes.result.token }
        });

        expect(res.statusCode).to.equal(200);
        expect(res.result.success).to.be.true();
        expect(res.result.users.length > 0).to.be.true();
    });
});

describe("DELETE /user/account/:id", () => {
    let server;

    beforeEach(async () => {
        server = await init();
    });

    afterEach(async () => {
        await server.stop();
        await User.where("id", "!=", 0).destroy();
    });

    it("Test deleting users from an account", async () => {
        let email = "supertest@gmail.com";
        let registerRes = await server.inject(TestHelper.newUser());
        let registerRes2 = await server.inject({
            method: "post",
            url: "/user",
            headers: { Authorization: registerRes.result.token },
            payload: {
                first_name: "Jack",
                last_name: "White",
                email: email
            }
        });

        // Test get users by account route.
        let res = await server.inject({
            method: "delete",
            url: "/user/account/" + registerRes2.result.user.id,
            headers: { Authorization: registerRes.result.token }
        });

        expect(res.statusCode).to.equal(200);
        expect(res.result.success).to.be.true();
    });
});

describe("POST /user", () => {
    let server;

    beforeEach(async () => {
        server = await init();
    });

    afterEach(async () => {
        await server.stop();
        await User.where("id", "!=", 0).destroy();
        await Account.where("id", "!=", 0).destroy();
    });

    it("Test creating a new user as an admin with an email already in use", async () => {
        let email = "jameswlewisiii@gmail.com";
        let registerRes = await server.inject(TestHelper.newUser());
        // Make new user an admin
        let user = await User.where({ id: registerRes.result.user.id }).fetch();
        user = await user.save(
            { role_id: User.getAdminRoleId() },
            { patch: true }
        );

        // Create a new user
        let res = await server.inject({
            method: "post",
            url: "/user",
            headers: { Authorization: registerRes.result.token },
            payload: {
                first_name: "Jack",
                last_name: "White",
                email: email
            }
        });

        expect(res.statusCode).to.equal(400);
        expect(res.result.success).to.be.false();
    });

    it("Test creating a new user as an admin", async () => {
        let email = "jwhite@test.com";
        let registerRes = await server.inject(TestHelper.newUser());
        // Make new user an admin
        let user = await User.where({ id: registerRes.result.user.id }).fetch();
        user = await user.save(
            { role_id: User.getAdminRoleId() },
            { patch: true }
        );

        // Create a new user
        let res = await server.inject({
            method: "post",
            url: "/user",
            headers: { Authorization: registerRes.result.token },
            payload: {
                first_name: "Jack",
                last_name: "White",
                email: email
            }
        });

        expect(res.statusCode).to.equal(201);
        expect(res.result.success).to.be.true();

        // Make sure the user exist.
        let newUser = await User.where({ email: email }).fetch();
        expect(newUser.get("account_id")).to.be.equal(user.get("account_id"));
    });

    it("Test creating a new user as non-admin account", async () => {
        let registerRes = await server.inject(TestHelper.newUser());
        let user = await User.where({
            id: registerRes.result.user.get("id")
        }).fetch();
        user.setRoleUser();
        await user.save();
        let res = await server.inject({
            method: "post",
            url: "/user",
            headers: { Authorization: registerRes.result.token },
            payload: {
                first_name: "Jack",
                last_name: "White",
                email: "test@test.com"
            }
        });
        expect(res.statusCode).to.equal(401);
        expect(res.result.success).to.be.false();
    });
});

describe("GET /user/{id}", () => {
    let server;

    beforeEach(async () => {
        server = await init();
    });

    afterEach(async () => {
        await server.stop();
        await User.where("id", "!=", 0).destroy();
        await Account.where("id", "!=", 0).destroy();
    });

    it("Test getting user data", async () => {
        let registerRes = await server.inject(TestHelper.newUser());

        // Get user data
        let res = await server.inject({
            method: "get",
            url: "/user/" + registerRes.result.user.get("id"),
            headers: { Authorization: registerRes.result.token }
        });

        expect(res.statusCode).to.equal(200);
        expect(res.result.success).to.be.true();
    });

    it("Test getting user data for invalid user id", async () => {
        let registerRes = await server.inject(TestHelper.newUser());
        let res = await server.inject({
            method: "get",
            url: "/user/" + registerRes.result.user.get("id") + 1,
            headers: { Authorization: registerRes.result.token }
        });

        expect(res.statusCode).to.equal(404);
        expect(res.result.success).to.be.false();
    });

    it("Test getting user data for invalid account", async () => {
        let email = "test@test.com";
        let registerRes = await server.inject(TestHelper.newUser());
        let registerRes2 = await server.inject({
            method: "post",
            url: "/register/freemium",
            payload: {
                email: "test@gmail.com",
                password: "testme",
                first_name: "Nick",
                last_name: "test"
            }
        });
        let res = await server.inject({
            method: "get",
            url: "/user/" + registerRes2.result.user.id,
            headers: { Authorization: registerRes.result.token }
        });

        expect(res.statusCode).to.equal(401);
        expect(res.result.success).to.be.false();
    });
});

describe("PUT /user/role/{id}", () => {
    let server;

    beforeEach(async () => {
        server = await init();
    });

    afterEach(async () => {
        await server.stop();
        await User.where("id", "!=", 0).destroy();
        await Account.where("id", "!=", 0).destroy();
    });

    it("Test updating user role as non admin", async () => {
        let email = "test@test.com";
        let registerRes = await server.inject(TestHelper.newUser());
        let registerRes2 = await server.inject({
            method: "post",
            url: "/register/freemium",
            payload: {
                email: "test@gmail.com",
                password: "testme",
                first_name: "Nick",
                last_name: "test"
            }
        });

        let res = await server.inject(
            TestHelper.updateRole(
                registerRes.result.token,
                User.getAdminRoleId(),
                registerRes2.result.user.id
            )
        );
        expect(res.statusCode).to.equal(401);
        expect(res.result.success).to.be.false();
    });
    it("Test updating non existing user", async () => {
        let registerRes = await server.inject(TestHelper.newUser());
        let res = await server.inject(
            TestHelper.updateRole(
                registerRes.result.token,
                User.getAdminRoleId(),
                Math.random()
            )
        );
        expect(res.statusCode).to.equal(404);
        expect(res.result.success).to.be.false();
    });
    it("Test updating user role", async () => {});
});

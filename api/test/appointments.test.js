'use strict';

const Lab = require('@hapi/lab');
const { expect } = require('@hapi/code');
const { afterEach, beforeEach, describe, it } = exports.lab = Lab.script();
const { init } = require('../lib/server-test');
const User = require('../models/user');
const Appointment = require('../models/appointment');
const Session = require('../modules/session');
const TestHelper = require('../modules/TestHelper');

// describe('POST /appointment', () => {
//     let server;

//     beforeEach(async () => {
//         server = await init();
//     });

//     afterEach(async () => {
//         await server.stop();
//         await User.where('id', '!=', 0).destroy();
//         await Appointment.where('id', '!=', 0).destroy();
//     });

//     it('Test creating an appointment', async () => {
//          // Register an account
//         let registerRes = await server.inject(TestHelper.newUser());
//         let res = await server.inject(TestHelper.newAppointment(registerRes.result.token, null));
        
//         expect(res.statusCode).to.equal(201); 
//         expect(res.result.success).to.be.true(); 
//     });

// });

// describe('GET /appointment/:id', () => {
//     let server;

//     beforeEach(async () => {
//         server = await init();
//     });

//     afterEach(async () => {
//         await server.stop();
//         await User.where('id', '!=', 0).destroy();
//         await Appointment.where('id', '!=', 0).destroy();
//     });

//     it('Test fetching an appointment by id', async () => {
//          // Register an account
//         let registerRes = await server.inject(TestHelper.newUser());
//         let appointmentRes = await server.inject(TestHelper.newAppointment(registerRes.result.token, null));
//         let res = await server.inject(TestHelper.getAppointment(registerRes.result.token, appointmentRes.result.appointment.id));
        
//         expect(res.statusCode).to.equal(200); 
//         expect(res.result.success).to.be.true(); 
//         expect(res.result.appointment.date).to.be.equal(appointmentRes.result.appointment.date);
//     });

//     it('Test fetching an appointment by an invalid id', async () => {
//         // Register an account
//        let registerRes = await server.inject(TestHelper.newUser());
//        let appointmentRes = await server.inject(TestHelper.newAppointment(registerRes.result.token, null));
//        let res = await server.inject(TestHelper.getAppointment(registerRes.result.token, appointmentRes.result.appointment.id + 1));
       
//        expect(res.statusCode).to.equal(404); 
//        expect(res.result.success).to.be.false(); 
//    });
// });
describe('GET /appointments/user', () => {
    let server;

    beforeEach(async () => {
        server = await init();
    });

    afterEach(async () => {
        await server.stop();
        await User.where('id', '!=', 0).destroy();
        await Transaction.where('id', '!=', 0).destroy();
    });

    it('Get all transactions for the given user id', async () => {
        // Register an account
        let registerRes = await server.inject(TestHelper.newUser());
        await server.inject(TestHelper.newAppointment(registerRes.result.token));
        await server.inject(TestHelper.newAppointment(registerRes.result.token));
        let res = await server.inject({
            method: 'get',
            url: '/appointments/user',
            headers: { 'Authorization': registerRes.result.token },
        });

        expect(res.statusCode).to.equal(200); 
        expect(res.result.success).to.be.true(); 
        expect(res.result.appointments.length == 2).to.be.true(); 
    });

});
describe('DELETE /appointment', () => {
    let server;

    beforeEach(async () => {
        server = await init();
    });

    afterEach(async () => {
        await server.stop();
        await User.where('id', '!=', 0).destroy();
        await Appointment.where('id', '!=', 0).destroy();
    });

    it('Test deleting an appointment', async () => {
        let registerRes = await server.inject(TestHelper.newUser());
        let appointmentRes = await server.inject(TestHelper.newAppointment(registerRes.result.token));
        let res = await server.inject(TestHelper.deleteAppointment(registerRes.result.token, appointmentRes.result.appointment.id));
        expect(res.statusCode).to.equal(200); 
        expect(res.result.success).to.be.true(); 
    });

    it('Test deleting an appointment without a valid id', async () => {
        let registerRes = await server.inject(TestHelper.newUser());
        let appointmentRes = await server.inject(TestHelper.newAppointment(registerRes.result.token));
        let res = await server.inject(TestHelper.deleteAppointment(registerRes.result.token, appointmentRes.result.appointment.id + 1));
        expect(res.statusCode).to.equal(404); 
        expect(res.result.success).to.be.false(); 
    });

});
'use strict';

const Lab = require('@hapi/lab');
const { expect } = require('@hapi/code');
const { afterEach, beforeEach, describe, it } = exports.lab = Lab.script();
const { init } = require('../lib/server-test');

describe('POST /feedback', () => {
    let server;

    beforeEach(async () => {
        server = await init();
    });

    afterEach(async () => {
        await server.stop();
    });

    it('Test submitting feedback', async () => {
        let res = await server.inject({
            method: 'post',
            url: '/feedback',
            payload: {
                first_name: 'James',
                last_name: 'Lewis',
                email: 'test@test.com',
                message: "This is a really cool test.",
            }
        });
        
        expect(res.statusCode).to.equal(201); 
        expect(res.result.success).to.be.true();
    });

});

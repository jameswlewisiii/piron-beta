'use strict';

const Lab = require('@hapi/lab');
const { expect } = require('@hapi/code');
const { afterEach, beforeEach, describe, it } = exports.lab = Lab.script();
const { init } = require('../lib/server-test');
const User = require('../models/user');
const TestHelper = require('../modules/TestHelper');

describe('GET /analytics/dashboard', () => {
    let server;

    beforeEach(async () => {
        server = await init();
    });

    afterEach(async () => {
        await server.stop();
        await User.where('id', '!=', 0).destroy();
    });

    it('Test getting dashboard data', async () => {
         // Register an account
        let registerRes = await server.inject(TestHelper.newUser());
        let transactionRes = await server.inject(TestHelper.newTransaction(registerRes.result.token));
        let res = await server.inject({
            method: 'get',
            url: '/analytics/dashboard',
            headers: { 'Authorization': registerRes.result.token }
        });
        
        expect(res.statusCode).to.equal(200); 
        expect(res.result.success).to.be.true(); 
        expect(res.result.analytics.income).to.be.equal(transactionRes.result.transaction.amount);
        expect(res.result.analytics.clients).to.be.equal(0);
        expect(res.result.analytics.appointments).to.be.equal(0);
        expect(res.result.analytics.transactions).to.be.equal(1);
    });

});
